﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows;
using System.Windows.Forms;
using GreenshotPlugin.Core.Settings;
using GreenshotPlugin.Controls;
using System.ComponentModel.Composition;

namespace Greenshot.Configuration.Pages {
	/// <summary>
	/// Logic for the CaptureSettingsPage.xaml
	/// </summary>
	[Export(typeof(SettingsPage))]
	[ExportMetadata("path", "settings_capture")]
	public partial class CaptureSettingsPage : SettingsPage {

		public CaptureSettingsPage() {
			InitializeComponent();
		}

		private void Button_Color(object sender, RoutedEventArgs e) {
			ColorForm colorForm = ColorForm.GetInstance();
			colorForm.Color = Config.DWMBackgroundColor;
			// Using the _parent to make sure the dialog doesn't show on another window
			colorForm.ShowDialog();
			if (colorForm.DialogResult != DialogResult.Cancel) {
				if (!colorForm.Color.Equals(Config.DWMBackgroundColor)) {
					Config.DWMBackgroundColor.Value = colorForm.Color;
				}
			}
		}
	}
}
