﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.Xml;
using GreenshotPlugin.Core;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using log4net;
using GreenshotPlugin.Modules;
using System.Reflection;
using Greenshot.IniFile;
using System.Diagnostics;
using System.Windows.Forms;
using Greenshot.Configuration;
using System.Threading;
using System.ComponentModel.Composition;

namespace Greenshot.Helpers {
	public class InstallerFile {
		public string Filename {
			get;
			set;
		}
		public bool IsReleaseCandidate {
			get;
			set;
		}
		public bool IsUnstable {
			get;
			set;
		}
		public Version Version {
			get;
			set;
		}
	}

	public class BlogEntry {
		public string Title {
			get;
			set;
		}
		public DateTimeOffset Published {
			get;
			set;
		}
		public string Description {
			get;
			set;
		}
		public Uri Link {
			get;
			set;
		}
	}

	/// <summary>
	/// the UpdateChecker takes care of checking for updates.
	/// </summary>
	[Export(typeof(IStartupAction))]
	public class UpdateCheckerAction : IStartupAction, IShutdownAction {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(UpdateCheckerAction));
		private static readonly CoreConfiguration _conf = IniConfig.GetIniSection<CoreConfiguration>();
		private const string Updatefeed = "http://getgreenshot.org/project-feed";
		private const string Blogfeed = "http://getgreenshot.org/feed/";
		// The version to check against, this is update when newer files are found so we only report an update once!
		private readonly Version _currentVersion = Assembly.GetExecutingAssembly().GetName().Version;
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
		/// <summary>
		/// Sinmle helper method to get a version from a filename
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		private static string GetVersion(string filename) {
			var match = Regex.Match(filename, @"(\d{1,4}\.){3}\d{1,4}");
			return match.Success ? match.Groups[0].Value : null;
		}

		/// <summary>
		/// Create the update checker with the current version
		/// </summary>
		/// <param name="currentVersion"></param>
		public UpdateCheckerAction() {
			BlogEntries = new SortedList<DateTimeOffset, BlogEntry>();
		}

		/// <summary>
		/// Current blog entries
		/// </summary>
		private SortedList<DateTimeOffset, BlogEntry> BlogEntries {
			get;
			set;
		}

		/// <summary>
		/// When we have a newer file as "this", the information on this file is stored here.
		/// </summary>
		private InstallerFile LatestInstaller {
			get;
			set;
		}

		/// <summary>
		/// IShutdownAction indirectly called from App.xaml.cs 
		/// </summary>
		public void Shutdown() {
			cancellationTokenSource.Cancel();
		}

		/// <summary>
		/// IStartupAction indirectly called from App.xaml.cs 
		/// </summary>
		public void Start() {
			Task perdiodicTask = PeriodicTaskFactory.Start(() => {
				if (IsCheckNeeded) {
					// If current is unstable or CheckForUnstable we check for unstables
					bool checkUnstable = _conf.CheckForUnstable || _conf.BuildState == BuildStates.UNSTABLE;
					// If current is release candidate or CheckForUnstable we check for release candidates
					bool checkReleaseCandidate = _conf.CheckForUnstable || _conf.BuildState != BuildStates.RELEASE;
					CheckForProgramUpdates(checkReleaseCandidate, checkUnstable).ContinueWith(
						hasNewFile => {
							if (!hasNewFile.Result) {
								return;
							}
							// TODO: Move to WPF NotifyIcon!!
							MainForm.Instance.NotifyIcon.BalloonTipClicked += HandleBalloonTipClick;
							MainForm.Instance.NotifyIcon.BalloonTipClosed += CleanupBalloonTipClick;
							MainForm.Instance.NotifyIcon.ShowBalloonTip(10000, "Greenshot", Language.GetFormattedString(LangKey.update_found, "'" + LatestInstaller.Filename + "'"), ToolTipIcon.Info);
						}
					);

					CheckBlogFeed().ContinueWith(
						hasNewBlogEntry => {
							if (!hasNewBlogEntry.Result) {
								return;
							}
							// TODO: Move to WPF NotifyIcon!!
							MainForm.Instance.NotifyIcon.ShowBalloonTip(10000, "Greenshot", "Blog entry found", ToolTipIcon.Info);
							foreach (var blog in BlogEntries.Reverse()) {
								Console.WriteLine(@"{0} - {1} - {2}", blog.Key, blog.Value.Title, blog.Value.Link);
							}
						}
					);
				}
			}, intervalInMilliseconds: 6000, cancelToken: cancellationTokenSource.Token);
		}

		/// <summary>
		/// Do we need to check for updates?
		/// </summary>
		private bool IsCheckNeeded {
			get {
				// Do we need to check?
				if (_conf.UpdateCheckInterval == 0) {
					return false;
				}
				var checkTime = _conf.LastUpdateCheck;
				checkTime = checkTime.AddDays(_conf.UpdateCheckInterval);
				if (DateTime.Now.CompareTo(checkTime.DateTime) < 0) {
					LOG.DebugFormat("No need to check RSS feed for updates, feed check will be after {0}", checkTime);
					return false;
				}
				LOG.DebugFormat("Update check is due, last check was {0} check needs to be made after {1} (which is one {2} later)", _conf.LastUpdateCheck, checkTime, _conf.UpdateCheckInterval);

				return true;
			}
		}

		/// <summary>
		/// Check if the blog has an update
		/// </summary>
		/// <returns>bool (async) when there is an update</returns>
		private async Task<bool> CheckBlogFeed() {
			HttpResponseMessage response = await NetworkHelper.HttpClient.GetAsync(Blogfeed);
			// Check that response was successful or throw exception
			response.EnsureSuccessStatusCode();

			var feedStream = await response.Content.ReadAsStreamAsync();
			var feed = SyndicationFeed.Load<SyndicationFeed>(new XmlTextReader(feedStream));
			if (feed == null) {
				return false;
			}
			bool hasUpdate = false;
			var blogEntries = from item in feed.Items
				where !BlogEntries.ContainsKey(item.PublishDate)
				select
					new BlogEntry {
						Title = item.Title.Text,
						Description = item.Summary.Text,
						Link = item.Links[0].GetAbsoluteUri(),
						Published = item.PublishDate
					};
			foreach (var blogEntry in blogEntries) {
				BlogEntries.Add(blogEntry.Published, blogEntry);
				hasUpdate = true;
			}
			return hasUpdate;
		}

		/// <summary>
		/// Check for program updates, return true if there are any
		/// </summary>
		/// <param name="checkRc"></param>
		/// <param name="checkUnstable"></param>
		/// <returns>bool (async)</returns>
		private async Task<bool> CheckForProgramUpdates(bool checkRc, bool checkUnstable) {
			HttpResponseMessage response = await NetworkHelper.HttpClient.GetAsync(Updatefeed);
			// Check that response was successful or throw exception
			response.EnsureSuccessStatusCode();

			bool hasUpdate = false;

			Stream feedStream = await response.Content.ReadAsStreamAsync();
			var feed = SyndicationFeed.Load<SyndicationFeed>(new XmlTextReader(feedStream));
			if (feed == null) {
				return false;
			}
			var installerFiles = from item in feed.Items
				where item.Title.Text.EndsWith(".exe") && item.Title.Text.Contains("-INSTALLER-")
				let version = GetVersion(item.Title.Text)
				where version != null
				select
					new InstallerFile {
						Filename = Regex.Replace(item.Title.Text, @".*/", ""),
						IsReleaseCandidate = item.Title.Text.ToLower().Contains("-rc"),
						IsUnstable = item.Title.Text.ToLower().Contains("unstable"),
						Version = new Version(version)
					};
			foreach (var installerFile in installerFiles) {
				// if the file is unstable, we will skip it when:
				// the current version is a release or release candidate AND check unstable is turned off.
				if (installerFile.IsUnstable && !checkUnstable) {
					// Skip if we shouldn't check unstables
					LOG.DebugFormat("Ignoring as we don't check unstable: {0}", installerFile.Filename);
					continue;
				}

				// if the file is a release candidate, we will skip it when:
				// the current version is a release AND check unstable is turned off.
				if (installerFile.IsReleaseCandidate && !checkRc) {
					LOG.DebugFormat("Ignoring as we don't check for rc: {0}", installerFile.Filename);
					continue;
				}

				// Compare versions
				if (installerFile.Version.CompareTo(_currentVersion) > 0) {
					// if current file newer than skip
					if (LatestInstaller != null && installerFile.Version.CompareTo(LatestInstaller.Version) <= 0) {
						LOG.DebugFormat("Ignoring as we already have this: {0}", installerFile.Filename);
						continue;
					}
					LatestInstaller = installerFile;
					hasUpdate = true;
				} else {
					LOG.DebugFormat("Ignoring as the version is lower: {0}", installerFile.Filename);
				}
			}
			return hasUpdate;
		}

		/// <summary>
		/// Remove the handlers
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CleanupBalloonTipClick(object sender, EventArgs e) {
			MainForm.Instance.NotifyIcon.BalloonTipClicked -= HandleBalloonTipClick;
			MainForm.Instance.NotifyIcon.BalloonTipClosed -= CleanupBalloonTipClick;
		}

		/// <summary>
		/// When the ballon tip is clicked, open the link to getgreenshot.org
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleBalloonTipClick(object sender, EventArgs e) {
			string link;
			if (LatestInstaller.IsReleaseCandidate || LatestInstaller.IsUnstable) {
				link = "http://getgreenshot.org/version-history/";
			} else {
				link = "http://getgreenshot.org/downloads/";
			}

			try {
				Process.Start(link);
			} catch (Exception) {
				MessageBox.Show(Language.GetFormattedString(LangKey.error_openlink, link), Language.GetString(LangKey.error));
			} finally {
				CleanupBalloonTipClick(null, null);
			}
		}

		public string Designation {
			get {
				return "UpdateCheckerAction";
			}
		}

		public string Description {
			get {
				return "Start the update checker";
			}
		}

		public int Priority {
			get {
				return 2;
			}
		}

		public bool IsActive {
			get {
				return true;
			}
		}
	}
}
