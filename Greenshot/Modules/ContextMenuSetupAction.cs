﻿
/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.ComponentModel.Composition;
using System.IO;
using System.Windows;
using System.Windows.Media;
using log4net;
using Apex.MVVM;
using Greenshot.Model;
using GreenshotPlugin.Core;
using GreenshotPlugin.Modules;
using GreenshotPlugin.WPF;
using System.Collections.ObjectModel;

namespace Greenshot.Services {
	/// <summary>
	/// This startup action does the initial context-menu setup
	/// </summary>
	[Export(typeof(IStartupAction))]
	public class ContextMenuSetupAction : IStartupAction {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(ContextMenuSetupAction));

		public void Start() {

			// Used for testing the icon, we could load any type of FrameworkElement, also a SVG
			FrameworkElement icon;
			using (Stream stream = this.GetType().Assembly.GetManifestResourceStream("Greenshot.icons.selection.svg")) {
				icon = SvgXaml.Wrap(SvgXaml.ToXaml(stream), Brushes.White, new Size(32, 32));
			}

			LOG.Debug("Setting up the context menu");
			MainViewModel.Instance.ContextMenu.Add(new ContextMenuItem {
				Key = "contextmenu_capturearea",
				ItemCommand = new Command(() => {
					CaptureHelper.CaptureRegion(false);
				}),
				Icon = icon
			});
			MainViewModel.Instance.ContextMenu.Add(new ContextMenuItem {
				Header = Language.GetString("contextmenu_capturearea") + " (async)",
				ItemCommand = new AsynchronousCommand(() => {
					CaptureHelper.CaptureRegion(false);
				})
			});
			// Add separator
			MainViewModel.Instance.ContextMenu.Add(null);

			var subItems = new ObservableCollection<ContextMenuItem>();
			subItems.Add(new ContextMenuItem {
				Header = "Level 2 (does exit)",
				ItemCommand = new Command(() => {
					Application.Current.Shutdown();
					System.Windows.Forms.Application.Exit();
				}),
				Icon = icon
			});
			MainViewModel.Instance.ContextMenu.Add(new ContextMenuItem {
				Header = "Level 1",
				SubItems = subItems
			});
			// Add separator
			MainViewModel.Instance.ContextMenu.Add(null);
			MainViewModel.Instance.ContextMenu.Add(new ContextMenuItem {
				Key = "contextmenu_exit",
				ItemCommand = new Command(() => {
					Application.Current.Shutdown();
					System.Windows.Forms.Application.Exit();
				})
			});

		}

		//<resource name="contextmenu_about">About Greenshot</resource>
		//<resource name="contextmenu_capturearea">Capture region</resource>
		//<resource name="contextmenu_captureclipboard">Open image from clipboard</resource>
		//<resource name="contextmenu_capturefullscreen">Capture full screen</resource>
		//<resource name="contextmenu_capturefullscreen_all">all</resource>
		//<resource name="contextmenu_capturefullscreen_bottom">bottom</resource>
		//<resource name="contextmenu_capturefullscreen_left">left</resource>
		//<resource name="contextmenu_capturefullscreen_right">right</resource>
		//<resource name="contextmenu_capturefullscreen_top">top</resource>
		//<resource name="contextmenu_captureie">Capture Internet Explorer</resource>
		//<resource name="contextmenu_captureiefromlist">Capture Internet Explorer from list</resource>
		//<resource name="contextmenu_capturelastregion">Capture last region</resource>
		//<resource name="contextmenu_capturewindow">Capture window</resource>
		//<resource name="contextmenu_capturewindowfromlist">Capture window from list</resource>
		//<resource name="contextmenu_donate">Support Greenshot</resource>
		//<resource name="contextmenu_exit">Exit</resource>
		//<resource name="contextmenu_help">Help</resource>
		//<resource name="contextmenu_openfile">Open image from file</resource>
		//<resource name="contextmenu_openrecentcapture">Open last capture location</resource>
		//<resource name="contextmenu_quicksettings">Quick preferences</resource>
		//<resource name="contextmenu_settings">Preferences...</resource>

		#region IStartupAction
		public string Designation {
			get {
				return "ContextMenuSetupAction";
			}
		}

		public string Description {
			get {
				return "Create the initial context menu";
			}
		}

		public int Priority {
			get {
				return 0;
			}
		}

		public bool IsActive {
			get {
				return true;
			}
		}
		#endregion

	}
}
