﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.ComponentModel.Composition;
using GreenshotPlugin.Modules;
using log4net;
using GreenshotPlugin.Core;
using System.Threading.Tasks;
using System.Threading;

namespace Greenshot.Services {
	/// <summary>
	/// This class will be automatically loaded and executed at startup
	/// </summary>
	[Export(typeof(IStartupAction))]
	[Export(typeof(IShutdownAction))]
	public class PluginLoaderAction : IStartupAction , IShutdownAction {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(PluginLoaderAction));

		/// <summary>
		/// Called when the application starts
		/// </summary>
		public async void Start() {
			await Task.Factory.StartNew(() => {
				foreach (var plugin in ModuleContainer.Instance.Plugins) {
					var capturedPlugin = plugin;
					try {
						LOG.InfoFormat("Initializing {0}", capturedPlugin.Metadata["name"]);
						// TODO: replace Greenshot host
						Task.Factory.StartNew(() => {
							capturedPlugin.Value.Initialize(PluginHelper.Instance, plugin.Metadata);
							LOG.InfoFormat("Initialized {0}", capturedPlugin.Metadata["name"]);
						}, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
					} catch (Exception ex) {
						LOG.Error("Error initializing plugin: ", ex);
					}
				}
			}, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
		}

		/// <summary>
		/// Called when the application shuts down
		/// </summary>
		public void Shutdown() {
			foreach (var plugin in ModuleContainer.Instance.Plugins) {
				try {
					LOG.InfoFormat("Shutting down {0}", plugin.Metadata["name"]);
					if (plugin.IsValueCreated) {
						plugin.Value.Shutdown();
						plugin.Value.Dispose();
					}
				} catch (Exception ex) {
					LOG.Error("Error shutting down plugin: ", ex);
				}
			}
		}

		public string Designation {
			get {
				return "PluginLoaderAction";
			}
		}

		public string Description {
			get {
				return "Load the plugins";
			}
		}

		public int Priority {
			get {
				return 1;
			}
		}

		public bool IsActive {
			get {
				return true;
			}
		}

	}
}
