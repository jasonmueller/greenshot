﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Windows;
using System.Reflection;
using System;
using System.Windows.Threading;
using System.IO;
using System.Linq;
using Greenshot.Windows;
using log4net;
using GreenshotPlugin.Modules;
using System.Threading;
using Greenshot.IniFile;
using GreenshotPlugin.Core;
using System.Security.Principal;
using System.Security.AccessControl;
using Greenshot.Services;
using System.Text;
using System.Diagnostics;
using GreenshotPlugin.UnmanagedHelpers;
using Greenshot.Configuration;
using Greenshot.Helpers;
using System.Collections.Generic;

namespace Greenshot {
	/// <summary>
	/// Logic for the App, here we:
	/// 1) catch exceptions and show the BugReportWindow.
	/// 2) Lock the AppMutex / detect running instances
	/// 3) Handle arguments
	/// 4) Call all IStartupActions at startup
	/// 5) Call all IShutdownActions at shutdown
	/// </summary>
	public partial class App : Application {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(App));
		private const string MutexId = @"Local\F48E86D3-E34C-4DB7-8F8F-9A0EA55F0D08";
		private static Mutex _applicationMutex;

		/// <summary>
		/// Static initializer, sets the exception handlers
		/// </summary>
		static App() {
			AppDomain.CurrentDomain.AssemblyResolve += (eventSender, args) => {
				String resourceName = Assembly.GetExecutingAssembly().GetName().Name + "." + new AssemblyName(args.Name).Name + ".dll";
				using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName)) {
					if (stream != null) {
						Byte[] assemblyData = new Byte[stream.Length];
						stream.Read(assemblyData, 0, assemblyData.Length);
						return Assembly.Load(assemblyData);
					} else {
						return null;
					}
				}
			};

			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
		}

		/// <summary>
		/// Set all stuff for starting up
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Application_Startup(object sender, StartupEventArgs e) {
			Current.DispatcherUnhandledException += Application_ThreadException;
			// Register cleanup
			Current.Exit += DoShutdown;

			// Set the Thread name, is better than "1"
			Thread.CurrentThread.Name = "Greenshot main";

			var arguments = new Arguments(e.Args);

			if (!LockAppMutex()) {
				// Other instance is running, call a Greenshot client or exit!!
				if (arguments.IsExit) {
					GreenshotClient.Exit();
				}
				if (arguments.IsReload) {
					GreenshotClient.ReloadConfig();
				}
				if (!String.IsNullOrWhiteSpace(arguments.Language)) {
					// Set language
				}
				GreenshotClient.OpenFiles(arguments.FilesToOpen);

				ShowOtherInstances();
				Current.Shutdown();
				return;
			}
			if (arguments.IsNoRun) {
				return;
			}

			// TODO: Process arguments.FilesToOpen for the case Greenshot wasn't running!

			DoStartup();
			MainForm.Start(e.Args);
		}

		/// <summary>
		/// Startup logic
		/// </summary>
		private static void DoStartup() {
			// Initialize the IniConfig
			IniConfig.Init();

			// Init Log4NET
			LogHelper.InitializeLog4NET();

			// Startup all "services"
			var startupActions = from export in ModuleContainer.Instance.Container.GetExports<IStartupAction, IDictionary<string,object>>() where export.Value.IsActive orderby export.Value.Priority select export;
			foreach (var startupAction in startupActions) {
				try {
					LOG.InfoFormat("Starting: {0}", startupAction.Value.Description);
					startupAction.Value.Start();
				} catch (Exception ex) {
					if (startupAction.IsValueCreated) {
						LOG.Error(string.Format("Exception executing startupAction {0}: ", startupAction.Value.Designation), ex);
					} else {
						LOG.Error("Exception instanciating startupAction: ", ex);
					}
				}
			}
		}

		/// <summary>
		/// Shutdown logic
		/// </summary>
		private static void DoShutdown(object sender, ExitEventArgs e) {
			// Shutdown all services
			var shutdownActions = from export in ModuleContainer.Instance.Container.GetExports<IShutdownAction>() where export.Value.IsActive orderby export.Value.Priority descending select export;
			foreach (var shutdownAction in shutdownActions) {
				try {
					LOG.InfoFormat("Shutting down: {0}", shutdownAction.Value.Description);
					shutdownAction.Value.Shutdown();
				} catch (Exception ex) {
					if (shutdownAction.IsValueCreated) {
						LOG.Error(string.Format("Exception executing shutdownAction {0}: ", shutdownAction.Value.Designation), ex);
					} else {
						LOG.Error("Exception instanciating shutdownAction: ", ex);
					}
				}
			}
			ReleaseMutex();
			Current.Shutdown();
		}

		/// <summary>
		/// This tries to get the AppMutex, which takes care of having multiple Greenshot instances running
		/// </summary>
		/// <returns>true if it worked, false if another instance is already running</returns>
		private static bool LockAppMutex() {
			bool lockSuccess = true;
			// check whether there's an local instance running already, but use local so this works in a multi-user environment
			try {
				// Added Mutex Security, hopefully this prevents the UnauthorizedAccessException more gracefully
				// See an example in Bug #3131534
				SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
				MutexSecurity mutexsecurity = new MutexSecurity();
				mutexsecurity.AddAccessRule(new MutexAccessRule(sid, MutexRights.FullControl, AccessControlType.Allow));
				mutexsecurity.AddAccessRule(new MutexAccessRule(sid, MutexRights.ChangePermissions, AccessControlType.Deny));
				mutexsecurity.AddAccessRule(new MutexAccessRule(sid, MutexRights.Delete, AccessControlType.Deny));

				bool created;
				// 1) Create Mutex
				_applicationMutex = new Mutex(false, MutexId, out created, mutexsecurity);
				// 2) Get the right to it, this returns false if it's already locked
				if (!_applicationMutex.WaitOne(0, false)) {
					LOG.Debug("Greenshot seems already to be running!");
					lockSuccess = false;
					// Clean up
					_applicationMutex.Close();
					_applicationMutex = null;
				}
			} catch (AbandonedMutexException e) {
				// Another Greenshot instance didn't cleanup correctly!
				// we can ignore the exception, it happend on the "waitone" but still the mutex belongs to us
				LOG.Warn("Greenshot didn't cleanup correctly!", e);
			} catch (UnauthorizedAccessException e) {
				LOG.Warn("Greenshot is most likely already running for a different user in the same session, can't create mutex due to error: ", e);
				lockSuccess = false;
			} catch (Exception e) {
				LOG.Warn("Problem obtaining the Mutex, assuming it was already taken!", e);
				lockSuccess = false;
			}
			return lockSuccess;
		}

		/// <summary>
		/// Release the mutex
		/// </summary>
		private static void ReleaseMutex() {
			// Remove the application mutex
			if (_applicationMutex != null) {
				try {
					_applicationMutex.ReleaseMutex();
					_applicationMutex = null;
				} catch (Exception ex) {
					LOG.Error("Error releasing Mutex!", ex);
				}
			}
		}

		/// <summary>
		/// Show other running instances
		/// </summary>
		private static void ShowOtherInstances() {
			StringBuilder instanceInfo = new StringBuilder();
			bool matchedThisProcess = false;
			int index = 1;
			foreach (Process greenshotProcess in Process.GetProcessesByName("greenshot")) {
				try {
					instanceInfo.Append(index++ + ": ").AppendLine(Kernel32.GetProcessPath(new IntPtr(greenshotProcess.Id)));
					if (Process.GetCurrentProcess().Id == greenshotProcess.Id) {
						matchedThisProcess = true;
					}
				} catch (Exception ex) {
					LOG.Debug(ex);
				}
			}
			if (!matchedThisProcess) {
				instanceInfo.Append(index + ": ").AppendLine(Kernel32.GetProcessPath(new IntPtr(Process.GetCurrentProcess().Id)));
			}
			MessageBox.Show(string.Format("{0}\r\n{1}", Language.GetString(LangKey.error_multipleinstances), instanceInfo), Language.GetString(LangKey.error), MessageBoxButton.OK, MessageBoxImage.Error);
		}

		/// <summary>
		/// Show any unhandled exceptions in the main thread
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
			Exception exceptionToLog = e.ExceptionObject as Exception;
			new BugReportWindow(exceptionToLog).ShowDialog();
		}

		/// <summary>
		/// Show any unhandled exceptions in a thread
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private static void Application_ThreadException(object sender, DispatcherUnhandledExceptionEventArgs e) {
			Exception exceptionToLog = e.Exception;
			new BugReportWindow(exceptionToLog).ShowDialog();
		}
	}
}
