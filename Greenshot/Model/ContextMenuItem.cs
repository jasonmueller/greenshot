﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using GreenshotPlugin.Core;

namespace Greenshot.Model {
	public class ContextMenuItem : INotifyPropertyChanged {
		private string _header;

		#region PropertyChanged
		public event PropertyChangedEventHandler PropertyChanged;
		public ContextMenuItem() {
			Language.LanguageChanged += Language_LanguageChanged;
		}
		/// <summary>
		/// Make sure that when the languageChange event comes, the Header property is updated!
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Language_LanguageChanged(object sender, EventArgs e) {
			if (PropertyChanged != null) {
				PropertyChanged(this, new PropertyChangedEventArgs("Header"));
			}
		}
		#endregion

		/// <summary>
		/// Set this to have the Header of the menu dependend on a language key
		/// </summary>
		public string Key {
			get;
			set;
		}

		/// <summary>
		/// This gives the Header of the ContextMenuItem, which should usually only be read after setting the key
		/// In cases where this is fix, the set can be called.
		/// </summary>
		public string Header {
			get {
				if (Key != null) {
					return Language.GetString(Key);
				}
				return _header;
			}
			set {
				_header = value;
			}
		}

		public FrameworkElement Icon {
			get;
			set;
		}

		public ICommand ItemCommand {
			get;
			set;
		}

		public object ItemCommandParameter {
			get;
			set;
		}

		public ObservableCollection<ContextMenuItem> SubItems {
			get;
			set;
		}
	}
}
