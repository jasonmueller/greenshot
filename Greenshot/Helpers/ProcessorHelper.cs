﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

using GreenshotPlugin;
using GreenshotPlugin.Core;
using log4net;
using GreenshotPlugin.Modules;

namespace Greenshot.Helpers {
	/// <summary>
	/// Description of ProcessorHelper.
	/// </summary>
	public static class ProcessorHelper {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(ProcessorHelper));
		private static readonly Dictionary<string, ILegacyProcessor> RegisteredProcessors = new Dictionary<string, ILegacyProcessor>();

		/// Initialize the Processors		
		static ProcessorHelper() {
			foreach(Type processorType in InterfaceUtils.GetSubclassesOf(typeof(ILegacyProcessor),true)) {
				// Only take our own
				if (!"Greenshot.Processors".Equals(processorType.Namespace)) {
					continue;
				}
				try {
					if (!processorType.IsAbstract) {
						ILegacyProcessor processor;
						try {
							processor = (ILegacyProcessor)Activator.CreateInstance(processorType);
						} catch (Exception e) {
							LOG.ErrorFormat("Can't create instance of {0}", processorType);
							LOG.Error(e);
							continue;
						}
						if (processor.isActive) {
							LOG.DebugFormat("Found Processor {0} with designation {1}", processorType.Name, processor.Designation);
							RegisterProcessor(processor);
						} else {
							LOG.DebugFormat("Ignoring Processor {0} with designation {1}", processorType.Name, processor.Designation);
						}
					}
				} catch (Exception ex) {
					LOG.ErrorFormat("Error loading processor {0}, message: ", processorType.FullName, ex.Message);
				}
			}
		}

		/// <summary>
		/// Register your Processor here, if it doesn't come from a plugin and needs to be available
		/// </summary>
		/// <param name="processor"></param>
		public static void RegisterProcessor(ILegacyProcessor processor) {
			// don't test the key, an exception should happen wenn it's not unique
			RegisteredProcessors.Add(processor.Designation, processor);
		}

		private static List<ILegacyProcessor> GetPluginsProcessors() {
			List<ILegacyProcessor> processors = new List<ILegacyProcessor>();
			if (ModuleContainer.Instance.HasPlugins) {
				foreach (var plugin in ModuleContainer.Instance.Plugins) {
					if (plugin.IsValueCreated) {
						try {
							var procs = plugin.Value.Processors();
							if (procs != null) {
								processors.AddRange(procs);
							}
						} catch (Exception ex) {
							LOG.ErrorFormat("Couldn't get processors from the plugin {0}", plugin.Metadata["name"]);
							LOG.Error(ex);
						}
					}
				}
				processors.Sort();
			}
			return processors;
		}

		/// <summary>
		/// Get a list of all Processors, registered or supplied by a plugin
		/// </summary>
		/// <returns></returns>
		public static List<ILegacyProcessor> GetAllProcessors() {
			List<ILegacyProcessor> processors = new List<ILegacyProcessor>();
			processors.AddRange(RegisteredProcessors.Values);
			processors.AddRange(GetPluginsProcessors());
			processors.Sort();
			return processors;
		}

		/// <summary>
		/// Get a Processor by a designation
		/// </summary>
		/// <param name="designation">Designation of the Processor</param>
		/// <returns>IProcessor or null</returns>
		public static ILegacyProcessor GetProcessor(string designation) {
			if (designation == null) {
				return null;
			}
			if (RegisteredProcessors.ContainsKey(designation)) {
				return RegisteredProcessors[designation];
			}
			foreach (ILegacyProcessor processor in GetPluginsProcessors()) {
				if (designation.Equals(processor.Designation)) {
					return processor;
				}
			}
			return null;
		}

		/// <summary>
		/// A simple helper method which will call ProcessCapture for the Processor with the specified designation
		/// </summary>
		/// <param name="designation"></param>
		/// <param name="surface"></param>
		/// <param name="captureDetails"></param>
		public static void ProcessCapture(string designation, ISurface surface, ICaptureDetails captureDetails) {
			if (RegisteredProcessors.ContainsKey(designation)) {
				ILegacyProcessor processor = RegisteredProcessors[designation];
				if (processor.isActive) {
					processor.ProcessCapture(surface, captureDetails);
				}
			}
		}
	}
}
