﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

using Greenshot.Configuration;
using Greenshot.Forms;
using GreenshotPlugin;
using GreenshotPlugin.Core;
using Greenshot.IniFile;
using Greenshot.Core;
using log4net;

namespace Greenshot.Helpers {
	/// <summary>
	/// Description of PrintHelper.
	/// </summary>
	public class PrintHelper : IDisposable {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(PrintHelper));
		private static readonly CoreConfiguration Conf = IniConfig.GetIniSection<CoreConfiguration>();

		private ISurface _surface;
		private readonly ICaptureDetails _captureDetails;
		private PrintDocument _printDocument = new PrintDocument();
		private PrintDialog _printDialog = new PrintDialog();

		public PrintHelper(ISurface surface, ICaptureDetails captureDetails) {
			_surface = surface;
			_captureDetails = captureDetails;
			_printDialog.UseEXDialog = true;
			_printDocument.DocumentName = FilenameHelper.GetFilenameWithoutExtensionFromPattern(Conf.OutputFileFilenamePattern, captureDetails);
			_printDocument.PrintPage += DrawImageForPrint;
			_printDialog.Document = _printDocument;
		}

		/**
		 * Destructor
		 */
		~PrintHelper() {
			Dispose(false);
		}

		/**
		 * The public accessible Dispose
		 * Will call the GarbageCollector to SuppressFinalize, preventing being cleaned twice
		 */
		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/**
		 * This Dispose is called from the Dispose and the Destructor.
		 * When disposing==true all non-managed resources should be freed too!
		 */
		protected virtual void Dispose(bool disposing) {
			if (disposing) {
				if (_printDocument != null) {
					_printDocument.Dispose();
				}
				if (_printDialog != null) {
					_printDialog.Dispose();
				}
			}
			_surface = null;
			_printDocument = null;
			_printDialog = null;
		}

		/// <summary>
		/// displays options dialog (if not disabled via settings) and windows
		/// print dialog.
		/// </summary>
		/// <returns>printer settings if actually printed, or null if print was cancelled or has failed</returns>
		public PrinterSettings PrintTo(string printerName) {
			PrinterSettings returnPrinterSettings = null;
			DialogResult? printOptionsResult = ShowPrintOptionsDialog();
				try {
					if (printOptionsResult == null || printOptionsResult == DialogResult.OK) {
					_printDocument.PrinterSettings.PrinterName = printerName;
					if (!IsColorPrint()) {
						_printDocument.DefaultPageSettings.Color = false;
					}
					_printDocument.Print();
					returnPrinterSettings = _printDocument.PrinterSettings;
				}
			} catch (Exception e) {
				LOG.Error("An error ocurred while trying to print", e);
				MessageBox.Show(Language.GetString(LangKey.print_error), Language.GetString(LangKey.error));
			}
			return returnPrinterSettings;
		}

		/// <summary>
		/// displays options dialog (if not disabled via settings) and windows
		/// print dialog.
		/// </summary>
		/// <returns>printer settings if actually printed, or null if print was cancelled or has failed</returns>
		public PrinterSettings PrintWithDialog() {
			PrinterSettings returnPrinterSettings = null;
			if (_printDialog.ShowDialog() == DialogResult.OK) {
				DialogResult? printOptionsResult = ShowPrintOptionsDialog();
				try {
					if (printOptionsResult == null || printOptionsResult == DialogResult.OK) {
						if (!IsColorPrint()) {
							_printDocument.DefaultPageSettings.Color = false;
						}
						_printDocument.Print();
						returnPrinterSettings = _printDialog.PrinterSettings;
					}
				} catch (Exception e) {
					LOG.Error("An error ocurred while trying to print", e);
					MessageBox.Show(Language.GetString(LangKey.print_error), Language.GetString(LangKey.error));
				}

			}
			return returnPrinterSettings;
		}

		private bool IsColorPrint() {
			return !Conf.OutputPrintGrayscale && !Conf.OutputPrintMonochrome;
		}

		/// <summary>
		/// display print options dialog (if the user has not configured Greenshot not to)
		/// </summary>
		/// <returns>result of the print dialog, or null if the dialog has not been displayed by config</returns>
		private DialogResult? ShowPrintOptionsDialog() {
			DialogResult? ret = null;
			if (Conf.OutputPrintPromptOptions) {
				using (PrintOptionsDialog printOptionsDialog = new PrintOptionsDialog()) {
					ret = printOptionsDialog.ShowDialog();
				}
			} 
			return ret;
		}

		void DrawImageForPrint(object sender, PrintPageEventArgs e) {
			

			// Create the output settins
			SurfaceOutputSettings printOutputSettings = new SurfaceOutputSettings(OutputFormat.png, 100, false);

			ApplyEffects(printOutputSettings);

			Image image;
			bool disposeImage = ImageOutput.CreateImageFromSurface(_surface, printOutputSettings, out image);
			try {
				ContentAlignment alignment = Conf.OutputPrintCenter ? ContentAlignment.MiddleCenter : ContentAlignment.TopLeft;

				// prepare timestamp
				float footerStringWidth = 0;
				float footerStringHeight = 0;
				string footerString = null; //DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
				if (Conf.OutputPrintFooter) {
					footerString = FilenameHelper.FillPattern(Conf.OutputPrintFooterPattern, _captureDetails, false);
					using (Font f = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Regular)) {
						footerStringWidth = e.Graphics.MeasureString(footerString, f).Width;
						footerStringHeight = e.Graphics.MeasureString(footerString, f).Height;
					}
				}

				// Get a rectangle representing the printable Area
				RectangleF pageRect = e.PageSettings.PrintableArea;
				if(e.PageSettings.Landscape) {
					float origWidth = pageRect.Width;
					pageRect.Width = pageRect.Height;
					pageRect.Height = origWidth;
				}

				// Subtract the dateString height from the available area, this way the area stays free
				pageRect.Height -= footerStringHeight;

				GraphicsUnit gu = GraphicsUnit.Pixel;
				RectangleF imageRect = image.GetBounds(ref gu);
				// rotate the image if it fits the page better
				if (Conf.OutputPrintAllowRotate) {
					if ((pageRect.Width > pageRect.Height && imageRect.Width < imageRect.Height) || (pageRect.Width < pageRect.Height && imageRect.Width > imageRect.Height)) {
						image.RotateFlip(RotateFlipType.Rotate270FlipNone);
						imageRect = image.GetBounds(ref gu);
						if (alignment.Equals(ContentAlignment.TopLeft)) {
							alignment = ContentAlignment.TopRight;
						}
					}
				}

				RectangleF printRect = new RectangleF(0, 0, imageRect.Width, imageRect.Height);
				// scale the image to fit the page better
				if (Conf.OutputPrintAllowEnlarge || Conf.OutputPrintAllowShrink) {
					SizeF resizedRect = GetScaledSize(imageRect.Size, pageRect.Size, false);
					if ((Conf.OutputPrintAllowShrink && resizedRect.Width < printRect.Width) || Conf.OutputPrintAllowEnlarge && resizedRect.Width > printRect.Width) {
						printRect.Size = resizedRect;
					}
				}

				// align the image
				printRect = GetAlignedRectangle(printRect, new RectangleF(0, 0, pageRect.Width, pageRect.Height), alignment);
				if (Conf.OutputPrintFooter) {
					//printRect = new RectangleF(0, 0, printRect.Width, printRect.Height - (dateStringHeight * 2));
					using (Font f = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Regular)) {
						e.Graphics.DrawString(footerString, f, Brushes.Black, pageRect.Width / 2 - (footerStringWidth / 2), pageRect.Height);
					}
				}
				e.Graphics.DrawImage(image, printRect, imageRect, GraphicsUnit.Pixel);

			} finally {
				if (disposeImage && image != null) {
					image.Dispose();
					image = null;
				}
			}
		}

		/// <summary>
		/// calculates the Size an element must be resized to, in order to fit another element, keeping aspect ratio
		/// </summary>
		/// <param name="currentSize">the size of the element to be resized</param>
		/// <param name="targetSize">the target size of the element</param>
		/// <param name="crop">in case the aspect ratio of currentSize and targetSize differs: shall the scaled size fit into targetSize (i.e. that one of its dimensions is smaller - false) or vice versa (true)</param>
		/// <returns>a new SizeF object indicating the width and height the element should be scaled to</returns>
		public static SizeF GetScaledSize(SizeF currentSize, SizeF targetSize, bool crop) {
			float wFactor = targetSize.Width / currentSize.Width;
			float hFactor = targetSize.Height / currentSize.Height;

			float factor = crop ? Math.Max(wFactor, hFactor) : Math.Min(wFactor, hFactor);
			return new SizeF(currentSize.Width * factor, currentSize.Height * factor);
		}

		/// <summary>
		/// calculates the position of an element depending on the desired alignment within a RectangleF
		/// </summary>
		/// <param name="currentRect">the bounds of the element to be aligned</param>
		/// <param name="targetRect">the rectangle reference for aligment of the element</param>
		/// <param name="alignment">the System.Drawing.ContentAlignment value indicating how the element is to be aligned should the width or height differ from targetSize</param>
		/// <returns>a new RectangleF object with Location aligned aligned to targetRect</returns>
		public static RectangleF GetAlignedRectangle(RectangleF currentRect, RectangleF targetRect, ContentAlignment alignment) {
			RectangleF newRect = new RectangleF(targetRect.Location, currentRect.Size);
			switch (alignment) {
				case ContentAlignment.TopCenter:
					newRect.X = (targetRect.Width - currentRect.Width) / 2;
					break;
				case ContentAlignment.TopRight:
					newRect.X = (targetRect.Width - currentRect.Width);
					break;
				case ContentAlignment.MiddleLeft:
					newRect.Y = (targetRect.Height - currentRect.Height) / 2;
					break;
				case ContentAlignment.MiddleCenter:
					newRect.Y = (targetRect.Height - currentRect.Height) / 2;
					newRect.X = (targetRect.Width - currentRect.Width) / 2;
					break;
				case ContentAlignment.MiddleRight:
					newRect.Y = (targetRect.Height - currentRect.Height) / 2;
					newRect.X = (targetRect.Width - currentRect.Width);
					break;
				case ContentAlignment.BottomLeft:
					newRect.Y = (targetRect.Height - currentRect.Height);
					break;
				case ContentAlignment.BottomCenter:
					newRect.Y = (targetRect.Height - currentRect.Height);
					newRect.X = (targetRect.Width - currentRect.Width) / 2;
					break;
				case ContentAlignment.BottomRight:
					newRect.Y = (targetRect.Height - currentRect.Height);
					newRect.X = (targetRect.Width - currentRect.Width);
					break;
			}
			return newRect;
		}


		private void ApplyEffects(SurfaceOutputSettings printOutputSettings) {
			// TODO:
			// add effects here
			if (Conf.OutputPrintMonochrome) {
				byte threshold = Conf.OutputPrintMonochromeThreshold;
				printOutputSettings.Effects.Add(new MonochromeEffect(threshold));
				printOutputSettings.ReduceColors = true;
			}

			// the invert effect should probably be the last
			if (Conf.OutputPrintInverted) {
				printOutputSettings.Effects.Add(new InvertEffect());
			}
		}
	}
}
