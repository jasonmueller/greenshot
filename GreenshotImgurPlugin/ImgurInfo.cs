﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Drawing;
using System.Xml;
using log4net;

namespace GreenshotImgurPlugin
{
	/// <summary>
	/// Description of ImgurInfo.
	/// </summary>
	public class ImgurInfo : IDisposable {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(ImgurInfo));
		public string Hash { get; set; }

		private string _deleteHash;
		public string DeleteHash {
			get {return _deleteHash;}
			set {
				_deleteHash = value;
				DeletePage = "http://imgur.com/delete/" + value;
			}
		}

		public string Title { get; set; }

		public string ImageType { get; set; }

		public DateTimeOffset Timestamp { get; set; }

		public string Original { get; set; }

		public string Page { get; set; }

		public string SmallSquare { get; set; }

		public string LargeThumbnail { get; set; }

		public string DeletePage { get; set; }

		private Image _image;
		public Image Image {
			get {return _image;}
			set {
				if (_image != null) {
					_image.Dispose();
				}
				_image = value;
			}
		}

		/// <summary>
		/// The public accessible Dispose
		/// Will call the GarbageCollector to SuppressFinalize, preventing being cleaned twice
		/// </summary>
		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// This Dispose is called from the Dispose and the Destructor.
		/// When disposing==true all non-managed resources should be freed too!
		/// </summary>
		/// <param name="disposing"></param>
		protected virtual void Dispose(bool disposing) {
			if (disposing) {
				if (_image != null) {
					_image.Dispose();
				}
			}
			_image = null;
		}
		public static ImgurInfo ParseResponse(string response) {
			LOG.Debug(response);
			var imgurInfo = new ImgurInfo();
			try {
				var doc = new XmlDocument();
				doc.LoadXml(response);
				XmlNodeList nodes = doc.GetElementsByTagName("hash");
				if(nodes.Count > 0) {
					var xmlNode = nodes.Item(0);
					if (xmlNode != null) imgurInfo.Hash = xmlNode.InnerText;
				}
				nodes = doc.GetElementsByTagName("deletehash");
				if(nodes.Count > 0) {
					var xmlNode = nodes.Item(0);
					if (xmlNode != null) imgurInfo.DeleteHash = xmlNode.InnerText;
				}
				nodes = doc.GetElementsByTagName("type");
				if(nodes.Count > 0) {
					var xmlNode = nodes.Item(0);
					if (xmlNode != null) imgurInfo.ImageType = xmlNode.InnerText;
				}
				nodes = doc.GetElementsByTagName("title");
				if(nodes.Count > 0) {
					var xmlNode = nodes.Item(0);
					if (xmlNode != null) imgurInfo.Title = xmlNode.InnerText;
				}
				nodes = doc.GetElementsByTagName("DateTime");
				if(nodes.Count > 0) {
					var xmlNode = nodes.Item(0);
					if (xmlNode != null) imgurInfo.Timestamp =  DateTimeOffset.Parse(xmlNode.InnerText);
				}
				nodes = doc.GetElementsByTagName("original");
				if(nodes.Count > 0) {
					var xmlNode = nodes.Item(0);
					if (xmlNode != null) imgurInfo.Original = xmlNode.InnerText;
				}
				nodes = doc.GetElementsByTagName("imgur_page");
				if(nodes.Count > 0) {
					var xmlNode = nodes.Item(0);
					if (xmlNode != null) imgurInfo.Page = xmlNode.InnerText;
				}
				nodes = doc.GetElementsByTagName("small_square");
				if(nodes.Count > 0) {
					var xmlNode = nodes.Item(0);
					if (xmlNode != null) imgurInfo.SmallSquare = xmlNode.InnerText;
				}
				nodes = doc.GetElementsByTagName("large_thumbnail");
				if(nodes.Count > 0) {
					var xmlNode = nodes.Item(0);
					if (xmlNode != null) imgurInfo.LargeThumbnail = xmlNode.InnerText;
				}
			} catch(Exception e) {
				LOG.ErrorFormat("Could not parse Imgur response due to error {0}, response was: {1}", e.Message, response);
			}
			return imgurInfo;
		}
	}
}
