﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

using GreenshotPlugin.Core;
using GreenshotPlugin;
using Greenshot.Interop.Office;
using Greenshot.IniFile;
using log4net;

namespace GreenshotOfficePlugin {
	public class OneNoteDestination : AbstractDestination {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(WordDestination));
		public const string DESIGNATION = "OneNote";
		private static readonly string ExePath;
		private static readonly Image ApplicationIcon;
		private static readonly Image NotebookIcon;
		private readonly OneNotePage _page;

		static OneNoteDestination() {
			ExePath = PluginUtils.GetExePath("ONENOTE.EXE");
			if (ExePath != null && File.Exists(ExePath)) {
				ApplicationIcon = PluginUtils.GetExeIcon(ExePath, 0);
				NotebookIcon = PluginUtils.GetExeIcon(ExePath, 0);
				WindowDetails.AddProcessToExcludeFromFreeze("onenote");
			} else {
				ExePath = null;
			}
		}
		
		public OneNoteDestination() {
			
		}

		public OneNoteDestination(OneNotePage page) {
			_page = page;
		}

		public override string Designation {
			get {
				return DESIGNATION;
			}
		}

		public override string Description {
			get {
				if (_page == null) {
					return "Microsoft OneNote";
				}
				return _page.PageName;
			}
		}

		public override int Priority {
			get {
				return 4;
			}
		}
		
		public override bool IsDynamic {
			get {
				return true;
			}
		}

		public override bool IsActive {
			get {
				return base.IsActive && ExePath != null;
			}
		}

		public override Image DisplayIcon {
			get {
				return _page != null ? NotebookIcon : ApplicationIcon;
			}
		}

		public override IEnumerable<ILegacyDestination> DynamicDestinations() {
			foreach (var page in OneNoteExporter.GetPages()) {
				yield return new OneNoteDestination(page);
			}
		}

		public override ExportInformation ExportCapture(bool manuallyInitiated, ISurface surface, ICaptureDetails captureDetails) {
			var exportInformation = new ExportInformation(Designation, Description);
			if (_page == null) {
				return exportInformation;
			}
			try {
				OneNoteExporter.ExportToPage(surface, _page);
				exportInformation.ExportMade = true;
			} catch (Exception ex) {
				exportInformation.ErrorMessage = ex.Message;
				LOG.Error(ex);
			}
			return exportInformation;
		}
	}
}
