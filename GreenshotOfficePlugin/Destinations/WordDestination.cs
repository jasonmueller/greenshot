﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

using GreenshotPlugin.Core;
using GreenshotPlugin;
using Greenshot.Interop.Office;
using System.Text.RegularExpressions;
using log4net;

namespace GreenshotOfficePlugin {
	/// <summary>
	/// Description of EmailDestination.
	/// </summary>
	public class WordDestination : AbstractDestination {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(WordDestination));
		private static readonly string ExePath;
		private static readonly Image ApplicationIcon;
		private static readonly Image DocumentIcon;
		private readonly string _documentCaption;

		static WordDestination() {
			ExePath = PluginUtils.GetExePath("WINWORD.EXE");
			if (ExePath != null && File.Exists(ExePath)) {
				ApplicationIcon = PluginUtils.GetExeIcon(ExePath, 0);
				DocumentIcon = PluginUtils.GetExeIcon(ExePath, 1);
			} else {
				ExePath = null;
			}
		}
		
		public WordDestination() {
			
		}

		public WordDestination(string wordCaption) {
			_documentCaption = wordCaption;
		}

		public override string Designation {
			get {
				return "Word";
			}
		}

		public override string Description {
			get {
				if (_documentCaption == null) {
					return "Microsoft Word";
				}
				return _documentCaption;
			}
		}

		public override int Priority {
			get {
				return 4;
			}
		}
		
		public override bool IsDynamic {
			get {
				return true;
			}
		}

		public override bool IsActive {
			get {
				return base.IsActive && ExePath != null;
			}
		}

		public override Image DisplayIcon {
			get {
				if (!string.IsNullOrEmpty(_documentCaption)) {
					return DocumentIcon;
				}
				return ApplicationIcon;
			}
		}

		public override IEnumerable<ILegacyDestination> DynamicDestinations() {
			foreach (string wordCaption in WordExporter.GetWordDocuments()) {
				yield return new WordDestination(wordCaption);
			}
		}

		public override ExportInformation ExportCapture(bool manuallyInitiated, ISurface surface, ICaptureDetails captureDetails) {
			ExportInformation exportInformation = new ExportInformation(Designation, Description);
			string tmpFile = captureDetails.Filename;
			if (tmpFile == null || surface.Modified || !Regex.IsMatch(tmpFile, @".*(\.png|\.gif|\.jpg|\.jpeg|\.tiff|\.bmp)$")) {
				tmpFile = ImageOutput.SaveNamedTmpFile(surface, captureDetails, new SurfaceOutputSettings().PreventGreenshotFormat());
			}
			if (_documentCaption != null) {
				try {
					WordExporter.InsertIntoExistingDocument(_documentCaption, tmpFile);
					exportInformation.ExportMade = true;
				} catch (Exception) {
					try {
						WordExporter.InsertIntoExistingDocument(_documentCaption, tmpFile);
						exportInformation.ExportMade = true;
					} catch (Exception ex) {
						LOG.Error(ex);
						// TODO: Change to general logic in ProcessExport
						surface.SendMessageEvent(this, SurfaceMessageTyp.Error, Language.GetFormattedString("destination_exportfailed", Description));
					}
				}
			} else {
				if (!manuallyInitiated) {
					List<string> documents = WordExporter.GetWordDocuments();
					if (documents != null && documents.Count > 0) {
						List<ILegacyDestination> destinations = new List<ILegacyDestination>();
						destinations.Add(new WordDestination());
						foreach (string document in documents) {
							destinations.Add(new WordDestination(document));
						}
						// Return the ExportInformation from the picker without processing, as this indirectly comes from us self
						return ShowPickerMenu(false, surface, captureDetails, destinations);
					}
				}
				try {
					WordExporter.InsertIntoNewDocument(tmpFile, null, null);
					exportInformation.ExportMade = true;
				} catch(Exception) {
					// Retry once, just in case
					try {
						WordExporter.InsertIntoNewDocument(tmpFile, null, null);
						exportInformation.ExportMade = true;
					} catch (Exception ex) {
						LOG.Error(ex);
						// TODO: Change to general logic in ProcessExport
						surface.SendMessageEvent(this, SurfaceMessageTyp.Error, Language.GetFormattedString("destination_exportfailed", Description));
					}
				}
			}
			ProcessExport(exportInformation, surface);
			return exportInformation;
		}
	}
}
