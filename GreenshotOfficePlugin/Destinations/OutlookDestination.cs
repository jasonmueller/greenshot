﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Greenshot.Interop.Office;
using GreenshotPlugin;
using GreenshotPlugin.Core;
using Greenshot.IniFile;
using log4net;

namespace GreenshotOfficePlugin {
	/// <summary>
	/// Description of OutlookDestination.
	/// </summary>
	public class OutlookDestination : AbstractDestination {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(OutlookDestination));
		private static readonly OfficeConfiguration Conf = IniConfig.GetIniSection<OfficeConfiguration>();
		private static readonly string ExePath;
		private static readonly Image ApplicationIcon;
		private static readonly Image MailIcon = GreenshotResources.GetImage("Email.Image");
		private static Image _meetingIcon;
		private static readonly bool IsActiveFlag;
		private const string MapiClient = "Microsoft Outlook";
		public const string DESIGNATION = "Outlook";
		private readonly string _outlookInspectorCaption;
		private readonly OlObjectClass _outlookInspectorType;

		static OutlookDestination() {
			if (EmailConfigHelper.HasOutlook()) {
				IsActiveFlag = true;
			}
			ExePath = PluginUtils.GetExePath("OUTLOOK.EXE");
			if (ExePath != null && File.Exists(ExePath)) {
				ApplicationIcon = PluginUtils.GetExeIcon(ExePath, 0);
				WindowDetails.AddProcessToExcludeFromFreeze("outlook");
				if (Conf.OutlookAllowExportInMeetings) {
					_meetingIcon = PluginUtils.GetExeIcon(ExePath, 2);
				}
			} else {
				ExePath = null;
			}
			if (ExePath == null) {
				IsActiveFlag = false;
			}
		}

		public OutlookDestination() {
		}

		public OutlookDestination(string outlookInspectorCaption, OlObjectClass outlookInspectorType) {
			_outlookInspectorCaption = outlookInspectorCaption;
			_outlookInspectorType = outlookInspectorType;
		}

		public override string Designation {
			get {
				return DESIGNATION;
			}
		}

		public override string Description {
			get {
				if (_outlookInspectorCaption == null) {
					return MapiClient;
				}
				return _outlookInspectorCaption;
			}
		}

		public override int Priority {
			get {
				return 3;
			}
		}

		public override bool IsActive {
			get {
				return base.IsActive && IsActiveFlag;
			}
		}

		public override bool IsDynamic {
			get {
				return true;
			}
		}

		public override Keys EditorShortcutKeys {
			get {
				return Keys.Control | Keys.E;
			}
		}

		public override Image DisplayIcon {
			get {
				if (_outlookInspectorCaption != null) {
					if (OlObjectClass.olAppointment.Equals(_outlookInspectorType)) {
						// Make sure we loaded the icon, maybe the configuration has been changed!
						if (_meetingIcon == null) {
							_meetingIcon = PluginUtils.GetExeIcon(ExePath, 2);
						}
						return _meetingIcon;
					}
					return MailIcon;
				}
				return ApplicationIcon;
			}
		}
		
		public override IEnumerable<ILegacyDestination> DynamicDestinations() {
			Dictionary<string, OlObjectClass> inspectorCaptions = OutlookEmailExporter.RetrievePossibleTargets(Conf.OutlookAllowExportInMeetings);
			if (inspectorCaptions != null) {
				foreach (string inspectorCaption in inspectorCaptions.Keys) {
					yield return new OutlookDestination(inspectorCaption, inspectorCaptions[inspectorCaption]);
				}
			}
		}

		/// <summary>
		/// Export the capture to outlook
		/// </summary>
		/// <param name="manuallyInitiated"></param>
		/// <param name="surface"></param>
		/// <param name="captureDetails"></param>
		/// <returns></returns>
		public override ExportInformation ExportCapture(bool manuallyInitiated, ISurface surface, ICaptureDetails captureDetails) {
			ExportInformation exportInformation = new ExportInformation(Designation, Description);
			// Outlook logic
			string tmpFile = captureDetails.Filename;
			if (tmpFile == null || surface.Modified || !Regex.IsMatch(tmpFile, @".*(\.png|\.gif|\.jpg|\.jpeg|\.tiff|\.bmp)$")) {
				tmpFile = ImageOutput.SaveNamedTmpFile(surface, captureDetails, new SurfaceOutputSettings().PreventGreenshotFormat());
			} else {
				LOG.InfoFormat("Using already available file: {0}", tmpFile);
			}

			// Create a attachment name for the image
			string attachmentName = captureDetails.Title;
			if (!string.IsNullOrEmpty(attachmentName)) {
				attachmentName = attachmentName.Trim();
			}
			// Set default if non is set
			if (string.IsNullOrEmpty(attachmentName)) {
				attachmentName = "Greenshot Capture";
			}
			// Make sure it's "clean" so it doesn't corrupt the header
			attachmentName = Regex.Replace(attachmentName, @"[^\x20\d\w]", "");

			if (_outlookInspectorCaption != null) {
				OutlookEmailExporter.ExportToInspector(_outlookInspectorCaption, tmpFile, attachmentName);
				exportInformation.ExportMade = true;
			} else {
				if (!manuallyInitiated) {
					Dictionary<string, OlObjectClass> inspectorCaptions = OutlookEmailExporter.RetrievePossibleTargets(Conf.OutlookAllowExportInMeetings);
					if (inspectorCaptions != null && inspectorCaptions.Count > 0) {
						List<ILegacyDestination> destinations = new List<ILegacyDestination> {
							new OutlookDestination()
						};
						foreach (string inspectorCaption in inspectorCaptions.Keys) {
							destinations.Add(new OutlookDestination(inspectorCaption, inspectorCaptions[inspectorCaption]));
						}
						// Return the ExportInformation from the picker without processing, as this indirectly comes from us self
						return ShowPickerMenu(false, surface, captureDetails, destinations);
					}
				}
				exportInformation.ExportMade = OutlookEmailExporter.ExportToOutlook(Conf.OutlookEmailFormat, tmpFile, FilenameHelper.FillPattern(Conf.EmailSubjectPattern, captureDetails, false), attachmentName, Conf.EmailTo, Conf.EmailCC, Conf.EmailBCC, null);
			}
			ProcessExport(exportInformation, surface);
			return exportInformation;
		}
	}
}