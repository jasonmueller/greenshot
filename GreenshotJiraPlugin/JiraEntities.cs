﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace GreenshotJiraPlugin {
	[DataContract]
	public class Error {
		[DataMember(Name = "errorMessages", EmitDefaultValue = false)]
		public List<string> ErrorMessages {
			get;
			set;
		}
		[DataMember(Name = "errors", EmitDefaultValue = false)]
		public IDictionary<string, string> Errors {
			get;
			set;
		}
	}

	[DataContract]
	public class FilterFavourite {
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}
		[DataMember(Name = "name", EmitDefaultValue = false)]
		public string Name {
			get;
			set;
		}
		[DataMember(Name = "jql", EmitDefaultValue = false)]
		public string Jql {
			get;
			set;
		}
		[DataMember(Name = "description", EmitDefaultValue = false)]
		public string Description {
			get;
			set;
		}
	}

	[DataContract]
	public class Resolution {
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}
		[DataMember(Name = "description", EmitDefaultValue = false)]
		public string Description {
			get;
			set;
		}
		[DataMember(Name = "name", EmitDefaultValue = false)]
		public string Name {
			get;
			set;
		}
	}

	[DataContract]
	public class Priority {
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}
		[DataMember(Name = "iconUrl", EmitDefaultValue = false)]
		public string IconUrl {
			get;
			set;
		}
		[DataMember(Name = "name", EmitDefaultValue = false)]
		public string Name {
			get;
			set;
		}
	}

	[DataContract]
	public class Component {
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}
		[DataMember(Name = "name", EmitDefaultValue = false)]
		public string Name {
			get;
			set;
		}
	}

	[DataContract]
	public class Status {
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}
		[DataMember(Name = "iconUrl", EmitDefaultValue = false)]
		public string IconUrl {
			get;
			set;
		}
		[DataMember(Name = "name", EmitDefaultValue = false)]
		public string Name {
			get;
			set;
		}
		[DataMember(Name = "description", EmitDefaultValue = false)]
		public string Description {
			get;
			set;
		}
	}

	[DataContract]
	public class Project {
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}
		[DataMember(Name = "key", EmitDefaultValue = false)]
		public string Key {
			get;
			set;
		}
		[DataMember(Name = "name", EmitDefaultValue = false)]
		public string Name {
			get;
			set;
		}
	}

	[DataContract]
	public class IssueType {
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}
		[DataMember(Name = "iconUrl", EmitDefaultValue = false)]
		public string IconUrl {
			get;
			set;
		}
		[DataMember(Name = "name", EmitDefaultValue = false)]
		public string Name {
			get;
			set;
		}
		[DataMember(Name = "description", EmitDefaultValue = false)]
		public string Description {
			get;
			set;
		}
		[DataMember(Name = "subtask", EmitDefaultValue = false)]
		public bool Subtask {
			get;
			set;
		}
	}

	[DataContract]
	public class User {
		[DataMember(Name = "name", EmitDefaultValue = false)]
		public string Name {
			get;
			set;
		}
		[DataMember(Name = "displayName", EmitDefaultValue = false)]
		public string DisplayName {
			get;
			set;
		}
		[DataMember(Name = "active", EmitDefaultValue = false)]
		public bool Active {
			get;
			set;
		}
	}

	[DataContract]
	public class Fields {
		[DataMember(Name = "project", EmitDefaultValue = false)]
		public Project Project {
			get;
			set;
		}

		[DataMember(Name = "summary", EmitDefaultValue = false)]
		public string Summary {
			get;
			set;
		}

		[DataMember(Name = "description", EmitDefaultValue = false)]
		public string Description {
			get;
			set;
		}

		[DataMember(Name = "environment", EmitDefaultValue = false)]
		public string Environment {
			get;
			set;
		}

		[DataMember(Name = "attachment", EmitDefaultValue = false)]
		public List<Attachment> Attachment {
			get;
			set;
		}

		[DataMember(Name = "reporter", EmitDefaultValue = false)]
		public User Reporter {
			get;
			set;
		}

		[DataMember(Name = "assignee", EmitDefaultValue = false)]
		public User Assignee {
			get;
			set;
		}

		[DataMember(Name = "priority", EmitDefaultValue = false)]
		public Priority Priority {
			get;
			set;
		}

		[DataMember(Name = "status", EmitDefaultValue = false)]
		public Status Status {
			get;
			set;
		}

		[DataMember(Name = "issuetype", EmitDefaultValue = false)]
		public IssueType IssueType {
			get;
			set;
		}

		[DataMember(Name = "components", EmitDefaultValue = false)]
		public List<Component> Components {
			get;
			set;
		}

		[DataMember(Name = "labels", EmitDefaultValue = false)]
		public List<string> Labels {
			get;
			set;
		}

		[DataMember(Name = "comment", EmitDefaultValue = false)]
		public CommentContainer comment {
			get;
			set;
		}

		[DataMember(Name = "resolution", EmitDefaultValue = false)]
		public Resolution Resolution {
			get;
			set;
		}

		[DataMember(Name = "resolutiondate", EmitDefaultValue = false)]
		private String resolutiondate;

		[IgnoreDataMember]
		public DateTimeOffset Resolutiondate {
			get {
				if (resolutiondate != null) {
					return DateTimeOffset.Parse(resolutiondate, null, DateTimeStyles.RoundtripKind);
				}
				return DateTimeOffset.MinValue;
			}
			set {
				if (value != null) {
					resolutiondate = value.ToUniversalTime().ToString("o");
				} else {
					resolutiondate = null;
				}
			}
		}

		[DataMember(Name = "updated", EmitDefaultValue = false)]
		private String updated;

		[IgnoreDataMember]
		public DateTimeOffset Updated {
			get {
				if (resolutiondate != null) {
					return DateTimeOffset.Parse(updated, null, DateTimeStyles.RoundtripKind);
				}
				return DateTimeOffset.MinValue;
			}
			set {
				if (value != null) {
					updated = value.ToUniversalTime().ToString("o");
				} else {
					updated = null;
				}
			}
		}

		[DataMember(Name = "created", EmitDefaultValue = false)]
		private String created;

		[IgnoreDataMember]
		public DateTimeOffset Created {
			get {
				if (created != null) {
					return DateTimeOffset.Parse(created, null, DateTimeStyles.RoundtripKind);
				}
				return DateTimeOffset.MinValue;
			}
			set {
				if (value != null) {
					created = value.ToUniversalTime().ToString("o");
				} else {
					created = null;
				}
			}
		}
	}

	[DataContract]
	public class CommentContainer {
		[DataMember(Name = "comments", EmitDefaultValue = false)]
		public List<Comment> Comments {
			get;
			set;
		}
	}

	[DataContract]
	public class Comment {
		[DataMember(Name = "body", EmitDefaultValue = false)]
		public string Body {
			get;
			set;
		}

		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}

		[DataMember(Name = "author", EmitDefaultValue = false)]
		public User Author {
			get;
			set;
		}

		[DataMember(Name = "updateAuthor", EmitDefaultValue = false)]
		public User UpdateAuthor {
			get;
			set;
		}

		[DataMember(Name = "created", EmitDefaultValue = false)]
		private String created;

		[IgnoreDataMember]
		public DateTimeOffset Created {
			get {
				if (created != null) {
					return DateTimeOffset.Parse(created, null, DateTimeStyles.RoundtripKind);
				}
				return DateTimeOffset.MinValue;
			}
			set {
				if (value != null) {
					created = value.ToUniversalTime().ToString("o");
				} else {
					created = null;
				}
			}
		}

		[DataMember(Name = "updated", EmitDefaultValue = false)]
		private String updated;

		[IgnoreDataMember]
		public DateTimeOffset Updated {
			get {
				if (updated != null) {
					return DateTimeOffset.Parse(updated, null, DateTimeStyles.RoundtripKind);
				}
				return DateTimeOffset.MinValue;
			}
			set {
				if (value != null) {
					updated = value.ToUniversalTime().ToString("o");
				} else {
					updated = null;
				}
			}
		}
	}

	[DataContract]
	public class SearchResult {
		[DataMember(Name = "total", EmitDefaultValue = false)]
		public int Total {
			get;
			set;
		}
		[DataMember(Name = "issues", EmitDefaultValue = false)]
		public List<Issue> Issues {
			get;
			set;
		}
	}

	[DataContract]
	public class Attachment {
		[DataMember(Name = "filename", EmitDefaultValue = false)]
		public string Filename {
			get;
			set;
		}
		[DataMember(Name = "mimeType", EmitDefaultValue = false)]
		public string MimeType {
			get;
			set;
		}
		[DataMember(Name = "size", EmitDefaultValue = false)]
		public int Size {
			get;
			set;
		}
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}
		[DataMember(Name = "content", EmitDefaultValue = false)]
		public string Content {
			get;
			set;
		}
		[DataMember(Name = "thumbnail", EmitDefaultValue = false)]
		public string Thumbnail {
			get;
			set;
		}

		[DataMember(Name = "author", EmitDefaultValue = false)]
		public User Author {
			get;
			set;
		}
	}

	[DataContract]
	public class Issue {
		[DataMember(Name = "id", EmitDefaultValue = false)]
		public string Id {
			get;
			set;
		}
		[DataMember(Name = "key", EmitDefaultValue = false)]
		public string Key {
			get;
			set;
		}
		[DataMember(Name = "fields", EmitDefaultValue = false)]
		public Fields Fields {
			get;
			set;
		}
	}
}
