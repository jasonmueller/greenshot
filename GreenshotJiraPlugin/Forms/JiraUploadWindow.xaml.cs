﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GreenshotJiraPlugin {
	/// <summary>
	/// Interaction logic for JiraUploadWindow.xaml
	/// </summary>
	public partial class JiraUploadWindow : Window {
		public List<FilterFavourite> Filters {
			get;
			private set;
		}
		public ObservableCollection<Issue> Issues {
			get;
			private set;
		}

		public JiraUploadWindow(JiraRest jira) {
			Filters = jira.Filters().Result;
			Issues = new ObservableCollection<Issue>();
			var searchResult = jira.Find(Filters[0].Jql).Result;
			foreach (var issue in searchResult.Issues) {
				Issues.Add(issue);
			}

			InitializeComponent();
			DataContext = this;
		}
	}
}
