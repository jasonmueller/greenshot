/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Globalization;
using System.Windows.Forms;

using GreenshotPlugin.Controls;
using GreenshotPlugin.Core;
using Greenshot.IniFile;
using System.Collections.Generic;
using System.Threading;

namespace GreenshotJiraPlugin {
	public partial class JiraForm : Form {
		private readonly JiraRest _jiraConnector;
		private Issue _selectedIssue;
		private readonly GreenshotColumnSorter _columnSorter;
		private readonly JiraConfiguration _config = IniConfig.GetIniSection<JiraConfiguration>();

		public JiraForm(JiraRest jiraConnector) {
			InitializeComponent();
			Icon = GreenshotResources.GetGreenshotIcon();
			InitializeComponentText();

			_columnSorter = new GreenshotColumnSorter();
			jiraListView.ListViewItemSorter = _columnSorter;

			_jiraConnector = jiraConnector;

			ChangeModus(false);
			uploadButton.Enabled = false;
			UpdateForm();
		}

		private void InitializeComponentText() {
			label_jirafilter.Text = Language.GetString("jira", LangKey.label_jirafilter);
			label_comment.Text = Language.GetString("jira", LangKey.label_comment);
			label_filename.Text = Language.GetString("jira", LangKey.label_filename);
		}

		private void UpdateForm() {
			var filters = _jiraConnector.Filters().Result;
			if (filters != null) {
				foreach (var filter in filters) {
					jiraFilterBox.Items.Add(filter);
				}
				jiraFilterBox.SelectedIndex = 0;
			}
			ChangeModus(true);
			if (!string.IsNullOrEmpty(_config.LastUsedJira)) {
				_selectedIssue = _jiraConnector.Details(_config.LastUsedJira).Result;
				if (_selectedIssue != null) {
					jiraKey.Text = _config.LastUsedJira;
					uploadButton.Enabled = true;
				}
			}
		}

		private void ChangeModus(bool enabled) {
			jiraFilterBox.Enabled = enabled;
			jiraListView.Enabled = enabled;
			jiraFilenameBox.Enabled = enabled;
			jiraCommentBox.Enabled = enabled;
		}

		public void SetFilename(string filename) {
			jiraFilenameBox.Text = filename;
		}

		public void SetComment(string comment) {
			jiraCommentBox.Text = comment;
		}

		public Issue GetJiraIssue() {
			return _selectedIssue;
		}

		public void Upload(IBinaryContainer attachment) {
			_config.LastUsedJira = _selectedIssue.Key;
			_jiraConnector.Attach(_selectedIssue.Key, jiraFilenameBox.Text, attachment, new CancellationToken());
			if (!string.IsNullOrEmpty(jiraCommentBox.Text)) {
				_jiraConnector.Comment(_selectedIssue.Key, jiraCommentBox.Text);
			}
		}

		private void jiraFilterBox_SelectedIndexChanged(object sender, EventArgs e) {
			List<Issue> issues = null;
			uploadButton.Enabled = false;
			var filter = (FilterFavourite)jiraFilterBox.SelectedItem;
			if (filter == null) {
				return;
			}
			// Run upload in the background
			new PleaseWaitForm().ShowAndWait("Jira plug-in", Language.GetString("jira", LangKey.communication_wait),
				delegate {
					issues = _jiraConnector.Find(filter.Jql).Result.Issues;
				}
			);
			jiraListView.BeginUpdate();
			jiraListView.Items.Clear();
			if (issues != null && issues.Count > 0) {
				jiraListView.Columns.Clear();
				LangKey[] columns = { LangKey.column_id, LangKey.column_created, LangKey.column_assignee, LangKey.column_reporter, LangKey.column_summary };
				foreach (LangKey column in columns) {
					jiraListView.Columns.Add(Language.GetString("jira", column));
				}
				foreach (Issue issue in issues) {
					ListViewItem item = new ListViewItem(issue.Key);
					item.Tag = issue;
					item.SubItems.Add(issue.Fields.Updated.ToLocalTime().ToString("d", DateTimeFormatInfo.InvariantInfo));
					if (issue.Fields.Assignee != null) {
						item.SubItems.Add(issue.Fields.Assignee.DisplayName);
					} else {
						item.SubItems.Add("n.a.");
					}
					if (issue.Fields.Reporter != null) {
						item.SubItems.Add(issue.Fields.Reporter.DisplayName);
					} else {
						item.SubItems.Add("n.a.");
					}
					item.SubItems.Add(issue.Fields.Summary);
					jiraListView.Items.Add(item);
				}
				for (int i = 0; i < columns.Length; i++) {
					jiraListView.AutoResizeColumn(i, ColumnHeaderAutoResizeStyle.ColumnContent);
				}
			}
			jiraListView.EndUpdate();
			jiraListView.Refresh();
		}

		private void jiraListView_SelectedIndexChanged(object sender, EventArgs e) {
			if (jiraListView.SelectedItems.Count > 0) {
				_selectedIssue = (Issue)jiraListView.SelectedItems[0].Tag;
				jiraKey.Text = _selectedIssue.Key;
				uploadButton.Enabled = true;
			} else {
				uploadButton.Enabled = false;
			}
		}

		private void uploadButton_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.OK;
		}

		private void cancelButton_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
		}

		private void jiraListView_ColumnClick(object sender, ColumnClickEventArgs e) {
			// Determine if clicked column is already the column that is being sorted.
			if (e.Column == _columnSorter.SortColumn) {
				// Reverse the current sort direction for this column.
				if (_columnSorter.Order == SortOrder.Ascending) {
					_columnSorter.Order = SortOrder.Descending;
				} else {
					_columnSorter.Order = SortOrder.Ascending;
				}
			} else {
				// Set the column number that is to be sorted; default to ascending.
				_columnSorter.SortColumn = e.Column;
				_columnSorter.Order = SortOrder.Ascending;
			}

			// Perform the sort with these new sort options.
			jiraListView.Sort();
		}

		void JiraKeyTextChanged(object sender, EventArgs e) {
			string jiranumber = jiraKey.Text;
			uploadButton.Enabled = false;
			int dashIndex = jiranumber.IndexOf('-');
			if (dashIndex > 0 && jiranumber.Length > dashIndex+1) {
				_selectedIssue = _jiraConnector.Details(jiraKey.Text).Result;
				if (_selectedIssue != null) {
					uploadButton.Enabled = true;
				}
			}
		}
	}
}
