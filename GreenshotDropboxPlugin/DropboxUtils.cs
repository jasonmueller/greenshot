﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom, Francis Noel
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Drawing;
using Greenshot.IniFile;
using GreenshotPlugin;
using GreenshotPlugin.Core;
using System.Runtime.Serialization;
using log4net;

namespace GreenshotDropboxPlugin {
	/// <summary>
	/// Description of DropboxUtils.
	/// </summary>
	public class DropboxUtils {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(DropboxUtils));
		private static readonly DropboxPluginConfiguration Config = IniConfig.GetIniSection<DropboxPluginConfiguration>();

		/// <summary>
		/// Class used to parse the json response
		/// </summary>
		[DataContract]
		internal class DropboxResponse {
			[DataMember]
			public string Url {
				get;
				set;
			}
		}

		private DropboxUtils() {
		}

		public static string UploadToDropbox(ISurface surfaceToUpload, SurfaceOutputSettings outputSettings, string filename) {
			OAuthSession oAuth = new OAuthSession(Config.ConsumerKey, Config.ConsumerSecret) {
				BrowserSize = new Size(1080, 650),
				CheckVerifier = false,
				AccessTokenUrl = "https://api.dropbox.com/1/oauth/access_token",
				AuthorizeUrl = "https://api.dropbox.com/1/oauth/authorize",
				RequestTokenUrl = "https://api.dropbox.com/1/oauth/request_token",
				LoginTitle = "Dropbox authorization",
				Token = Config.DropboxToken,
				TokenSecret = Config.DropboxTokenSecret
			};

			try {
				SurfaceContainer imageToUpload = new SurfaceContainer(surfaceToUpload, outputSettings, filename);
				string uploadResponse = oAuth.MakeOAuthRequest(HTTPMethod.POST, "https://api-content.dropbox.com/1/files_put/sandbox/" + OAuthSession.UrlEncode3986(filename), null, null, imageToUpload);
				LOG.DebugFormat("Upload response: {0}", uploadResponse);
			} catch (Exception ex) {
				LOG.Error("Upload error: ", ex);
				throw;
			} finally {
				if (!string.IsNullOrEmpty(oAuth.Token)) {
					Config.DropboxToken = oAuth.Token;
				}
				if (!string.IsNullOrEmpty(oAuth.TokenSecret)) {
					Config.DropboxTokenSecret = oAuth.TokenSecret;
				}
			}

			// Try to get a URL to the uploaded image
			try {
				string responseString = oAuth.MakeOAuthRequest(HTTPMethod.GET, "https://api.dropbox.com/1/shares/sandbox/" + OAuthSession.UrlEncode3986(filename), null, null, null);
				if (responseString != null) {
					LOG.DebugFormat("Parsing output: {0}", responseString);
					DropboxResponse response = JSONSerializer.Deserialize<DropboxResponse>(responseString);
					return response.Url;
				}
			} catch (Exception ex) {
				LOG.Error("Can't parse response.", ex);
			}
			return null;
 		}
	}
}
