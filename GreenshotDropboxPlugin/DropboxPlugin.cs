/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom, Francis Noel
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;
using Greenshot.IniFile;
using GreenshotPlugin;
using GreenshotPlugin.Controls;
using GreenshotPlugin.Core;
using System.ComponentModel.Composition;
using System.Windows;
using log4net;

namespace GreenshotDropboxPlugin {
	/// <summary>
	/// This is the Dropbox base code
	/// </summary>
	[Export(typeof(IGreenshotPlugin))]
	[ExportMetadata("name", "Dropbox plug-in")]
	public class DropboxPlugin : AbstractPlugin {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(DropboxPlugin));
		private static DropboxPluginConfiguration _config;

		public DropboxPlugin() {
		}

		public override IEnumerable<ILegacyDestination> Destinations() {
			yield return new DropboxDestination(this);
		}

		/// <summary>
		/// Implementation of the IGreenshotPlugin.Initialize
		/// </summary>
		/// <param name="pluginHost">Use the IGreenshotPluginHost interface to register events</param>
		/// <param name="metadata">IDictionary string, object</param>
		/// <returns>true if plugin is initialized, false if not (doesn't show)</returns>
		public override bool Initialize(IGreenshotHost pluginHost, IDictionary<string, object> metadata) {
			// Register configuration (don't need the configuration itself)
			_config = IniConfig.GetIniSection<DropboxPluginConfiguration>();
			return true;
		}

		public override void Shutdown() {
			LOG.Debug("Dropbox Plugin shutdown.");
		}

		/// <summary>
		/// This will be called when the menu item in the Editor is clicked
		/// </summary>
		public bool Upload(ICaptureDetails captureDetails, ISurface surfaceToUpload, out string uploadUrl) {
			uploadUrl = null;
			SurfaceOutputSettings outputSettings = new SurfaceOutputSettings(_config.UploadFormat, _config.UploadJpegQuality, false);
			try {
				string dropboxUrl = null;
				PleaseWaitWrapper.ShowAndWait("Dropbox plug-in", Language.GetString("dropbox", LangKey.communication_wait), 
					delegate {
						string filename = Path.GetFileName(FilenameHelper.GetFilename(_config.UploadFormat, captureDetails));
						dropboxUrl = DropboxUtils.UploadToDropbox(surfaceToUpload, outputSettings, filename);
					}
				);
				if (dropboxUrl == null) {
					return false;
				}
				uploadUrl = dropboxUrl;
				return true;
			} catch (Exception e) {
				LOG.Error("Error uploading.", e);
				MessageBox.Show(Language.GetString("dropbox", LangKey.upload_failure) + " " + e.Message);
				return false;
			}
		}
	}
}
