﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Greenshot.IniFile;
using GreenshotPlugin.Core;
using log4net;

namespace ExternalCommand {
	public static class IconCache {
		private static readonly Dictionary<string, Image> iconCache = new Dictionary<string, Image>();
		private static ExternalCommandConfiguration config = IniConfig.GetIniSection<ExternalCommandConfiguration>();
		private static readonly ILog LOG = LogManager.GetLogger(typeof(IconCache));

		public static Image IconForCommand(ExternalCommandData command) {
			if (!iconCache.ContainsKey(command.Name)) {
				Image icon = null;
				if (File.Exists(command.Commandline)) {
					try {
						icon = PluginUtils.GetExeIcon(command.Commandline, 0);
					} catch (Exception ex) {
						LOG.Warn("Problem loading icon for " + command.Commandline, ex);
					}
				}
				// Also add null to the cache if nothing is found
				iconCache.Add(command.Name, icon);
			}
			if (iconCache.ContainsKey(command.Name)) {
				return iconCache[command.Name];
			}
			return null;
		}
	}
}
