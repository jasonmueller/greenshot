﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;
using GreenshotInterop.Interop;
using System.Runtime.InteropServices;

namespace GreenshotPlugin.Controls {
	// Provides data for the WebBrowser2.NavigateError event.
	public class WebBrowserNavigateErrorEventArgs : EventArgs {
		public WebBrowserNavigateErrorEventArgs(
			String url, String frame, Int32 statusCode, Boolean cancel) {
			Url = url;
			Frame = frame;
			StatusCode = statusCode;
			Cancel = cancel;
		}

		public string Url { get; set; }

		public string Frame { get; set; }

		public int StatusCode { get; set; }

		public bool Cancel { get; set; }
	}

	public class ExtendedWebBrowser : WebBrowser {
		protected class ExtendedWebBrowserSite : WebBrowserSite, IOleCommandTarget {
			const int OLECMDID_SHOWSCRIPTERROR = 40;
			const int OLECMDID_SHOWMESSAGE = 41;

			static readonly Guid CGID_DocHostCommandHandler = new Guid("F38BC242-B950-11D1-8918-00C04FC2C836");

			const int S_OK = 0;
			const int OLECMDERR_E_NOTSUPPORTED = (-2147221248);
			const int OLECMDERR_E_UNKNOWNGROUP = (-2147221244);

			public ExtendedWebBrowserSite(WebBrowser wb) : base(wb) {
			}

			#region IOleCommandTarget Members
			public int QueryStatus(Guid pguidCmdGroup, int cCmds, IntPtr prgCmds, IntPtr pCmdText) {
				return OLECMDERR_E_NOTSUPPORTED;
			}

			public int Exec(Guid pguidCmdGroup, int cmdId, int cmdExecOpt, IntPtr pvaIn, IntPtr pvaOut) {
				if (pguidCmdGroup == CGID_DocHostCommandHandler) {
					if (cmdId == OLECMDID_SHOWSCRIPTERROR) {
						// do not need to alter pvaOut as the docs says, enough to return S_OK here
						return S_OK;
					}
				}

				return OLECMDERR_E_NOTSUPPORTED;
			}

			#endregion
		}

		// Imports the NavigateError method from the OLE DWebBrowserEvents2 interface. 
		// See: http://pinvoke.net/default.aspx/Interfaces.DWebBrowserEvents2
		[ComImport, Guid("34A715A0-6587-11D0-924A-0020AFC7AC4D"), InterfaceType(ComInterfaceType.InterfaceIsIDispatch), TypeLibType(TypeLibTypeFlags.FHidden)]
		public interface DWebBrowserEvents2 {
			[DispId(271)]
			void NavigateError(
				[In, MarshalAs(UnmanagedType.IDispatch)] object pDisp,
				[In] ref object url, [In] ref object frame,
				[In] ref object statusCode, [In, Out] ref bool cancel);
			[DispId(250)]
			void BeforeNavigate2(
				[MarshalAs(UnmanagedType.IDispatch)] object pDisp,
				[In] ref object url,
				[In] ref object flags,
				[In] ref object targetFrameName,
				[In] ref object postData,
				[In] ref object headers,
				[In, Out, MarshalAs(UnmanagedType.VariantBool)] ref bool cancel);
		}

		// Handles the NavigateError event from the underlying ActiveX 
		// control by raising the NavigateError event defined in this class.
		private class WebBrowser2EventHelper : StandardOleMarshalObject, DWebBrowserEvents2 {
			private readonly ExtendedWebBrowser _parent;

			public WebBrowser2EventHelper(ExtendedWebBrowser parent) {
				_parent = parent;
			}

			public void NavigateError(object pDisp, ref object url, ref object frame, ref object statusCode, ref bool cancel) {
				// Raise the NavigateError event.
				_parent.OnNavigateError(new WebBrowserNavigateErrorEventArgs((String)url, (String)frame, (Int32)statusCode, cancel));
			}

			public void BeforeNavigate2(object pDisp, ref object url, ref object flags, ref object targetFrameName, ref object postData, ref object headers, ref bool cancel) {
				Debug.WriteLine((String)url);
			}
		}

		// 

		/// <summary>
		/// Represents the method that will handle the WebBrowser2.NavigateError event.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void WebBrowserNavigateErrorEventHandler(object sender, WebBrowserNavigateErrorEventArgs e);
		public event WebBrowserNavigateErrorEventHandler NavigateError;

		AxHost.ConnectionPointCookie _cookie;
		WebBrowser2EventHelper _helper;

		// Raises the NavigateError event.
		protected virtual void OnNavigateError(WebBrowserNavigateErrorEventArgs e) {
			if (NavigateError != null) {
				NavigateError(this, e);
			}
		}

		[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
		protected override void CreateSink() {
			base.CreateSink();

			// Create an instance of the client that will handle the event
			// and associate it with the underlying ActiveX control.
			_helper = new WebBrowser2EventHelper(this);
			_cookie = new AxHost.ConnectionPointCookie(ActiveXInstance, _helper, typeof(DWebBrowserEvents2));
		}

		[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
		protected override void DetachSink() {
			// Disconnect the client that handles the event
			// from the underlying ActiveX control.
			if (_cookie != null) {
				_cookie.Disconnect();
				_cookie = null;
			}
			base.DetachSink();
		}
		protected override WebBrowserSiteBase CreateWebBrowserSiteBase() {
			return new ExtendedWebBrowserSite(this);
		}

		public ExtendedWebBrowser() {
		}
	}
}