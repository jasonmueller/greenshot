﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using GreenshotPlugin.Core;
using Greenshot.IniFile;

namespace GreenshotPlugin.Modules {
	/// <summary>
	/// This class contains everything on a capture, from the moment of the capture itself to the export
	/// </summary>
	public class CaptureContext : ICaptureDetails {
		private static readonly CoreConfiguration Conf = IniConfig.GetIniSection<CoreConfiguration>();

		private readonly Dictionary<string, string> _metaData = new Dictionary<string, string>();
		private string _filenamePattern;
		private string _filename;
		private string _title;
		private OutputFormat _format = Conf.OutputFileFormat;
		private DateTimeOffset _captureTaken = new DateTimeOffset();

		public IElement<BitmapSource> Capture {
			get;
			set;
		}
		public IElement<BitmapSource> MouseCursor {
			get;
			set;
		}

		/// <summary>
		/// When using this value, the default (from CoreConfiguration) FilenamePattern is given unless a value was set
		/// </summary>
		public string FilenamePattern {
			get {
				if (_filenamePattern != null) {
					return _filenamePattern;
				}
				return Conf.OutputFileFilenamePattern;
			}
			set {
				_filenamePattern = value;
			}
		}

		/// <summary>
		/// When using this value, the default (from FilenamePattern) Filename is given unless a value was set
		/// The filename is without extension!!
		/// </summary>
		public string Filename {
			get {
				if (_filename != null) {
					return _filename;
				}
				return FilenameHelper.GetFilenameWithoutExtensionFromPattern(FilenamePattern, this);
			}
			set {
				_filename = value;
			}
		}

		/// <summary>
		/// The extension for the filename, this is generated from the format. 
		/// </summary>
		public string Extension {
			get {
				return "." + Format.ToString();
			}
		}

		/// <summary>
		/// When using this value, the default (from CoreConfiguration) OutputFormat is given unless a value was set
		/// </summary>
		public OutputFormat Format {
			get {
				return _format;
			}
			set {
				_format = value;
			}
		}

		/// <summary>
		/// Title of the capture, this can e.g. be the title of the selected window.
		/// </summary>
		public string Title {
			get {
				return _title;
			}
			set {
				_title = value;
			}
		}

		/// <summary>
		/// The time that the capture was taken
		/// </summary>
		public DateTimeOffset DateTime {
			get {
				return _captureTaken;
			}
			set {
				_captureTaken = value;
			}
		}

		/// <summary>
		/// Special meta-data for this capture
		/// </summary>
		public Dictionary<string, string> MetaData {
			get {
				return _metaData;
			}
		}

		/// <summary>
		/// Add to the meta-data for this capture
		/// </summary>
		public void AddMetaData(string key, string value) {
			if (_metaData.ContainsKey(key)) {
				_metaData[key] = value;
			} else {
				_metaData.Add(key, value);
			}
		}


		/// <summary>
		/// Write this context as a Bitmap to a stream, using the settings in the context.
		/// </summary>
		/// <param name="stream">Stream to write the bitmap to</param>
		public void ToStream(Stream stream) {
			BitmapEncoder encoder;
			switch (Format) {
				case OutputFormat.bmp:
					encoder = new BmpBitmapEncoder();
					break;
				case OutputFormat.gif:
					encoder = new GifBitmapEncoder();
					break;
				case OutputFormat.jpg:
					encoder = new JpegBitmapEncoder();
					break;
				case OutputFormat.png:
					encoder = new PngBitmapEncoder();
					break;
				case OutputFormat.tiff:
					encoder = new TiffBitmapEncoder();
					break;
				default:
					throw new ArgumentException(string.Format("Unsupported image format: {0}", Format));
			}
			BitmapFrame frame = BitmapFrame.Create(Capture.Content);
			encoder.Frames.Add(frame);
			encoder.Save(stream);
		}

		#region Obsolete
		public List<ILegacyDestination> CaptureDestinations {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		public CaptureMode CaptureMode {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		public float DpiX {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		public float DpiY {
			get {
				throw new NotImplementedException();
			}
			set {
				throw new NotImplementedException();
			}
		}

		public void ClearDestinations() {
			throw new NotImplementedException();
		}

		public void RemoveDestination(ILegacyDestination captureDestination) {
			throw new NotImplementedException();
		}

		public void AddDestination(ILegacyDestination captureDestination) {
			throw new NotImplementedException();
		}

		public bool HasDestination(string designation) {
			throw new NotImplementedException();
		}

		public bool HasDestination(KnownDesignations knownDesignation) {
			throw new NotImplementedException();
		}
		#endregion
	}
}
