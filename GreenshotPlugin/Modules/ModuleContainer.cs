﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Greenshot.IniFile;
using GreenshotPlugin;
using GreenshotPlugin.Core.Settings;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace GreenshotPlugin.Modules {
	/// <summary>
	/// This is a container for all components, like plugins etc
	/// We use MEF for this
	/// </summary>
	public class ModuleContainer {
		private static readonly string PluginPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Application.ProductName + @"\Plugins");
		private static readonly string ApplicationPath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), @"Plugins");
		private static readonly string PafPath = Path.Combine(Application.StartupPath, @"App\Greenshot\Plugins");
		private static readonly ModuleContainer _instance = new ModuleContainer();

		/// <summary>
		/// Use this instance to access the modules
		/// </summary>
		public static ModuleContainer Instance {
			get {
				return _instance;
			}
		}

		private CompositionContainer _container;
		/// <summary>
		/// Use this container to get any exported modules like this:
		/// var shutdownActions = from export in ModuleContainer.Instance.Container.GetExports<IShutdownAction>() where export.Value.IsActive orderby export.Value.Priority descending select export;
		/// </summary>
		public CompositionContainer Container {
			get {
				return _container;
			}
		}

		[ImportMany]
		public IEnumerable<Lazy<IGreenshotPlugin, IDictionary<string, object>>> Plugins {
			get;
			set;
		}

		public bool HasPlugins {
			get {
				return (Plugins != null);
			}
		}

		[ImportMany]
		public IEnumerable<Lazy<SettingsPage, IDictionary<string, object>>> SettingsPages {
			get;
			set;
		}

		/// <summary>
		/// Constructor will load all managed extensions
		/// </summary>
		public ModuleContainer() {
			// Create the catalog for the plugin "locations"

			var catalog = new AggregateCatalog();

			// Plugins can be defined inside greenshot itself
			catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
			catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetEntryAssembly()));

			if (IniConfig.IsPortable && Directory.Exists(PafPath)) {
				// Portable
				foreach (string pluginFile in Directory.GetFiles(PafPath, "*.gsp", SearchOption.AllDirectories)) {
					catalog.Catalogs.Add(new SafeDirectoryCatalog(Path.GetDirectoryName(pluginFile), "*.gsp"));
				}
			} else {
				// Plugin path
				if (Directory.Exists(PluginPath)) {
					foreach (string pluginFile in Directory.GetFiles(PluginPath, "*.gsp", SearchOption.AllDirectories)) {
						catalog.Catalogs.Add(new SafeDirectoryCatalog(Path.GetDirectoryName(pluginFile), "*.gsp"));
					}
				}

				if (Directory.Exists(ApplicationPath)) {
					foreach (string pluginFile in Directory.GetFiles(ApplicationPath, "*.gsp", SearchOption.AllDirectories)) {
						catalog.Catalogs.Add(new SafeDirectoryCatalog(Path.GetDirectoryName(pluginFile), "*.gsp"));
					}
				}
			}
			//Create the CompositionContainer with the parts in the catalog
			_container = new CompositionContainer(catalog);

			//Fill the imports of this object
			_container.ComposeParts(this);
		}
	}

	/// <summary>
	/// This makes sure we don't get problems loading plugins, see this stackoverflow article
	/// http://stackoverflow.com/questions/4144683/handle-reflectiontypeloadexception-during-mef-composition
	/// </summary>
	public class SafeDirectoryCatalog : ComposablePartCatalog {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(SafeDirectoryCatalog));
		private readonly AggregateCatalog _catalog;

		public SafeDirectoryCatalog(string directory)
			: this(directory, "*.dll") {
		}
		public SafeDirectoryCatalog(string directory, string pattern) {
			var files = Directory.EnumerateFiles(directory, pattern, SearchOption.AllDirectories);

			_catalog = new AggregateCatalog();

			foreach (var file in files) {
				try {
					var asmCat = new AssemblyCatalog(file);

					//Force MEF to load the plugin and figure out if there are any exports
					// good assemblies will not throw an exception and can be added to the catalog
					if (asmCat.Parts.ToList().Count > 0) {
						_catalog.Catalogs.Add(asmCat);
					}
				} catch (Exception ex) {
					LOG.ErrorFormat(string.Format("Error loading {0}", file), ex);
				}
			}
		}

		public override IQueryable<ComposablePartDefinition> Parts {
			get {
				return _catalog.Parts;
			}
		}
	}
}
