﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GreenshotPlugin.Core.Settings;

namespace GreenshotPlugin.Modules.Triggers {
	/// <summary>
	/// A hotkey trigger calls the TriggerAction whenever the hotkey is pressed.
	/// </summary>
	public class HotkeyTrigger : ITrigger {
		private int hotkeyRegistrationCode = 0;

		/// <summary>
		/// Activate the Hotkey trigger, first the Hotkey should be set
		/// This registers the hotkey with the OS!
		/// </summary>
		public void Activate() {
			if (Hotkey == null) {
				throw new ArgumentException("Hotkey is not set");
			}
			if (IsActive) {
				throw new InvalidOperationException("Trigger is already active");
			}

			hotkeyRegistrationCode = SettingsHotkeyTextBox.RegisterHotKey(Hotkey, Trigger);
			IsActive = true;
		}

		/// <summary>
		/// Deactivate the Hotkey trigger, first the Hotkey should be set
		/// </summary>
		public void Deactivate() {
			if (!IsActive) {
				throw new InvalidOperationException("Trigger is not active");
			}
			SettingsHotkeyTextBox.UnregisterHotkey(hotkeyRegistrationCode);
			hotkeyRegistrationCode = 0;
			IsActive = false;
		}

		/// <summary>
		/// The hotkey to trigger on, this can come directly from the config
		/// </summary>
		public string Hotkey {
			get;
			set;
		}

		public void Trigger() {
			if (TriggerAction != null) {
				TriggerAction.Execute();
			}
		}

		/// <summary>
		/// Set the action to be started when the trigger triggers to this value
		/// </summary>
		public IAction TriggerAction {
			get;
			set;
		}

		public string Designation {
			get;
			set;
		}

		public string Description {
			get;
			set;
		}

		public int Priority {
			get;
			set;
		}

		public DependencyObject Visual {
			get;
			set;
		}

		public bool IsActive {
			get;
			set;
		}
	}
}
