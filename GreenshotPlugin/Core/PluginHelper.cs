﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

using Greenshot.IniFile;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using GreenshotPlugin.Core.Settings;
using log4net;

namespace GreenshotPlugin.Core {
	public class PluginInfo {
		public string Name {
			get;
			set;
		}
		public string Version {
			get;
			set;
		}
		public string Location {
			get;
			set;
		}
		public string CreatedBy {
			get;
			set;
		}
	}
	/// <summary>
	/// The PluginHelper takes care of all plugin related functionality
	/// </summary>
	[Serializable]
	public class PluginHelper : IGreenshotHost {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(PluginHelper));
		private static readonly PluginHelper instance = new PluginHelper();
		public static PluginHelper Instance {
			get {
				return instance;
			}
		}

		private PluginHelper() {
			PluginUtils.Host = this;
		}

		public IGreenshotMain GreenshotMain {
			get;
			set;
		}


		#region Implementation of IGreenshotPluginHost

		/// <summary>
		/// Create a Thumbnail
		/// </summary>
		/// <param name="image">Image of which we need a Thumbnail</param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns>Image with Thumbnail</returns>
		public Image GetThumbnail(Image image, int width, int height) {
			return image.GetThumbnailImage(width, height,  ThumbnailCallback, IntPtr.Zero);
		}

		///  <summary>
		/// Required for GetThumbnail, but not used
		/// </summary>
		/// <returns>true</returns>
		private bool ThumbnailCallback() {
			return true;
		}

		public ILegacyDestination GetDestination(string designation) {
			return DestinationHelper.GetDestination(designation);
		}
		public List<ILegacyDestination> GetAllDestinations() {
			return DestinationHelper.GetAllDestinations();
		}

		public ExportInformation ExportCapture(bool manuallyInitiated, string designation, ISurface surface, ICaptureDetails captureDetails) {
			return DestinationHelper.ExportCapture(manuallyInitiated, designation, surface, captureDetails);
		}

		/// <summary>
		/// Make Capture with specified Handler
		/// </summary>
		/// <param name="captureMouseCursor">bool false if the mouse should not be captured, true if the configuration should be checked</param>
		/// <param name="destination">IDestination</param>
		public void CaptureRegion(bool captureMouseCursor, ILegacyDestination destination) {
			//CaptureHelper.CaptureRegion(captureMouseCursor, destination);
		}

		/// <summary>
		/// Use the supplied image, and handle it as if it's captured.
		/// </summary>
		/// <param name="captureToImport">Image to handle</param>
		public void ImportCapture(ICapture captureToImport) {
			((Control)GreenshotMain).BeginInvoke((MethodInvoker)delegate {
				//CaptureHelper.ImportCapture(captureToImport);
			});
		}
		
		/// <summary>
		/// Get an ICapture object, so the plugin can modify this
		/// </summary>
		/// <returns></returns>
		public ICapture GetCapture(Image imageToCapture) {
			Capture capture = new Capture(imageToCapture);
			capture.CaptureDetails = new CaptureDetails();
			capture.CaptureDetails.CaptureMode = CaptureMode.Import;
			capture.CaptureDetails.Title = "Imported";
			return capture;
		}
		#endregion
	}
}
