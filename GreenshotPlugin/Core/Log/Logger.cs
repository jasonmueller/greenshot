﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom, Francis Noel
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Timers;
using GreenshotPlugin.Core;
using Greenshot.IniFile;
using System.Diagnostics;
using Timer = System.Timers.Timer;

namespace log4net {
	public enum LogLevel {
		DEBUG,
		INFO,
		WARN,
		ERROR,
		FATAL
	}

	public interface ILog {
		void Debug(string msg);
		void Debug(Exception ex);
		void Debug(string msg, Exception ex);
		void Error(string msg);
		void Error(Exception ex);
		void Error(string msg, Exception ex);
		void Info(string msg);
		void Warn(string msg);
		void Warn(Exception ex);
		void Warn(string msg, Exception ex);
		void Fatal(string msg);

		void DebugFormat(string msg, params object[] objs);
		void ErrorFormat(string msg, params object[] objs);
		void InfoFormat(string msg, params object[] objs);
		void WarnFormat(string msg, params object[] objs);
		void FatalFormat(string msg, params object[] objs);

		bool IsDebugEnabled {
			get;
		}
	}

	internal class FileAbstractLogger : AbstractLogger {
		private static CoreConfiguration _coreConfig;
		private static FileAbstractLogger _instance;
		public static FileAbstractLogger Instance {
			get {
				return _instance;
			}
		}

		public static void CreateInstance(string filename, int sizelimitKB) {
			if (_instance == null) {
				_instance = new FileAbstractLogger();
			}
			try {
				_instance.Init(filename, sizelimitKB);
			} catch (Exception ex) {
				// Couldn't initialize the file logger.
				Debug.WriteLine(ex.ToString());
			}
		}
		private FileAbstractLogger() {
			AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
			AppDomain.CurrentDomain.DomainUnload += CurrentDomain_ProcessExit;
		}

		Timer _saveTimer;
		private readonly Queue<String> _que = new Queue<String>(500);
		private StreamWriter _output;
		private string _filename;
		private int _sizeLimit;
		private long _lastSize;
		private DateTimeOffset _lastFileDate;
		private string _filePath = "";

		public void Init(string filename, int sizelimitKB) {
			if (_filename != null) {
				return;
			}
			_sizeLimit = sizelimitKB;
			_filename = filename;
			// handle folder names as well -> create dir etc.
			_filePath = Path.GetDirectoryName(filename);
			if (!string.IsNullOrEmpty(_filePath)) {
				_filePath = Directory.CreateDirectory(_filePath).FullName;
				if (_filePath.EndsWith("\\") == false) {
					_filePath += "\\";
				}
			}
			_output = new StreamWriter(filename, true);
			var fi = new FileInfo(filename);
			_lastSize = fi.Length;
			_lastFileDate = fi.LastWriteTime;

			_saveTimer = new Timer(500);
			_saveTimer.Elapsed += SaveTimer_Elapsed;
			_saveTimer.Enabled = true;
			_saveTimer.AutoReset = true;
		}

		void SaveTimer_Elapsed(object sender, ElapsedEventArgs e) {
			WriteData();
		}
 
		private void Shutdown() {
			_saveTimer.Enabled = false;
			WriteData();
			if (_output != null) {
				_output.Flush();
				_output.Close();
				_output = null;
			}
		}

		void CurrentDomain_ProcessExit(object sender, EventArgs e) {
			Shutdown();
		}

		private void WriteData() {
			while (_que.Count > 0) {
				string logdata = _que.Dequeue();
				if (_output != null && logdata != null) {
					if (_sizeLimit > 0) {
						// implement size limited logs
						// implement rolling logs
						#region [  rolling size limit ]
						_lastSize += logdata.Length;
						if (_lastSize > _sizeLimit * 1000) {
							_output.Flush();
							_output.Close();
							int count = 1;
							while (File.Exists(_filePath + Path.GetFileNameWithoutExtension(_filename) + "." + count.ToString("0000")))
								count++;

							File.Move(_filename, _filePath + Path.GetFileNameWithoutExtension(_filename) + "." + count.ToString("0000"));
							_output = new StreamWriter(_filename, true);
							_lastSize = 0;
						}
						#endregion
					}
					if (DateTimeOffset.Now.Subtract(_lastFileDate).Days > 0) {
						// implement date logs
						#region [  rolling dates  ]
						_output.Flush();
						_output.Close();
						int count = 1;
						while (File.Exists(_filePath + Path.GetFileNameWithoutExtension(_filename) + "." + count.ToString("0000"))) {
							File.Move(_filePath + Path.GetFileNameWithoutExtension(_filename) + "." + count.ToString("0000"), _filePath + Path.GetFileNameWithoutExtension(_filename) + "." + count.ToString("0000") + "." + _lastFileDate.ToString("yyyy-MM-dd"));
							count++;
						}
						File.Move(_filename, _filePath + Path.GetFileNameWithoutExtension(_filename) + "." + count.ToString("0000") + "." + _lastFileDate.ToString("yyyy-MM-dd"));

						_output = new StreamWriter(_filename, true);
						_lastFileDate = DateTimeOffset.Now;
						_lastSize = 0;
						#endregion
					}
					_output.Write(logdata);
				}
			}
			if (_output != null) {
				_output.Flush();
			}
		}

		public override void Log(LogLevel level, string typename, string msg, params object[] objs) {
			if (_coreConfig == null && IniConfig.IsInitialized) {
				_coreConfig = IniConfig.GetIniSection<CoreConfiguration>(false);
			}
			if (_coreConfig == null || level >= _coreConfig.LogLevel) {
				_que.Enqueue(FormatLog(level, typename, msg, objs));
			}
		}
	}

	internal abstract class AbstractLogger {
		protected string FormatLog(LogLevel level, string type, string msg, object[] objs) {
			var sb = new StringBuilder();
			sb.Append(DateTimeOffset.Now.ToString("yyyy-MM-dd hh:mm:ss.fff"));
			sb.Append("|").Append(level).Append("|").Append(Thread.CurrentThread.ManagedThreadId).Append("|").Append(type).Append("|");
			if (objs != null) {
				sb.AppendFormat(msg, objs).AppendLine();
			} else {
				sb.AppendLine(msg);
			}
			return sb.ToString();
		}

		public abstract void Log(LogLevel level, string typename, string msg, params object[] objs);
	}

	internal class ConsoleAbstractLogger : AbstractLogger {
		private static CoreConfiguration _coreConfig;

		public static ConsoleAbstractLogger Instance {
			get;
			private set;
		}

		public static void CreateInstance() {
			if (Instance == null) {
				Instance = new ConsoleAbstractLogger();
			}

		}

		private ConsoleAbstractLogger() {
			AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
			AppDomain.CurrentDomain.DomainUnload += CurrentDomain_ProcessExit;
			_saveTimer = new Timer(500);
			_saveTimer.Elapsed += SaveTimer_Elapsed;
			_saveTimer.Enabled = true;
			_saveTimer.AutoReset = true;
		}

		private readonly Timer _saveTimer;
		private readonly Queue<String> _que = new Queue<String>(500);

		void SaveTimer_Elapsed(object sender, ElapsedEventArgs e) {
			WriteData();
		}

		private void Shutdown() {
			_saveTimer.Enabled = false;
			WriteData();
		}

		void CurrentDomain_ProcessExit(object sender, EventArgs e) {
			Shutdown();
		}

		private void WriteData() {
			var sb = new StringBuilder();
			while (_que.Count > 0) {
				sb.Append(_que.Dequeue());
			}
			if (sb.Length > 0) {
				Debug.Write(sb.ToString());
			}
		}

		public override void Log(LogLevel level, string typename, string msg, params object[] objs) {
			if (_coreConfig == null && IniConfig.IsInitialized) {
				_coreConfig = IniConfig.GetIniSection<CoreConfiguration>(false);
			}
			if (_coreConfig == null || level >= _coreConfig.LogLevel) {
				_que.Enqueue(FormatLog(level, typename, msg, objs));
			}
		}
	}

	internal class Logger : ILog {
		public Logger(Type type) {
			_typename = type.Namespace + "." + type.Name;
		}

		private readonly string _typename = "";

		private void LOG(LogLevel level, string msg, params object[] objs) {
			try {
				if (FileAbstractLogger.Instance != null) {
					FileAbstractLogger.Instance.Log(level, _typename, msg, objs);
				} else if (ConsoleAbstractLogger.Instance != null) {
					ConsoleAbstractLogger.Instance.Log(level, _typename, msg, objs);
				}
			} catch  {
				// Ignore
			}
		}

		#region ILog Members

		public bool IsDebugEnabled {
			get {
				return true;
			}
		}

		public void Debug(string msg) {
			LOG(LogLevel.DEBUG, msg, null);
		}

		public void Debug(Exception ex) {
			LOG(LogLevel.DEBUG, "{0}", ex);
		}

		public void Debug(string msg, Exception ex) {
			LOG(LogLevel.DEBUG, msg + " {0}", ex);
		}

		public void DebugFormat(string msg, params object[] objs) {
			LOG(LogLevel.DEBUG, msg, objs);
		}

		public void Error(string msg) {
			LOG(LogLevel.ERROR, msg, null);
		}

		public void Error(Exception ex) {
			LOG(LogLevel.ERROR, "{0}", ex);
		}

		public void Error(string msg, Exception ex) {
			LOG(LogLevel.ERROR, msg + " {0}", ex);
		}

		public void ErrorFormat(string msg, params object[] objs) {
			LOG(LogLevel.ERROR, msg, objs);
		}

		public void Info(string msg) {
			LOG(LogLevel.INFO, msg, null);
		}

		public void InfoFormat(string msg, params object[] objs) {
			LOG(LogLevel.INFO, msg, objs);
		}

		public void Warn(string msg) {
			LOG(LogLevel.WARN, msg, null);
		}

		public void Warn(Exception ex) {
			LOG(LogLevel.WARN, "{0}", ex);
		}

		public void Warn(string msg, Exception ex) {
			LOG(LogLevel.WARN, msg + " {0}", ex);
		}

		public void WarnFormat(string msg, params object[] objs) {
			LOG(LogLevel.WARN, msg, objs);
		}

		public void Fatal(string msg) {
			LOG(LogLevel.FATAL, msg, null);
		}

		public void FatalFormat(string msg, params object[] objs) {
			LOG(LogLevel.FATAL, msg, objs);
		}
		#endregion
	}

	public static class LogManager {
		public static ILog GetLogger(Type obj) {
			// Ensure console logger is always available
			Configure();
			return new Logger(obj);
		}
		public static void Configure() {
			ConsoleAbstractLogger.CreateInstance();
		}

		public static void Configure(string filename, int sizelimitKB) {
			FileAbstractLogger.CreateInstance(filename, sizelimitKB);
		}
	}
}
