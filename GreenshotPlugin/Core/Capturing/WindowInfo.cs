﻿
using GreenshotPlugin.UnmanagedHelpers;
/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Media;
using GreenshotPlugin.Core.Imaging;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace GreenshotPlugin.Core.Capturing {
	/// <summary>
	/// This class holds all the available information on a Window.
	/// The information is updated (or reset) by the WinEventHook
	/// </summary>
	public class WindowInfo : INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;

		#region USER32 DllImports
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr GetParent(IntPtr hWnd);
		[DllImport("user32", CharSet = CharSet.Unicode, SetLastError = true)]
		private extern static int GetWindowText(IntPtr hWnd, StringBuilder lpString, int cch);
		[DllImport("user32", CharSet = CharSet.Unicode, SetLastError = true)]
		private extern static int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
		[DllImport("user32", SetLastError = true)]
		private extern static IntPtr SendMessage(IntPtr hWnd, uint wMsg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr GetClassLong(IntPtr hWnd, int nIndex);
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr GetClassLongPtr(IntPtr hWnd, int nIndex);

		/// <summary>
		/// Wrapper for the GetClassLong which decides if the system is 64-bit or not and calls the right one.
		/// </summary>
		/// <param name="hWnd">IntPtr</param>
		/// <param name="nIndex">int</param>
		/// <returns>IntPtr</returns>
		public static IntPtr GetClassLongWrapper(IntPtr hWnd, int nIndex) {
			if (IntPtr.Size > 4) {
				return GetClassLongPtr(hWnd, nIndex);
			}
			return GetClassLong(hWnd, nIndex);
		}

		#endregion

		/// <summary>
		/// Retrieve the windows title, also called Text
		/// </summary>
		/// <param name="hWnd">IntPtr for the window</param>
		/// <returns>string</returns>
		private static string GetText(IntPtr hWnd) {
			StringBuilder title = new StringBuilder(260, 260);
			int length = GetWindowText(hWnd, title, title.Capacity);
			if (length == 0) {
				Win32Error error = Win32.GetLastErrorCode();
				if (error != Win32Error.Success) {
					return null;
				}
			}
			return title.ToString();
		}
		/// <summary>
		/// Retrieve the windows classname
		/// </summary>
		/// <param name="hWnd">IntPtr for the window</param>
		/// <returns>string</returns>
		private static string GetClassname(IntPtr hWnd) {
			StringBuilder classNameBuilder = new StringBuilder(260, 260);
			int hresult = GetClassName(hWnd, classNameBuilder, classNameBuilder.Capacity);
			if (hresult == 0) {
				return null;
			}
			return classNameBuilder.ToString();
		}


		/// <summary>
		/// Get the icon for a hWnd
		/// </summary>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		private static ImageSource GetIcon(IntPtr hWnd) {
			IntPtr iconSmall = IntPtr.Zero;
			IntPtr iconBig = new IntPtr(1);
			IntPtr iconSmall2 = new IntPtr(2);

			IntPtr iconHandle = SendMessage(hWnd, (int)WindowsMessages.WM_GETICON, iconSmall2, IntPtr.Zero);
			if (iconHandle == IntPtr.Zero) {
				iconHandle = SendMessage(hWnd, (int)WindowsMessages.WM_GETICON, iconSmall, IntPtr.Zero);
			}
			if (iconHandle == IntPtr.Zero) {
				iconHandle = GetClassLongWrapper(hWnd, (int)ClassLongIndex.GCL_HICONSM);
			}
			if (iconHandle == IntPtr.Zero) {
				iconHandle = SendMessage(hWnd, (int)WindowsMessages.WM_GETICON, iconBig, IntPtr.Zero);
			}
			if (iconHandle == IntPtr.Zero) {
				iconHandle = GetClassLongWrapper(hWnd, (int)ClassLongIndex.GCL_HICON);
			}

			if (iconHandle == IntPtr.Zero) {
				return null;
			}

			using (Icon icon = Icon.FromHandle(iconHandle)) {
				return icon.ToImageSource();
			}
		}

		/// <summary>
		/// A helper method to create the WindowInfo for a hWnd with it's parent
		/// </summary>
		/// <param name="hWnd">IntPtr</param>
		/// <returns>WindowInfo</returns>
		public static WindowInfo CreateFor(IntPtr hWnd) {
			return new WindowInfo {
				Handle = hWnd,
				Classname = GetClassname(hWnd),
				Parent = GetParent(hWnd)
			};
		}

		/// <summary>
		/// Equals override
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(Object obj) {
			// Check for null values and compare run-time types.
			if (obj == null || GetType() != obj.GetType())
				return false;

			WindowInfo wi = (WindowInfo)obj;
			return wi.Handle == Handle;
		}

		/// <summary>
		/// GetHashCode override
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode() {
			return Handle.GetHashCode();
		}

		/// <summary>
		/// The hWnd
		/// </summary>
		public IntPtr Handle {
			get;
			set;
		}

		/// <summary>
		/// The parent hWnd
		/// </summary>
		public IntPtr Parent {
			get;
			set;
		}

		/// <summary>
		/// Returns true if this WindowInfo has a parent
		/// </summary>
		public bool HasParent {
			get {
				return IntPtr.Zero != Parent;
			}
		}

		private readonly List<WindowInfo> children = new List<WindowInfo>();
		/// <summary>
		/// List of children
		/// </summary>
		public List<WindowInfo> Children {
			get {
				return children;
			}
		}


		private string _text;
		/// <summary>
		/// Property for the "title" of the window, use "Property = null" to update
		/// </summary>
		public string Text {
			get {
				if (_text == null) {
					_text = GetText(Handle);
				}
				return _text;
			}
			set {
				_text = value;
				PropertyChanged.Notify(() => Text);
			}
		}

		private string _classname;
		/// <summary>
		/// Classname of this window
		/// </summary>
		public string Classname {
			get {
				if (_classname == null) {
					_classname = GetClassname(Handle);
				}
				return _classname;
			}
			set {
				_classname = value;
				PropertyChanged.Notify(() => Classname);
			}
		}

		/// <summary>
		/// A simple check if this window has a classname, this skips any retrieval
		/// </summary>
		public bool HasClassname {
			get {
				return _classname != null;
			}
		}

		private Rectangle _bounds;
		/// <summary>
		/// Bounds for this window
		/// </summary>
		public Rectangle Bounds {
			get {
				return _bounds;
			}
			set {
				if (value != Rectangle.Empty) {
					_bounds = value;
				} else {
					Rectangle windowRect = Rectangle.Empty;
					if (!HasParent && DWM.IsDwmEnabled()) {
						DWM.GetExtendedFrameBounds(Handle, out windowRect);
					}

					if (windowRect.IsEmpty) {
						User32.GetWindowRect(Handle, out windowRect);
					}

					// Correction for maximized windows, only if it's not an app
					if (!HasParent && !IsApp && Maximised) {
						Size size;
						User32.GetBorderSize(Handle, out size);
						windowRect = new Rectangle(windowRect.X + size.Width, windowRect.Y + size.Height, windowRect.Width - (2 * size.Width), windowRect.Height - (2 * size.Height));
					}
					_bounds = windowRect;
				}
				PropertyChanged.Notify(() => Bounds);
			}
		}

		/// <summary>
		/// Checks if this window is maximized
		/// </summary>
		public bool Maximised {
			get {
				if (IsApp) {
					if (Visible) {
						foreach (Screen screen in Screen.AllScreens) {
							if (_bounds.Equals(screen.Bounds)) {
								return true;
							}
						}
					}
					return false;
				}
				return User32.IsZoomed(Handle);
			}
		}

		/// <summary>
		/// Gets whether the window is visible.
		/// </summary>
		public bool Visible {
			get {
				if (IsApp) {
					return ModernUI.AppVisible(Bounds);
				}
				if (IsGutter) {
					// gutter is only made available when it's visible
					return true;
				}
				if (IsAppLauncher) {
					return ModernUI.IsLauncherVisible;
				}
				return User32.IsWindowVisible(Handle);
			}
		}

		private ImageSource _displayIcon;
		/// <summary>
		/// Returns an imagesource with the icon for this window
		/// </summary>
		public ImageSource DisplayIcon {
			get {
				if (_displayIcon == null) {
					_displayIcon = GetIcon(Handle);
				}
				return _displayIcon;
			}
			set {
				_displayIcon = value;
			}
		}

		/// <summary>
		/// Returns true if this window is an App
		/// </summary>
		public bool IsApp {
			get {
				return ModernUI.ModernuiWindowsClass.Equals(_classname);
			}
		}

		/// <summary>
		/// Returns true if this window is the app "gutter"
		/// </summary>
		public bool IsGutter {
			get {
				return ModernUI.ModernuiGutterClass.Equals(_classname);
			}
		}

		/// <summary>
		/// Returns true if this window is the app launcher (Windows 8 start screen)
		/// </summary>
		public bool IsAppLauncher {
			get {
				return ModernUI.ModernuiApplauncherClass.Equals(_classname);
			}
		}

		/// <summary>
		/// Check if this window is the window of a metro app
		/// </summary>
		public bool IsMetroApp {
			get {
				return IsAppLauncher || IsApp;
			}
		}
	}
}
