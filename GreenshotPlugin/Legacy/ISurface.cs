﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Greenshot.Core;
using Greenshot.IniFile;
using GreenshotPlugin.Core;
using GreenshotPlugin.Drawing;

namespace GreenshotPlugin {
    /// <summary>
    ///     Alignment Enums for possitioning
    /// </summary>
    //public enum HorizontalAlignment {LEFT, CENTER, RIGHT};
    [Serializable]
    public enum VerticalAlignment {
        TOP,
        CENTER,
        BOTTOM
    };

    [Serializable]
    public enum SurfaceMessageTyp {
        FileSaved,
        Error,
        Info,
        UploadedUri
    }

    public class SurfaceMessageEventArgs : EventArgs {
        public SurfaceMessageTyp MessageType { get; set; }
        public string Message { get; set; }
        public ISurface Surface { get; set; }
    }

    public class SurfaceElementEventArgs : EventArgs {
        public IList<IDrawableContainer> Elements { get; set; }
    }

    public class SurfaceDrawingModeEventArgs : EventArgs {
        public DrawingModes DrawingMode { get; set; }
    }

    public delegate void SurfaceSizeChangeEventHandler(object sender, EventArgs eventArgs);

    public delegate void SurfaceMessageEventHandler(object sender, SurfaceMessageEventArgs eventArgs);

    public delegate void SurfaceElementEventHandler(object sender, SurfaceElementEventArgs eventArgs);

    public delegate void SurfaceDrawingModeEventHandler(object sender, SurfaceDrawingModeEventArgs eventArgs);

    public enum DrawingModes {
        None,
        Rect,
        Ellipse,
        Text,
        Line,
        Arrow,
        Crop,
        Highlight,
        Obfuscate,
        Bitmap,
        Path
    }

    /// <summary>
    ///     The interface to the Surface object, so Plugins can use it.
    /// </summary>
    public interface ISurface : IDisposable {
        /// <summary>
        ///     Unique ID of the Surface
        /// </summary>
        Guid ID { get; set; }

        /// <summary>
        ///     Get/Set the image to the Surface
        ///     get will give the image as is currently visible
        ///     set will overwrite both the visible image as the underlying image
        ///     important notice:
        ///     The setter will clone the passed bitmap and dispose it when the Surface is disposed
        ///     This means that the supplied image needs to be disposed by the calling code (if needed!)
        /// </summary>
        Image Image { get; set; }

        bool HasSelectedElements { get; }
        bool Modified { get; set; }
        string LastSaveFullPath { get; set; }
        string UploadURL { get; set; }
        bool HasCursor { get; }

        ICaptureDetails CaptureDetails { get; set; }

        event SurfaceSizeChangeEventHandler SurfaceSizeChanged;
        event SurfaceMessageEventHandler SurfaceMessage;
        event SurfaceDrawingModeEventHandler DrawingModeChanged;
        event SurfaceElementEventHandler MovingElementChanged;

        /// <summary>
        ///     Get the current Image from the Editor for Exporting (save/upload etc)
        ///     Don't forget to call image.Dispose() when finished!!!
        /// </summary>
        /// <returns>Bitmap</returns>
        Image GetImageForExport();

        /// <summary>
        ///     Add a TextContainer, at the given location, to the Surface.
        ///     The TextContainer will be "re"sized to the text size.
        /// </summary>
        /// <param name="text">String to show</param>
        /// <param name="horizontalAlignment">Left, Center, Right</param>
        /// <param name="verticalAlignment">TOP, CENTER, BOTTOM</param>
        /// <param name="family">FontFamily</param>
        /// <param name="size">Font Size in float</param>
        /// <param name="italic">bool true if italic</param>
        /// <param name="bold">bool true if bold</param>
        /// <param name="shadow">bool true if shadow</param>
        /// <param name="borderSize">size of border (0 for none)</param>
        /// <param name="color">Color of string</param>
        /// <param name="fillColor">Color of background (e.g. Color.Transparent)</param>
        ITextContainer AddTextContainer(string text, HorizontalAlignment horizontalAlignment,
            VerticalAlignment verticalAlignment, FontFamily family, float size, bool italic, bool bold, bool shadow,
            int borderSize, Color color, Color fillColor);

        IImageContainer AddImageContainer(Image image, int x, int y);
        ICursorContainer AddCursorContainer(Cursor cursor, int x, int y);
        IIconContainer AddIconContainer(Icon icon, int x, int y);
        IImageContainer AddImageContainer(string filename, int x, int y);
        ICursorContainer AddCursorContainer(string filename, int x, int y);
        IIconContainer AddIconContainer(string filename, int x, int y);
        long SaveElementsToStream(Stream stream);
        void LoadElementsFromStream(Stream stream);

        void RemoveSelectedElements();
        void CutSelectedElements();
        void CopySelectedElements();
        void PasteElementFromClipboard();
        void DuplicateSelectedElements();
        void DeselectElement(IDrawableContainer container);
        void DeselectAllElements();
        void SelectElement(IDrawableContainer container);
        void Invalidate(Rectangle rectangleToInvalidate);
        void Invalidate();
        void RemoveElement(IDrawableContainer elementToRemove, bool makeUndoable);
        void SendMessageEvent(object source, SurfaceMessageTyp messageType, string message);
        void ApplyBitmapEffect(IEffect effect);
        void RemoveCursor();


    }

    public class SurfaceOutputSettings {
        private static readonly CoreConfiguration conf = IniConfig.GetIniSection<CoreConfiguration>();
        private bool disableReduceColors;
        private readonly List<IEffect> effects = new List<IEffect>();
        private bool reduceColors;

        public SurfaceOutputSettings() {
            disableReduceColors = false;
            Format = conf.OutputFileFormat;
            JPGQuality = conf.OutputFileJpegQuality;
            ReduceColors = conf.OutputFileReduceColors;
        }

        public SurfaceOutputSettings(OutputFormat format) : this() {
            Format = format;
        }

        public SurfaceOutputSettings(OutputFormat format, int quality) : this(format) {
            JPGQuality = quality;
        }

        public SurfaceOutputSettings(OutputFormat format, int quality, bool reduceColors) : this(format, quality) {
            ReduceColors = reduceColors;
        }

        public OutputFormat Format { get; set; }

        public int JPGQuality { get; set; }

        public bool SaveBackgroundOnly { get; set; }

        public List<IEffect> Effects {
            get { return effects; }
        }

        public bool ReduceColors {
            get {
                // Fix for Bug #3468436, force quantizing when output format is gif as this has only 256 colors!
                if (OutputFormat.gif.Equals(Format)) {
                    return true;
                }
                return reduceColors;
            }
            set { reduceColors = value; }
        }

        /// <summary>
        ///     Disable the reduce colors option, this overrules the enabling
        /// </summary>
        public bool DisableReduceColors {
            get { return disableReduceColors; }
            set {
                // Quantizing os needed when output format is gif as this has only 256 colors!
                if (!OutputFormat.gif.Equals(Format)) {
                    disableReduceColors = value;
                }
            }
        }

        public SurfaceOutputSettings PreventGreenshotFormat() {
            if (Format == OutputFormat.greenshot) {
                Format = OutputFormat.png;
            }
            return this;
        }
    }
}