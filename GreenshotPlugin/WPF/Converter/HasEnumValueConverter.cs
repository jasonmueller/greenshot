﻿using System.Globalization;
using GreenshotPlugin.Core;
/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Windows.Data;

namespace GreenshotPlugin.WPF {
	/// <summary>
	/// The HasEnumValueConverter returns true if a property has a certain enum value.
	/// Example in CaptureSettingsPage.xaml: Binding ElementName="windowCaptureMode" Path="SelectedValue" Converter="{StaticResource HasEnumValueConverter}" ConverterParameter="Aero"
	/// </summary>
	[ValueConversion(typeof(bool), typeof(bool))]
	public class HasEnumValueConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			if (value == null) {
				return false;
			}
			object compareValue = Enum.Parse(value.GetType(), (string)parameter);
			bool returnValue = value.Equals(compareValue);
			return returnValue;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
