﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace GreenshotPlugin.WPF {
	/// <summary>
	/// This BoolToVisibilityConverter can converts a bool in a Visibility
	/// As a Visibility can have 3 values, the true / false mapping can be specified.
	/// Default is:
	///		true -> Visible
	///		false -> Collapsed
	/// </summary>
	[ValueConversion(typeof(bool), typeof(Visibility))]
	public sealed class BoolToVisibilityConverter : IValueConverter {
		public Visibility TrueValue {
			get;
			set;
		}
		public Visibility FalseValue {
			get;
			set;
		}

		public BoolToVisibilityConverter() {
			// set defaults
			TrueValue = Visibility.Visible;
			FalseValue = Visibility.Collapsed;
		}

		public object Convert(object value, Type targetType,
			object parameter, CultureInfo culture) {
			if (!(value is bool)) {
				return null;
			}
			return (bool)value ? TrueValue : FalseValue;
		}

		public object ConvertBack(object value, Type targetType,
			object parameter, CultureInfo culture) {
			if (Equals(value, TrueValue)) {
				return true;
			}
			if (Equals(value, FalseValue)) {
				return false;
			}
			return null;
		}
	}
}
