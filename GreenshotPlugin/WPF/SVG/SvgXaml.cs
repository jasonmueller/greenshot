﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml;
using System.Xml.Xsl;

namespace GreenshotPlugin.WPF {
	/// <summary>
	/// This class is a Svg to Xaml converter, currently the way back is not implemented, but this would be easy.
	/// SVG->XAML is being made possible via an XSL, described here:
	/// http://sanpaku72.blogspot.de/2007/09/having-fun-with-xaml-silverlight-and.html
	/// Also used in InkScape, see here: https://github.com/piksels-and-lines-orchestra/inkscape/tree/master/share/extensions
	/// </summary>
	public static class SvgXaml {
		private static XslCompiledTransform xslSvg2Xaml;
		private	static XmlReaderSettings readerSettings;

		static SvgXaml() {
			xslSvg2Xaml = new XslCompiledTransform();
			readerSettings = new XmlReaderSettings();
			readerSettings.DtdProcessing = DtdProcessing.Ignore;
			var resources = Assembly.GetExecutingAssembly().GetManifestResourceNames();
			using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("GreenshotPlugin.WPF.SVG.svg2xaml.xsl")) {
				using (var reader = XmlReader.Create(stream, readerSettings)) {
					xslSvg2Xaml.Load(reader);
				}
			}
		}

		/// <summary>
		/// Convert the supplied svg to a xaml and read this with the XamlReader.
		/// </summary>
		/// <param name="svg">Filename of the svg file to convert</param>
		/// <returns>FrameworkElement</returns>
		public static FrameworkElement ToXaml(string svg) {
			using (var reader = XmlReader.Create(svg, readerSettings)) {
				return ToXaml(reader);
			}
		}
		/// <summary>
		/// Convert the supplied svg to a xaml and read this with the XamlReader.
		/// </summary>
		/// <param name="svg">Stream with the SVG to convert</param>
		/// <returns>FrameworkElement</returns>
		public static FrameworkElement ToXaml(Stream svg) {
			using (var reader = XmlReader.Create(svg, readerSettings)) {
				return ToXaml(reader);
			}
		}

		/// <summary>
		/// Private helper method wich uses the XmlReader
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		private static FrameworkElement ToXaml(XmlReader reader) {
			using (var stream = new MemoryStream()) {
				using (var writer = new StreamWriter(stream)) {
					var arguments = new XsltArgumentList();
					xslSvg2Xaml.Transform(reader, arguments, writer);
					stream.Position = 0;
					return (FrameworkElement)XamlReader.Load(stream);
				}
			}
		}

		/// <summary>
		/// Make clickable and transformable
		/// </summary>
		/// <param name="canvas"></param>
		/// <returns></returns>
		public static FrameworkElement Wrap(FrameworkElement canvas, Brush background = null, Size size = default(Size)) {
			// Put the Canvas in a border, which has a not null background to keep it clickable
			var element = new Border();
			if (background != null) {
				element.Background = background;
			} else {
				element.Background = Brushes.Transparent;
			}
			element.Child = canvas;
			// Place a viewbox around the border/canvas, so we can scale it.
			// For rotation, or other transforms, we would need to add a TransformGroup with these in it.
			//var element = new Viewbox();
			//element.RenderTransform = new ScaleTransform();
			//element.RenderTransformOrigin = new Point(0.5, 0.5);
			//element.Child = border;
			if (size != default(Size)) {
				element.Width = size.Width;
				element.Height = size.Height;
			}
			return element;

		}
	}
}

