/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Windows;
using Confluence;
using Greenshot.IniFile;
using GreenshotPlugin;
using GreenshotPlugin.Core;
using System.ComponentModel.Composition;
using log4net;

namespace GreenshotConfluencePlugin {
	/// <summary>
	/// This is the ConfluencePlugin base code
	/// </summary>
	[Export(typeof(IGreenshotPlugin))]
	[ExportMetadata("name", "Confluence plug-in")]
	public class ConfluencePlugin : AbstractPlugin {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(ConfluencePlugin));
		private static ConfluenceConnector _confluenceConnector;
		private static ConfluenceConfiguration _config;

		private static void CreateConfluenceConntector() {
			if (_confluenceConnector == null) {
				if (_config.Url.Contains("soap-axis")) {
					_confluenceConnector = new ConfluenceConnector(_config.Url, _config.Timeout);
				} else {
					_confluenceConnector = new ConfluenceConnector(_config.Url + ConfluenceConfiguration.DEFAULT_POSTFIX2, _config.Timeout);
				}
			}
		}

		public static ConfluenceConnector ConfluenceConnectorNoLogin {
			get {
				return _confluenceConnector;
			}
		}

		public static ConfluenceConnector ConfluenceConnector {
			get {
				if (_confluenceConnector == null) {
					CreateConfluenceConntector();
				}
				try {
					if (_confluenceConnector != null && !_confluenceConnector.IsLoggedIn) {
						_confluenceConnector.Login();
					}
				} catch (Exception e) {
					MessageBox.Show(Language.GetFormattedString("confluence", LangKey.login_error, e.Message));
				}
				return _confluenceConnector;
			}
		}

		public override IEnumerable<ILegacyDestination> Destinations() {
			if (ConfluenceDestination.IsInitialized) {
				yield return new ConfluenceDestination();
			} else {
				yield break;
			}
		}

		/// <summary>
		/// Implementation of the IGreenshotPlugin.Initialize
		/// </summary>
		/// <param name="pluginHost">Use the IGreenshotPluginHost interface to register events</param>
		/// <param name="metadata">IDictionary string, object</param>
		/// <returns>true if plugin is initialized, false if not (doesn't show)</returns>
		public override bool Initialize(IGreenshotHost pluginHost, IDictionary<string, object> metadata) {
			// Register configuration (don't need the configuration itself)
			_config = IniConfig.GetIniSection<ConfluenceConfiguration>();
			if(_config.IsDirty) {
				IniConfig.Save();
			}
			return true;
		}

		public override void Shutdown() {
			LOG.Debug("Confluence Plugin shutdown.");
			if (_confluenceConnector != null) {
				_confluenceConnector.Logout();
				_confluenceConnector = null;
			}
		}
	}
}
