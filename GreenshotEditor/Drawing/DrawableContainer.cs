/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using GreenshotEditor.Drawing.Fields;
using GreenshotEditor.Drawing.Filters;
using GreenshotPlugin;
using GreenshotPlugin.Drawing;
using GreenshotEditor.Memento;
using GreenshotEditor.Configuration;
using Greenshot.IniFile;
using log4net;

namespace GreenshotEditor.Drawing {
	/// <summary>
	/// represents a rectangle, ellipse, label or whatever. Can contain filters, too.
	/// serializable for clipboard support
	/// Subclasses should fulfill INotifyPropertyChanged contract, i.e. call
	/// OnPropertyChanged whenever a public property has been changed.
	/// </summary>
	[Serializable]
	public abstract class DrawableContainer : AbstractFieldHolder, IDrawableContainer {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(DrawableContainer));
		protected static readonly EditorConfiguration EditorConfig = IniConfig.GetIniSection<EditorConfiguration>();
		[NonSerialized]
		private bool _isMadeUndoable;

		readonly List<IFilter> _filters = new List<IFilter>();
		public List<IFilter> Filters {
			get {
				return _filters;
			}
		}
			
		[NonSerialized]
		internal Surface _parent;
		public ISurface Parent {
			get { return _parent; }
			set { SwitchParent((Surface)value); }
		}
		[NonSerialized]
		private Gripper[] _grippers;
		public Gripper[] Grippers {
			get {
				return _grippers;
			}
		}

		[NonSerialized]
		private Gripper _targetGripper;

		public Gripper TargetGripper {
			get {
				return _targetGripper;
			}
		}

		[NonSerialized]
		private bool _layoutSuspended;
		
		[NonSerialized]
		private bool _selected;
		public bool Selected {
			get {return _selected;}
			set {
				_selected = value;
				OnPropertyChanged("Selected");
			}
		}
		
		[NonSerialized]
		private EditStatus _status = EditStatus.UNDRAWN;
		public EditStatus Status {
			get {
				return _status;
			}
			set {
				_status = value;
			}
		}

		
		private int _left;
		public int Left {
			get { return _left; }
			set {
				if(value != _left) {
					_left = value;
					DoLayout();
				}
			}
		}
		
		private int _top;
		public int Top {
			get { return _top; }
			set {
				if (value != _top) {
					_top = value;
					DoLayout();
				}
			}
		}
		
		private int _width;
		public int Width {
			get { return _width; }
			set {
				if(value != _width) {
					_width = value;
					DoLayout();
				}
			}
		}
		
		private int _height;
		public int Height {
			get { return _height; }
			set {
				if (value != _height) {
					_height = value;
					DoLayout();
				}
			}
		}
		
		public Point Location {
			get {
				return new Point(_left, _top);
			}
		}

		public Size Size {
			get {
				return new Size(_width, _height);
			}
		}

		/// <summary>
		/// will store current bounds of this DrawableContainer before starting a resize
		/// </summary>
		[NonSerialized]
		private Rectangle _boundsBeforeResize = Rectangle.Empty;

		/// <summary>
		/// "workbench" rectangle - used for calculatoing bounds during resizing (to be applied to this DrawableContainer afterwards)
		/// </summary>
		[NonSerialized]
		private RectangleF _boundsAfterResize = RectangleF.Empty;
		
		public Rectangle Bounds {
			get { return GuiRectangle.GetGuiRectangle(Left, Top, Width, Height); }
			set {
				Left = Round(value.Left);
				Top = Round(value.Top);
				Width = Round(value.Width);
				Height = Round(value.Height);
			}
		}
		
		public virtual void ApplyBounds(RectangleF newBounds) {
			Left = Round(newBounds.Left);
			Top = Round(newBounds.Top);
			Width = Round(newBounds.Width);
			Height = Round(newBounds.Height);
		}

		/// <summary>
		/// Don't allow default constructor!
		/// </summary>
		private DrawableContainer() {
		}

		public virtual void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing) {
			if (disposing) {
				if (_grippers != null) {
					for (int i = 0; i < _grippers.Length; i++) {
						if (_grippers[i] != null) {
							_grippers[i].Dispose();
							_grippers[i] = null;
						}
					}
					_grippers = null;
				}
				if (_targetGripper != null) {
					_targetGripper.Dispose();
					_targetGripper = null;
				}
			}
		}

		~DrawableContainer() {
			Dispose(false);
		}

		/// <summary>
		/// a drawable container is always linked to a surface
		/// </summary>
		/// <param name="parent"></param>
		public DrawableContainer(Surface parent) {
			_parent = parent;
			InitFieldAttributes();
			InitControls();
		}

		public void Add(IFilter filter) {
			_filters.Add(filter);
		}
		
		public void Remove(IFilter filter) {
			_filters.Remove(filter);
		}
		
		private int Round(float f) {
			if(float.IsPositiveInfinity(f) || f>int.MaxValue/2) return int.MaxValue/2;
			if (float.IsNegativeInfinity(f) || f<int.MinValue/2) return int.MinValue/2;
			return (int)Math.Round(f);
		}

		private int InternalLineThickness {
			get {
				FieldAttribute fieldAttribute;
				if (FieldAttributes.TryGetValue(FieldTypes.LINE_THICKNESS, out fieldAttribute)) {
					return (int)fieldAttribute.GetValue(this);
				}
				return 0;
			}
		}
		private bool HasShadow {
			get {
				FieldAttribute fieldAttribute;
				if (FieldAttributes.TryGetValue(FieldTypes.SHADOW, out fieldAttribute)) {
					return (bool)fieldAttribute.GetValue(this);
				}
				return false;
			}
		}

		public virtual Rectangle DrawingBounds {
			get {
				foreach(IFilter filter in Filters) {
					if (filter.Invert) {
						return new Rectangle(Point.Empty, _parent.Image.Size);
					}
				}
				// Take a base safetymargin
				int lineThickness = 5 + InternalLineThickness;
				int offset = lineThickness/2;

				int shadow = 0;
				if (HasShadow) {
					shadow += 10;
				}
				return new Rectangle(Bounds.Left-offset, Bounds.Top-offset, Bounds.Width+lineThickness+shadow, Bounds.Height+lineThickness+shadow);
			}
		}

		public override void Invalidate() {
			if (_parent != null) {
				_parent.Invalidate(DrawingBounds);
			}
		}
		
		public void AlignToParent(HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment) {
			if (_parent == null) {
				return;
			}

			int lineThickness = InternalLineThickness;
			if (horizontalAlignment == HorizontalAlignment.Left) {
				Left = lineThickness/2;
			}
			if (horizontalAlignment == HorizontalAlignment.Right) {
				Left = _parent.Width - Width - lineThickness/2;
			}
			if (horizontalAlignment == HorizontalAlignment.Center) {
				Left = (_parent.Width / 2) - (Width / 2) - lineThickness/2;
			}

			if (verticalAlignment == VerticalAlignment.TOP) {
				Top = lineThickness/2;
			}
			if (verticalAlignment == VerticalAlignment.BOTTOM) {
				Top = _parent.Height - Height - lineThickness/2;
			}
			if (verticalAlignment == VerticalAlignment.CENTER) {
				Top = (_parent.Height / 2) - (Height / 2) - lineThickness/2;
			}
		}
		
		public virtual bool InitContent() { return true; }
		
		public virtual void OnDoubleClick() {}
		
		private void InitControls() {
			InitGrippers();
			
			DoLayout();
		}
		
		/// <summary>
		/// Initialize a target gripper
		/// </summary>
		protected void InitTargetGripper(Color gripperColor, Point location) {
			_targetGripper = new Gripper {
				Cursor = Cursors.SizeAll,
				BackColor = gripperColor,
				Visible = false,
				Parent = _parent,
				Location = location
			};
			_targetGripper.MouseDown += GripperMouseDown;
			_targetGripper.MouseUp += GripperMouseUp;
			_targetGripper.MouseMove += GripperMouseMove;
			if (_parent != null) {
				_parent.Controls.Add(_targetGripper); // otherwise we'll attach them in switchParent
			}
		}

		protected void InitGrippers() {
			_grippers = new Gripper[8];
			for(int i=0; i<_grippers.Length; i++) {
				_grippers[i] = new Gripper {
					Position = i,
					Visible = false,
					Parent = _parent
				};
				_grippers[i].MouseDown += GripperMouseDown;
				_grippers[i].MouseUp += GripperMouseUp;
				_grippers[i].MouseMove += GripperMouseMove;
			}
			_grippers[Gripper.POSITION_TOP_CENTER].Cursor = Cursors.SizeNS;
			_grippers[Gripper.POSITION_MIDDLE_RIGHT].Cursor = Cursors.SizeWE;
			_grippers[Gripper.POSITION_BOTTOM_CENTER].Cursor = Cursors.SizeNS;
			_grippers[Gripper.POSITION_MIDDLE_LEFT].Cursor = Cursors.SizeWE;
			if (_parent != null) {
				_parent.Controls.AddRange(_grippers); // otherwise we'll attach them in switchParent
			}
		}
		
		public void SuspendLayout() {
			_layoutSuspended = true;
		}
		
		public void ResumeLayout() {
			_layoutSuspended = false;
			DoLayout();
		}
		
		protected virtual void DoLayout() {
			if (_grippers == null) {
				return;
			}
			if (!_layoutSuspended) {
				int[] xChoords = {Left-2,Left+Width/2-2,Left+Width-2};
				int[] yChoords = {Top-2,Top+Height/2-2,Top+Height-2};

				_grippers[Gripper.POSITION_TOP_LEFT].Left = xChoords[0]; _grippers[Gripper.POSITION_TOP_LEFT].Top = yChoords[0];
				_grippers[Gripper.POSITION_TOP_CENTER].Left = xChoords[1]; _grippers[Gripper.POSITION_TOP_CENTER].Top = yChoords[0];
				_grippers[Gripper.POSITION_TOP_RIGHT].Left = xChoords[2]; _grippers[Gripper.POSITION_TOP_RIGHT].Top = yChoords[0];
				_grippers[Gripper.POSITION_MIDDLE_RIGHT].Left = xChoords[2]; _grippers[Gripper.POSITION_MIDDLE_RIGHT].Top = yChoords[1];
				_grippers[Gripper.POSITION_BOTTOM_RIGHT].Left = xChoords[2]; _grippers[Gripper.POSITION_BOTTOM_RIGHT].Top = yChoords[2];
				_grippers[Gripper.POSITION_BOTTOM_CENTER].Left = xChoords[1]; _grippers[Gripper.POSITION_BOTTOM_CENTER].Top = yChoords[2];
				_grippers[Gripper.POSITION_BOTTOM_LEFT].Left = xChoords[0]; _grippers[Gripper.POSITION_BOTTOM_LEFT].Top = yChoords[2];
				_grippers[Gripper.POSITION_MIDDLE_LEFT].Left = xChoords[0]; _grippers[Gripper.POSITION_MIDDLE_LEFT].Top = yChoords[1];
				
				if((_grippers[Gripper.POSITION_TOP_LEFT].Left < _grippers[Gripper.POSITION_BOTTOM_RIGHT].Left && _grippers[Gripper.POSITION_TOP_LEFT].Top < _grippers[Gripper.POSITION_BOTTOM_RIGHT].Top) ||
					_grippers[Gripper.POSITION_TOP_LEFT].Left > _grippers[Gripper.POSITION_BOTTOM_RIGHT].Left && _grippers[Gripper.POSITION_TOP_LEFT].Top > _grippers[Gripper.POSITION_BOTTOM_RIGHT].Top) {
					_grippers[Gripper.POSITION_TOP_LEFT].Cursor = Cursors.SizeNWSE;
					_grippers[Gripper.POSITION_TOP_RIGHT].Cursor = Cursors.SizeNESW;
					_grippers[Gripper.POSITION_BOTTOM_RIGHT].Cursor = Cursors.SizeNWSE;
					_grippers[Gripper.POSITION_BOTTOM_LEFT].Cursor = Cursors.SizeNESW;
				} else if((_grippers[Gripper.POSITION_TOP_LEFT].Left > _grippers[Gripper.POSITION_BOTTOM_RIGHT].Left && _grippers[Gripper.POSITION_TOP_LEFT].Top < _grippers[Gripper.POSITION_BOTTOM_RIGHT].Top) ||
					_grippers[Gripper.POSITION_TOP_LEFT].Left < _grippers[Gripper.POSITION_BOTTOM_RIGHT].Left && _grippers[Gripper.POSITION_TOP_LEFT].Top > _grippers[Gripper.POSITION_BOTTOM_RIGHT].Top) {
					_grippers[Gripper.POSITION_TOP_LEFT].Cursor = Cursors.SizeNESW;
					_grippers[Gripper.POSITION_TOP_RIGHT].Cursor = Cursors.SizeNWSE;
					_grippers[Gripper.POSITION_BOTTOM_RIGHT].Cursor = Cursors.SizeNESW;
					_grippers[Gripper.POSITION_BOTTOM_LEFT].Cursor = Cursors.SizeNWSE;
				} else if (_grippers[Gripper.POSITION_TOP_LEFT].Left == _grippers[Gripper.POSITION_BOTTOM_RIGHT].Left) {
					_grippers[Gripper.POSITION_TOP_LEFT].Cursor = Cursors.SizeNS;
					_grippers[Gripper.POSITION_BOTTOM_RIGHT].Cursor = Cursors.SizeNS;
				} else if (_grippers[Gripper.POSITION_TOP_LEFT].Top == _grippers[Gripper.POSITION_BOTTOM_RIGHT].Top) {
					_grippers[Gripper.POSITION_TOP_LEFT].Cursor = Cursors.SizeWE;
					_grippers[Gripper.POSITION_BOTTOM_RIGHT].Cursor = Cursors.SizeWE;
				}
			}
		}
		
		private void GripperMouseDown(object sender, MouseEventArgs e) {
			Gripper originatingGripper = (Gripper)sender;
			if (originatingGripper != _targetGripper) {
				Status = EditStatus.RESIZING;
				_boundsBeforeResize = new Rectangle(_left, _top, _width, _height);
				_boundsAfterResize = new RectangleF(_boundsBeforeResize.Left, _boundsBeforeResize.Top, _boundsBeforeResize.Width, _boundsBeforeResize.Height);
			} else {
				Status = EditStatus.MOVING;
			}
			_isMadeUndoable = false;
		}

		private void GripperMouseUp(object sender, MouseEventArgs e) {
			Gripper originatingGripper = (Gripper)sender;
			if (originatingGripper != _targetGripper) {
				_boundsBeforeResize = Rectangle.Empty;
				_boundsAfterResize = RectangleF.Empty;
				_isMadeUndoable = false;
			}
			Status = EditStatus.IDLE;
			Invalidate();
		}

		/// <summary>
		/// Should be overridden to handle gripper moves on the "TargetGripper"
		/// </summary>
		/// <param name="newX"></param>
		/// <param name="newY"></param>
		protected virtual void TargetGripperMove(int newX, int newY) {
			_targetGripper.Left = newX;
			_targetGripper.Top = newY;
		}

		private void GripperMouseMove(object sender, MouseEventArgs e) {
			Gripper originatingGripper = (Gripper)sender;
			int absX = originatingGripper.Left + e.X;
			int absY = originatingGripper.Top + e.Y;
			if (originatingGripper == _targetGripper && Status.Equals(EditStatus.MOVING)) {
				TargetGripperMove(absX, absY);
			} else if (Status.Equals(EditStatus.RESIZING)) {
				// check if we already made this undoable
				if (!_isMadeUndoable) {
					// don't allow another undo until we are finished with this move
					_isMadeUndoable = true;
					// Make undo-able
					MakeBoundsChangeUndoable(false);
				}
				
				Invalidate();
				SuspendLayout();
				
				// reset "workbench" rectangle to current bounds
				_boundsAfterResize.X = _boundsBeforeResize.X;
				_boundsAfterResize.Y = _boundsBeforeResize.Y;
				_boundsAfterResize.Width = _boundsBeforeResize.Width;
				_boundsAfterResize.Height = _boundsBeforeResize.Height;

				// calculate scaled rectangle
				ScaleHelper.Scale(ref _boundsAfterResize, originatingGripper.Position, new PointF(absX, absY), ScaleHelper.GetScaleOptions());

				// apply scaled bounds to this DrawableContainer
				ApplyBounds(_boundsAfterResize);
	            
				ResumeLayout();
				Invalidate();
			}
		}

		public bool hasFilters {
			get {
				return Filters.Count > 0;
			}
		}

		public abstract void Draw(Graphics graphics, RenderMode renderMode);
		
		public virtual void DrawContent(Graphics graphics, Bitmap bmp, RenderMode renderMode, Rectangle clipRectangle) {
			if (Filters.Count > 0) {
				if (Status != EditStatus.IDLE) {
					DrawSelectionBorder(graphics, Bounds);
				} else {
					if (clipRectangle.Width != 0 && clipRectangle.Height != 0) {
						foreach(IFilter filter in Filters) {
							if (filter.Invert) {
								filter.Apply(graphics, bmp, Bounds, renderMode);
							} else {
								Rectangle drawingRect = new Rectangle(Bounds.Location, Bounds.Size);
								drawingRect.Intersect(clipRectangle);
								if(filter is MagnifierFilter) {
                                    // quick&dirty bugfix, because MagnifierFilter behaves differently when drawn only partially
                                    // what we should actually do to resolve this is add a better magnifier which is not that special
                                    filter.Apply(graphics, bmp, Bounds, renderMode);
                                } else {
                                    filter.Apply(graphics, bmp, drawingRect, renderMode);
                                }
							}
						}
					}
	
				}
			}
			Draw(graphics, renderMode);
		}
		
		public virtual bool Contains(int x, int y) {
			return Bounds.Contains(x , y);
		}
		
		public virtual bool ClickableAt(int x, int y) {
			Rectangle r = GuiRectangle.GetGuiRectangle(Left, Top, Width, Height);
			r.Inflate(5, 5);
			return r.Contains(x, y);
		}
		
		protected void DrawSelectionBorder(Graphics g, Rectangle rect) {
			using (Pen pen = new Pen(Color.MediumSeaGreen)) {
				pen.DashPattern = new float[]{1,2};
				pen.Width = 1;
				g.DrawRectangle(pen, rect);
			}
		}
		
		public virtual void ShowGrippers() {
			if (_grippers != null) {
				for (int i = 0; i < _grippers.Length; i++) {
					if (_grippers[i].Enabled) {
						_grippers[i].Show();
					} else {
						_grippers[i].Hide();
					}
				}
			}
			if (_targetGripper != null) {
				if (_targetGripper.Enabled) {
					_targetGripper.Show();
				} else {
					_targetGripper.Hide();
				}
			}
			ResumeLayout();
		}
		
		public void HideGrippers() {
			SuspendLayout();
			if (_grippers != null) {
				for (int i = 0; i < _grippers.Length; i++) {
					_grippers[i].Hide();
				}
			}
			if (_targetGripper != null) {
				_targetGripper.Hide();
			}
		}
		
		public void ResizeTo(int width, int height, int anchorPosition) {
			SuspendLayout();
			Width = width;
			Height = height;
			ResumeLayout();
		}

		/// <summary>
		/// Make a following bounds change on this drawablecontainer undoable!
		/// </summary>
		/// <param name="allowMerge">true means allow the moves to be merged</param>
		public void MakeBoundsChangeUndoable(bool allowMerge) {
			if (_parent != null) {
				_parent.MakeUndoable(new DrawableContainerBoundsChangeMemento(this), allowMerge);
			}
		}
		
		public void MoveBy(int dx, int dy) {
			SuspendLayout();
			Left += dx;
			Top += dy;
			ResumeLayout();
		}
		
		/// <summary>
		/// A handler for the MouseDown, used if you don't want the surface to handle this for you
		/// </summary>
		/// <param name="x">current mouse x</param>
		/// <param name="y">current mouse y</param>
		/// <returns>true if the event is handled, false if the surface needs to handle it</returns>
		public virtual bool HandleMouseDown(int x, int y) {
			Left = _boundsBeforeResize.X = x;
			Top = _boundsBeforeResize.Y = y;
			return true;
		}

		/// <summary>
		/// A handler for the MouseMove, used if you don't want the surface to handle this for you
		/// </summary>
		/// <param name="x">current mouse x</param>
		/// <param name="y">current mouse y</param>
		/// <returns>true if the event is handled, false if the surface needs to handle it</returns>
		public virtual bool HandleMouseMove(int x, int y) {
			Invalidate();
			SuspendLayout();
			
			// reset "workrbench" rectangle to current bounds
			_boundsAfterResize.X = _boundsBeforeResize.Left;
			_boundsAfterResize.Y = _boundsBeforeResize.Top;
			_boundsAfterResize.Width = x - _boundsAfterResize.Left;
			_boundsAfterResize.Height = y - _boundsAfterResize.Top;
			
			ScaleHelper.Scale(_boundsBeforeResize, x, y, ref _boundsAfterResize, GetAngleRoundProcessor());
			
			// apply scaled bounds to this DrawableContainer
			ApplyBounds(_boundsAfterResize);
			
			ResumeLayout();
			Invalidate();
			return true;
		}
		
		/// <summary>
		/// A handler for the MouseUp
		/// </summary>
		/// <param name="x">current mouse x</param>
		/// <param name="y">current mouse y</param>
		public virtual void HandleMouseUp(int x, int y) {
		}
		
		private void SwitchParent(Surface newParent) {
			if (_parent != null && _grippers != null) {
				for (int i=0; i<_grippers.Length; i++) {
					_parent.Controls.Remove(_grippers[i]);
				}
			} else if (_grippers == null) {
				InitControls();
			}

			_parent = newParent;
			if (_grippers != null) {
				_parent.Controls.AddRange(_grippers);
			}

			foreach(IFilter filter in Filters) {
				filter.Parent = this;
			}
		}
		
		// drawablecontainers are regarded equal if they are of the same type and their bounds are equal. this should be sufficient.
		public override bool Equals(object obj) {
			bool ret = false;
			if (obj != null && GetType() == obj.GetType()) {
				DrawableContainer other = obj as DrawableContainer;
				if (other != null && (_left==other._left && _top==other._top && _width==other._width && _height==other._height)) {
					ret = true;
				}
			}
			return ret;
		}
		
		public override int GetHashCode() {
			return _left.GetHashCode() ^ _top.GetHashCode() ^ _width.GetHashCode() ^ _height.GetHashCode() ^ fieldAttributes.GetHashCode();
		}

		public virtual bool CanRotate {
			get {
				return true;
			}
		}

		public virtual void Rotate(RotateFlipType rotateFlipType) {
			// somehow the rotation is the wrong way?
			int angle = 90;
			if (RotateFlipType.Rotate90FlipNone == rotateFlipType) {
				angle = 270;
			}

			Rectangle beforeBounds = new Rectangle(Left, Top, Width, Height);
			int centerX = Width >> 1;
			int centerY = Height >> 1;
			if (_parent != null) {
				centerX = _parent.Width >> 1;
				centerY = _parent.Height >> 1;
			}
			LOG.DebugFormat("Bounds before: {0}", beforeBounds);
			GraphicsPath translatePath = new GraphicsPath();
			translatePath.AddRectangle(beforeBounds);
			Matrix rotateMatrix = new Matrix();
			rotateMatrix.RotateAt(angle, new PointF(centerX, centerY));
			translatePath.Transform(rotateMatrix);
			RectangleF newBounds = translatePath.GetBounds();
			LOG.DebugFormat("New bounds by using graphics path: {0}", newBounds);

			int ox = 0;
			int oy = 0;
			// Transform from screen to normal coordinates
			int px = Left - centerX;
			int py = centerY - Top;
			double theta = Math.PI * angle / 180.0;
			double x1 = Math.Cos(theta) * (px - ox) - Math.Sin(theta) * (py - oy) + ox;
			double y1 = Math.Sin(theta) * (px - ox) + Math.Cos(theta) * (py - oy) + oy;

			// Transform from screen to normal coordinates
			px = (Left + Width) - centerX;
			py = centerY - (Top + Height);

			double x2 = Math.Cos(theta) * (px - ox) - Math.Sin(theta) * (py - oy) + ox;
			double y2 = Math.Sin(theta) * (px - ox) + Math.Cos(theta) * (py - oy) + oy;

			// Transform back to screen coordinates, as we rotate the bitmap we need to switch the center X&Y
			x1 += centerY;
			y1 = centerX - y1;
			x2 += centerY;
			y2 = centerX - y2;

			// Calculate to rectangle
			double newWidth = x2 - x1;
			double newHeight = y2 - y1;

			RectangleF newRectangle = new RectangleF(
				(float)x1,
				(float)y1,
				(float)newWidth,
				(float)newHeight
			);
			ApplyBounds(newRectangle);
			LOG.DebugFormat("New bounds by using old method: {0}", newRectangle);
		}

		protected virtual ScaleHelper.IDoubleProcessor GetAngleRoundProcessor() {
			return ScaleHelper.ShapeAngleRoundBehavior.Instance;
		}
		
		public virtual bool HasContextMenu {
			get {
				return true;
			}
		}

		public virtual bool HasDefaultSize {
			get {
				return false;
			}
		}

		public virtual Size DefaultSize {
			get {
				throw new NotSupportedException("Object doesn't have a default size");
			}
		}
	}
}