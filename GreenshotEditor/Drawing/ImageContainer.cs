﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using GreenshotPlugin.Drawing;
using GreenshotPlugin.Core;
using Greenshot.Core;
using log4net;

namespace GreenshotEditor.Drawing {
	/// <summary>
	/// Description of BitmapContainer.
	/// </summary>
	[Serializable] 
	public class ImageContainer : DrawableContainer, IImageContainer {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(ImageContainer));

		protected bool shadow = true;
		[Field(FieldTypes.SHADOW)]
		public bool Shadow {
			get {
				return shadow;
			}
			set {
				shadow = value;
				OnFieldPropertyChanged(FieldTypes.SHADOW);
				ChangeShadowField();
			}
		}

		private Image _image;

		/// <summary>
		/// This is the shadow version of the bitmap, rendered once to save performance
		/// Do not serialize, as the shadow is recreated from the original bitmap if it's not available
		/// </summary>
		[NonSerialized]
		private Image _shadowBitmap;

		/// <summary>
		/// This is the offset for the shadow version of the bitmap
		/// Do not serialize, as the offset is recreated
		/// </summary>
		[NonSerialized]
		private Point _shadowOffset = new Point(-1, -1);

		public ImageContainer(Surface parent, string filename) : this(parent) {
			Load(filename);
		}

		public ImageContainer(Surface parent) : base(parent) {
		}

		public void ChangeShadowField() {
			if (shadow) {
				CheckShadow();
				Width = _shadowBitmap.Width;
				Height = _shadowBitmap.Height;
				Left = Left - _shadowOffset.X;
				Top = Top - _shadowOffset.Y;
			} else {
				Width = _image.Width;
				Height = _image.Height;
				if (_shadowBitmap != null) {
					Left = Left + _shadowOffset.X;
					Top = Top + _shadowOffset.Y;
				}
			}
		}

		public Image Image {
			set {
				// Remove all current bitmaps
				DisposeImages();
				_image = ImageHelper.Clone(value);
				CheckShadow();
				if (!shadow) {
					Width = _image.Width;
					Height = _image.Height;
				} else {
					Width = _shadowBitmap.Width;
					Height = _shadowBitmap.Height;
					Left = Left - _shadowOffset.X;
					Top = Top - _shadowOffset.Y;
				}
			}
			get { return _image; }
		}

		/// <summary>
		/// The bulk of the clean-up code is implemented in Dispose(bool)
		/// This Dispose is called from the Dispose and the Destructor.
		/// When disposing==true all non-managed resources should be freed too!
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing) {
			if (disposing) {
				DisposeImages();
			}
			_image = null;
			_shadowBitmap = null;
			base.Dispose(disposing);
		}

		private void DisposeImages() {
			if (_image != null) {
				_image.Dispose();
			}
			if (_shadowBitmap != null) {
				_shadowBitmap.Dispose();
			}
			_image = null;
			_shadowBitmap = null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename"></param>
		public void Load(string filename) {
			if (File.Exists(filename)) {
				// Always make sure ImageHelper.LoadBitmap results are disposed some time,
				// as we close the bitmap internally, we need to do it afterwards
				using (Image tmpImage = ImageHelper.LoadImage(filename)) {
					Image = tmpImage;
				}
				LOG.Debug("Loaded file: " + filename + " with resolution: " + Height + "," + Width);
			}
		}

		/// <summary>
		/// Rotate the bitmap
		/// </summary>
		/// <param name="rotateFlipType"></param>
		public override void Rotate(RotateFlipType rotateFlipType) {
			Image newImage = ImageHelper.RotateFlip(_image, rotateFlipType);
			if (newImage != null) {
				// Remove all current bitmaps, also the shadow (will be recreated)
				DisposeImages();
				_image = newImage;
			}
			base.Rotate(rotateFlipType);
		}

		/// <summary>
		/// This checks if a shadow is already generated
		/// </summary>
		private void CheckShadow() {
			if (shadow && _shadowBitmap == null) {
				_shadowBitmap = ImageHelper.ApplyEffect(_image, new DropShadowEffect(), out _shadowOffset);
			}
		}

		/// <summary>
		/// Draw the actual container to the graphics object
		/// </summary>
		/// <param name="graphics"></param>
		/// <param name="rm"></param>
		public override void Draw(Graphics graphics, RenderMode rm) {
			if (_image != null) {
				GraphicsState state = graphics.Save();
				graphics.SmoothingMode = SmoothingMode.HighQuality;
				graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
				graphics.CompositingQuality = CompositingQuality.HighQuality;
				graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

				if (shadow) {
					CheckShadow();
					graphics.DrawImage(_shadowBitmap, Bounds);
				} else {
					graphics.DrawImage(_image, Bounds);
				}
				graphics.Restore(state);
			}
		}

		public override bool HasDefaultSize {
			get {
				return true;
			}
		}

		public override Size DefaultSize {
			get {
				return _image.Size;
			}
		}
	}
}
