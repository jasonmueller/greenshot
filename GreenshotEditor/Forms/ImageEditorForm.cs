/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using GreenshotEditor.Configuration;
using GreenshotEditor.Drawing;
using GreenshotEditor.Drawing.Fields.Binding;
using GreenshotPlugin;
using GreenshotPlugin.Core;
using Greenshot.IniFile;
using GreenshotPlugin.Drawing;
using Greenshot.Core;
using GreenshotPlugin.Controls;
using GreenshotEditor.Controls;
using log4net;

namespace GreenshotEditor {
	/// <summary>
	/// Description of ImageEditorForm.
	/// </summary>
	public partial class ImageEditorForm : EditorForm, IImageEditor {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(ImageEditorForm));
		private static readonly EditorConfiguration EditorConfiguration = IniConfig.GetIniSection<EditorConfiguration>();
		private static readonly List<IImageEditor> EditorList = new List<IImageEditor>();

		private Surface _surface;
		private GreenshotToolStripButton[] _toolbarButtons;
		
		private static readonly string[] SupportedClipboardFormats = {typeof(string).FullName, "Text", typeof(DrawableContainerList).FullName};

		private bool _originalBoldCheckState;
		private bool _originalItalicCheckState;
		
		// whether part of the editor controls are disabled depending on selected item(s)
		private bool _controlsDisabledDueToConfirmable;

		/// <summary>
		/// An Implementation for the IImageEditor, this way Plugins have access to the HWND handles wich can be used with Win32 API calls.
		/// </summary>
		public IWin32Window WindowHandle {
			get { return this; }
		}

		public static List<IImageEditor> Editors {
			get {
				return EditorList;
			}
		}

		public ImageEditorForm(ISurface iSurface, bool outputMade) {
			EditorList.Add(this);

			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			ManualLanguageApply = true;
			InitializeComponent();
			
			Load += delegate {
				var thread = new Thread(AddDestinations) {
					Name = "add destinations"
				};
				thread.Start();
			};

			// Make sure the editor is placed on the same location as the last editor was on close
			WindowDetails thisForm = new WindowDetails(Handle);
			thisForm.WindowPlacement = EditorConfiguration.GetEditorPlacement();

			// init surface
			Surface = iSurface;
			// Intial "saved" flag for asking if the image needs to be save
			_surface.Modified = !outputMade;

			UpdateUI();

			// Workaround: As the cursor is (mostly) selected on the surface a funny artifact is visible, this fixes it.
			HideToolstripItems();
		}

		/// <summary>
		/// Remove the current surface
		/// </summary>
		private void RemoveSurface() {
			if (_surface != null) {
				panel1.Controls.Remove(_surface);
				_surface.Dispose();
				_surface = null;
			}
		}

		/// <summary>
		/// Change the surface
		/// </summary>
		/// <param name="newSurface"></param>
		private void SetSurface(ISurface newSurface) {
			if (Surface != null && Surface.Modified) {
				throw new ApplicationException("Surface modified");
			}

			RemoveSurface();

			panel1.Height = 10;
			panel1.Width = 10;
			_surface = newSurface as Surface;
			if (_surface != null) {
				panel1.Controls.Add(_surface);
				Image backgroundForTransparency = GreenshotResources.GetImage("Checkerboard.Image");
				_surface.TransparencyBackgroundBrush = new TextureBrush(backgroundForTransparency, WrapMode.Tile);

				_surface.MovingElementChanged += delegate {
					RefreshEditorControls();
				};
				_surface.DrawingModeChanged += surface_DrawingModeChanged;
				_surface.SurfaceSizeChanged += SurfaceSizeChanged;
				_surface.SurfaceMessage += SurfaceMessageReceived;
				SurfaceSizeChanged(Surface, null);

				BindFieldControls();
				RefreshEditorControls();
				// Fix title
				if (_surface != null && _surface.CaptureDetails != null && _surface.CaptureDetails.Title != null) {
					Text = _surface.CaptureDetails.Title + " - " + Language.GetString(LangKey.editor_title);
				}
			}
			WindowDetails.ToForeground(Handle);
		}

		private void UpdateUI() {
			Icon = GreenshotResources.GetGreenshotIcon();

			// Disable access to the settings, for feature #3521446
			preferencesToolStripMenuItem.Visible = !CoreConfiguration.DisableSettings;
			toolStripSeparator12.Visible = !CoreConfiguration.DisableSettings;
			toolStripSeparator11.Visible = !CoreConfiguration.DisableSettings;
			btnSettings.Visible = !CoreConfiguration.DisableSettings;

			// Make sure Double-buffer is enabled
			SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);

			// resizing the panel is futile, since it is docked. however, it seems
			// to fix the bug (?) with the vscrollbar not being able to shrink to
			// a smaller size than the initial panel size (as set by the forms designer)
			panel1.Height = 10;

			fontFamilyComboBox.PropertyChanged += FontPropertyChanged;
			
			obfuscateModeButton.DropDownItemClicked += FilterPresetDropDownItemClicked;
			highlightModeButton.DropDownItemClicked += FilterPresetDropDownItemClicked;
			
			_toolbarButtons = new[] { btnCursor, btnRect, btnEllipse, btnText, btnLine, btnArrow, btnFreehand, btnHighlight, btnObfuscate, btnCrop };
			//toolbarDropDownButtons = new ToolStripDropDownButton[]{btnBlur, btnPixeliate, btnTextHighlighter, btnAreaHighlighter, btnMagnifier};

			pluginToolStripMenuItem.Visible = pluginToolStripMenuItem.DropDownItems.Count > 0;
			
			// Workaround: for the MouseWheel event which doesn't get to the panel
			MouseWheel += PanelMouseWheel;

			ApplyLanguage();
		}
		
		/// <summary>
		/// Get all the destinations and display them in the file menu and the buttons
		/// </summary>
		void AddDestinations() {
			Invoke((MethodInvoker)delegate {
				// Create export buttons 
				foreach(ILegacyDestination destination in DestinationHelper.GetAllDestinations()) {
					if (destination.Priority <= 2) {
						continue;
					}
					if (!destination.IsActive) {
						continue;
					}
					if (destination.DisplayIcon == null) {
						continue;
					}
					try {
						AddDestinationButton(destination);
					} catch (Exception addingException) {
						LOG.WarnFormat("Problem adding destination {0}", destination.Designation);
						LOG.Warn("Exception: ", addingException);
					}
				}
			});
		}

		void AddDestinationButton(ILegacyDestination toolstripDestination) {
			if (toolstripDestination.IsDynamic) {
				ToolStripSplitButton destinationButton = new ToolStripSplitButton {
					DisplayStyle = ToolStripItemDisplayStyle.Image,
					Size = new Size(23, 22),
					Text = toolstripDestination.Description,
					Image = toolstripDestination.DisplayIcon
				};
				//ToolStripDropDownButton destinationButton = new ToolStripDropDownButton();

				ToolStripMenuItem defaultItem = new ToolStripMenuItem(toolstripDestination.Description) {
					Tag = toolstripDestination,
					Image = toolstripDestination.DisplayIcon
				};
				defaultItem.Click += delegate {
					toolstripDestination.ExportCapture(true, _surface, _surface.CaptureDetails);
				};
				
				// The ButtonClick, this is for the icon, gets the current default item
				destinationButton.ButtonClick += delegate {
					toolstripDestination.ExportCapture(true, _surface, _surface.CaptureDetails);
				};
				
				// Generate the entries for the drop down
				destinationButton.DropDownOpening += delegate {
					ClearItems(destinationButton.DropDownItems);
					destinationButton.DropDownItems.Add(defaultItem);

					List<ILegacyDestination> subDestinations = new List<ILegacyDestination>();
					subDestinations.AddRange(toolstripDestination.DynamicDestinations());
					if (subDestinations.Count > 0) {
						subDestinations.Sort();
						foreach(ILegacyDestination subDestination in subDestinations) {
							ILegacyDestination closureFixedDestination = subDestination;
							ToolStripMenuItem destinationMenuItem = new ToolStripMenuItem(closureFixedDestination.Description) {
								Tag = closureFixedDestination,
								Image = closureFixedDestination.DisplayIcon
							};
							destinationMenuItem.Click += delegate {
								closureFixedDestination.ExportCapture(true, _surface, _surface.CaptureDetails);
							};
							destinationButton.DropDownItems.Add(destinationMenuItem);
						}
					}
				};

				toolStrip1.Items.Insert(toolStrip1.Items.IndexOf(toolStripSeparator16), destinationButton);
				
			} else {
				ToolStripButton destinationButton = new ToolStripButton {
					DisplayStyle = ToolStripItemDisplayStyle.Image,
					Size = new Size(23, 22),
					Text = toolstripDestination.Description,
					Image = toolstripDestination.DisplayIcon
				};
				destinationButton.Click += delegate {
					toolstripDestination.ExportCapture(true, _surface, _surface.CaptureDetails);
				};
				toolStrip1.Items.Insert(toolStrip1.Items.IndexOf(toolStripSeparator16), destinationButton);
			}
		}
		
		/// <summary>
		/// According to some information I found, the clear doesn't work correctly when the shortcutkeys are set?
		/// This helper method takes care of this.
		/// </summary>
		/// <param name="items"></param>
		private static void ClearItems(ToolStripItemCollection items) {
			foreach(var item in items) {
				ToolStripMenuItem menuItem = item as ToolStripMenuItem;
				if (menuItem != null && menuItem.ShortcutKeys != Keys.None) {
					menuItem.ShortcutKeys = Keys.None;
				}
			}
			items.Clear();
		}

		void FileMenuDropDownOpening(object sender, EventArgs eventArgs) {
			ClearItems(fileStripMenuItem.DropDownItems);

			// Add the destinations
			foreach(ILegacyDestination destination in DestinationHelper.GetAllDestinations()) {
				if (destination.Designation == KnownDesignations.Picker.ToString() || destination.Designation == KnownDesignations.Editor.ToString()) {
					continue;
				}
				if (!destination.IsActive) {
					continue;
				}
				
				ToolStripMenuItem item = destination.GetMenuItem(true, null, DestinationToolStripMenuItemClick);
				if (item != null) {
					item.ShortcutKeys = destination.EditorShortcutKeys;
					fileStripMenuItem.DropDownItems.Add(item);
				}
			}
			// add the elements after the destinations
			fileStripMenuItem.DropDownItems.Add(toolStripSeparator9);
			fileStripMenuItem.DropDownItems.Add(closeToolStripMenuItem);
		}

		/// <summary>
		/// This is the SufraceMessageEvent receiver which display a message in the status bar if the
		/// surface is exported. It also updates the title to represent the filename, if there is one.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="eventArgs"></param>
		private void SurfaceMessageReceived(object sender, SurfaceMessageEventArgs eventArgs) {
			string dateTimeOffset = DateTimeOffset.Now.ToString("T");
			// TODO: Fix that we only open files, like in the tooltip
			switch (eventArgs.MessageType) {
				case SurfaceMessageTyp.FileSaved:
					// Put the event message on the status label and attach the context menu
					UpdateStatusLabel(dateTimeOffset + " - " + eventArgs.Message, fileSavedStatusContextMenu);
					// Change title
					Text = eventArgs.Surface.LastSaveFullPath + " - " + Language.GetString(LangKey.editor_title);
					break;
				default:
					// Put the event message on the status label
					UpdateStatusLabel(dateTimeOffset + " - " + eventArgs.Message);
					break;
			}
		}

		/// <summary>
		/// This is called when the size of the surface chances, used for resizing and displaying the size information
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SurfaceSizeChanged(object sender, EventArgs e) {
			if (EditorConfiguration.MatchSizeToCapture) {
				// Set editor's initial size to the size of the surface plus the size of the chrome
				Size imageSize = Surface.Image.Size;
				Size currentFormSize = Size;
				Size currentImageClientSize = panel1.ClientSize;
				const int minimumFormWidth = 650;
				const int minimumFormHeight = 530;
				int newWidth = Math.Max(minimumFormWidth, (currentFormSize.Width - currentImageClientSize.Width) + imageSize.Width);
				int newHeight = Math.Max(minimumFormHeight, (currentFormSize.Height - currentImageClientSize.Height) + imageSize.Height);
				Size = new Size(newWidth, newHeight);
			}
			dimensionsLabel.Text = Surface.Image.Width + "x" + Surface.Image.Height;
			ImageEditorFormResize(sender, new EventArgs());
		}

		public ISurface Surface {
			get {
				return _surface;
			}
			set {
				SetSurface(value);
			}
		}

		public void SetImagePath(string fullpath) {
			// Check if the editor supports the format
			if (fullpath != null && (fullpath.EndsWith(".ico") || fullpath.EndsWith(".wmf"))) {
				fullpath = null;
			}
			_surface.LastSaveFullPath = fullpath;

			if (fullpath == null) {
				return;
			}
			UpdateStatusLabel(Language.GetFormattedString(LangKey.editor_imagesaved, fullpath), fileSavedStatusContextMenu);
			Text = Path.GetFileName(fullpath) + " - " + Language.GetString(LangKey.editor_title);
		}
		
		void surface_DrawingModeChanged(object source, SurfaceDrawingModeEventArgs eventArgs) {
			switch (eventArgs.DrawingMode) {
				case DrawingModes.None:
					SetButtonChecked(btnCursor);
					break;
				case DrawingModes.Ellipse:
					SetButtonChecked(btnEllipse);
					break;
				case DrawingModes.Rect:
					SetButtonChecked(btnRect);
					break;
				case DrawingModes.Text:
					SetButtonChecked(btnText);
					break;
				case DrawingModes.Line:
					SetButtonChecked(btnLine);
					break;
				case DrawingModes.Arrow:
					SetButtonChecked(btnArrow);
					break;
				case DrawingModes.Crop:
					SetButtonChecked(btnCrop);
					break;
				case DrawingModes.Highlight:
					SetButtonChecked(btnHighlight);
					break;
				case DrawingModes.Obfuscate:
					SetButtonChecked(btnObfuscate);
					break;
				case DrawingModes.Path:
					SetButtonChecked(btnFreehand);
					break;
			}
		}

		#region plugin interfaces
		
		/**
		 * Interfaces for plugins, see GreenshotInterface for more details!
		 */
		
		public Image GetImageForExport() {
			return _surface.GetImageForExport();
		}
		
		public ICaptureDetails CaptureDetails {
			get { return _surface.CaptureDetails; }
		}
		
		public ToolStripMenuItem GetPluginMenuItem() {
			return pluginToolStripMenuItem;
		}

		public ToolStripMenuItem GetFileMenuItem() {
			return fileStripMenuItem;
		}
		#endregion
		
		#region filesystem options
		void BtnSaveClick(object sender, EventArgs e) {
			if (_surface.LastSaveFullPath == null) {
				DestinationHelper.ExportCapture(true, KnownDesignations.FileDialog, _surface, _surface.CaptureDetails);
			} else {
				DestinationHelper.ExportCapture(true, KnownDesignations.FileNoDialog, _surface, _surface.CaptureDetails);
			}
		}
		
		void BtnClipboardClick(object sender, EventArgs e) {
			DestinationHelper.ExportCapture(true, KnownDesignations.Clipboard, _surface, _surface.CaptureDetails);
		}

		void BtnPrintClick(object sender, EventArgs e) {
			// The BeginInvoke is a solution for the printdialog not having focus
			BeginInvoke((MethodInvoker) delegate {
				DestinationHelper.ExportCapture(true, KnownDesignations.Printer, _surface, _surface.CaptureDetails);
			});
		}

		void CloseToolStripMenuItemClick(object sender, EventArgs e) {
			Close();
		}
		#endregion
		
		#region drawing options
		void BtnEllipseClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.Ellipse;
			RefreshFieldControls();
		}
		
		void BtnCursorClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.None;
			RefreshFieldControls();
		}
		
		void BtnRectClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.Rect;
			RefreshFieldControls();
		}
		
		void BtnTextClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.Text;
			RefreshFieldControls();
		}
		
		void BtnLineClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.Line;
			RefreshFieldControls();
		}
		
		void BtnArrowClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.Arrow;
			RefreshFieldControls();
		}
		
		void BtnCropClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.Crop;
			RefreshFieldControls();
		}
		
		void BtnHighlightClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.Highlight;
			RefreshFieldControls();
		}
		
		void BtnObfuscateClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.Obfuscate;
			RefreshFieldControls();
		}

		void BtnFreehandClick(object sender, EventArgs e) {
			_surface.DrawingMode = DrawingModes.Path;
			RefreshFieldControls();
		}
		
		void SetButtonChecked(ToolStripButton btn) {
			UncheckAllToolButtons();
			btn.Checked = true;
		}
		
		private void UncheckAllToolButtons() {
			if (_toolbarButtons != null) {
				foreach (ToolStripButton butt in _toolbarButtons) {
					butt.Checked = false;
				}
			}
		}
		
		void AddRectangleToolStripMenuItemClick(object sender, EventArgs e) {
			BtnRectClick(sender, e);
		}

		void DrawFreehandToolStripMenuItemClick(object sender, EventArgs e) {
			BtnFreehandClick(sender, e);
		}
		
		void AddEllipseToolStripMenuItemClick(object sender, EventArgs e) {
			BtnEllipseClick(sender, e);
		}
		
		void AddTextBoxToolStripMenuItemClick(object sender, EventArgs e) {
			BtnTextClick(sender, e);
		}
		
		void DrawLineToolStripMenuItemClick(object sender, EventArgs e) {
			BtnLineClick(sender, e);
		}
		
		void DrawArrowToolStripMenuItemClick(object sender, EventArgs e) {
			BtnArrowClick(sender, e);
		}
		
		void DrawHighlightToolStripMenuItemClick(object sender, EventArgs e) {
			BtnHighlightClick(sender, e);
		}
		
		void BlurToolStripMenuItemClick(object sender, EventArgs e) {
			BtnObfuscateClick(sender, e);
		}
		
		void RemoveObjectToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.RemoveSelectedElements();
		}

		void BtnDeleteClick(object sender, EventArgs e) {
			RemoveObjectToolStripMenuItemClick(sender, e);
		}
		#endregion
		
		#region copy&paste options
		void CutToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.CutSelectedElements();
			UpdateClipboardSurfaceDependencies();
		}

		void BtnCutClick(object sender, EventArgs e) {
			CutToolStripMenuItemClick(sender, e);
		}
		
		void CopyToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.CopySelectedElements();
			UpdateClipboardSurfaceDependencies();
		}

		void BtnCopyClick(object sender, EventArgs e) {
			CopyToolStripMenuItemClick(sender, e);
		}
		
		void PasteToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.PasteElementFromClipboard();
			UpdateClipboardSurfaceDependencies();
		}

		void BtnPasteClick(object sender, EventArgs e) {
			PasteToolStripMenuItemClick(sender, e);
		}

		void UndoToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.Undo();
			UpdateUndoRedoSurfaceDependencies();
		}

		void BtnUndoClick(object sender, EventArgs e) {
			UndoToolStripMenuItemClick(sender, e);
		}

		void RedoToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.Redo();
			UpdateUndoRedoSurfaceDependencies();
		}

		void BtnRedoClick(object sender, EventArgs e) {
			RedoToolStripMenuItemClick(sender, e);
		}

		void DuplicateToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.DuplicateSelectedElements();
			UpdateClipboardSurfaceDependencies();
		}
		#endregion
		
		#region element properties
		void UpOneLevelToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.PullElementsUp();
		}
		
		void DownOneLevelToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.PushElementsDown();
		}
		
		void UpToTopToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.PullElementsToTop();
		}
		
		void DownToBottomToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.PushElementsToBottom();
		}
		
		
		#endregion
		
		#region help
		void HelpToolStripMenuItem1Click(object sender, EventArgs e) {
			HelpFileLoader.LoadHelp();
		}

		void AboutToolStripMenuItemClick(object sender, EventArgs e) {
			PluginHelper.Instance.GreenshotMain.ShowAbout();
		}

		void PreferencesToolStripMenuItemClick(object sender, EventArgs e) {
			PluginHelper.Instance.GreenshotMain.ShowSetting();
		}

		void BtnSettingsClick(object sender, EventArgs e) {
			PreferencesToolStripMenuItemClick(sender, e);
		}

		void BtnHelpClick(object sender, EventArgs e) {
			HelpToolStripMenuItem1Click(sender, e);
		}
		#endregion
		
		#region image editor event handlers
		void ImageEditorFormActivated(object sender, EventArgs e) {
			UpdateClipboardSurfaceDependencies();
			UpdateUndoRedoSurfaceDependencies();
		}

		void ImageEditorFormFormClosing(object sender, FormClosingEventArgs e) {
			if (_surface.Modified && !EditorConfiguration.SuppressSaveDialogAtClose) {
				// Make sure the editor is visible
				WindowDetails.ToForeground(Handle);

				string text = Language.GetString(LangKey.editor_close_on_save);
				string title = Language.GetString(LangKey.editor_close_on_save_title);
				DialogResult result;
				// Dissallow "CANCEL" if the application needs to shutdown
				if (e.CloseReason == CloseReason.ApplicationExitCall || e.CloseReason == CloseReason.WindowsShutDown || e.CloseReason == CloseReason.TaskManagerClosing) {
					result = GreenshotDialog.AskYesNo(null, title, text);
				} else {
					result = GreenshotDialog.AskYesNoCancel(null, title, text);
				}

				if (result.Equals(DialogResult.Cancel)) {
					e.Cancel = true;
					return;
				}
				if (result.Equals(DialogResult.Yes)) {
					BtnSaveClick(sender,e);
					// Check if the save was made, if not it was cancelled so we cancel the closing
					if (_surface.Modified) {
						e.Cancel = true;
						return;
					}
				}
			}
			// persist our geometry string.
			EditorConfiguration.SetEditorPlacement(new WindowDetails(Handle).WindowPlacement);
			IniConfig.Save();
			
			// remove from the editor list
			EditorList.Remove(this);

			_surface.Dispose();

			GC.Collect();
		}

		void ImageEditorFormKeyDown(object sender, KeyEventArgs e) {
			// LOG.Debug("Got key event "+e.KeyCode + ", " + e.Modifiers);
			// avoid conflict with other shortcuts and
			// make sure there's no selected element claiming input focus
			if(e.Modifiers.Equals(Keys.None) && !_surface.KeysLocked) {
				switch(e.KeyCode) {
					case Keys.Escape:
						BtnCursorClick(sender, e);
						break;
					case Keys.R:
						BtnRectClick(sender, e);
						break;
					case Keys.E:
						BtnEllipseClick(sender, e);
						break;
					case Keys.L:
						BtnLineClick(sender, e);
						break;
					case Keys.F:
						BtnFreehandClick(sender, e);
						break;
					case Keys.A:
						BtnArrowClick(sender, e);
						break;
					case Keys.T:
						BtnTextClick(sender, e);
						break;
					case Keys.H:
						BtnHighlightClick(sender, e);
						break;
					case Keys.O:
						BtnObfuscateClick(sender, e);
						break;
					case Keys.C:
						BtnCropClick(sender, e);
						break;
				}
			} else if (e.Modifiers.Equals(Keys.Control)) {
				switch (e.KeyCode) {
					case Keys.Z:
						UndoToolStripMenuItemClick(sender, e);
						break;
					case Keys.Y:
						RedoToolStripMenuItemClick(sender, e);
						break;
					case Keys.Q:	// Dropshadow Ctrl + Q
						AddDropshadowToolStripMenuItemClick(sender, e);
						break;
					case Keys.B:	// Border Ctrl + B
						AddBorderToolStripMenuItemClick(sender, e);
						break;
					case Keys.T:	// Torn edge Ctrl + T
						TornEdgesToolStripMenuItemClick(sender, e);
						break;
					case Keys.I:	// Invert Ctrl + I
						InvertToolStripMenuItemClick(sender, e);
						break;
					case Keys.G:	// Grayscale Ctrl + G
						GrayscaleToolStripMenuItemClick(sender, e);
						break;
					case Keys.Delete:	// Grayscale Ctrl + Delete
						ClearToolStripMenuItemClick(sender, e);
						break;
					case Keys.Oemcomma:	// Rotate CCW Ctrl + ,
						RotateCcwToolstripButtonClick(sender, e);
						break;
					case Keys.OemPeriod:	// Rotate CW Ctrl + .
						RotateCwToolstripButtonClick(sender, e);
						break;
				}
			}
		}

		/// <summary>
		/// This is a "work-around" for the MouseWheel event which doesn't get to the panel
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PanelMouseWheel(object sender, MouseEventArgs e) {
			panel1.Focus();
		}
		#endregion
		
		#region key handling
		protected override bool ProcessKeyPreview(ref Message msg) {
			// disable default key handling if surface has requested a lock
			if (!_surface.KeysLocked) {
				return base.ProcessKeyPreview(ref msg);
			}
			return false;
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keys) {
			// disable default key handling if surface has requested a lock
			if (!_surface.KeysLocked) {

				// Go through the destinations to check the EditorShortcut Keys
				// this way the menu entries don't need to be enabled.
				// This also fixes bugs #3526974 & #3527020
				foreach (ILegacyDestination destination in DestinationHelper.GetAllDestinations()) {
					if (destination.Designation == KnownDesignations.Picker.ToString() || destination.Designation == KnownDesignations.Editor.ToString()) {
						continue;
					}
					if (!destination.IsActive) {
						continue;
					}

					if (destination.EditorShortcutKeys == keys) {
						destination.ExportCapture(true, _surface, _surface.CaptureDetails);
						return true;
					}
				}
				if (!_surface.ProcessCmdKey(keys)) {
					return base.ProcessCmdKey(ref msg, keys);
				}
			}
			return false;
		}
		#endregion
		
		#region helpers
		
		private void UpdateUndoRedoSurfaceDependencies() {
			if (_surface == null) {
				return;
			}
			bool canUndo = _surface.CanUndo;
			btnUndo.Enabled = canUndo;
			undoToolStripMenuItem.Enabled = canUndo;
			string undoAction = "";
			if (canUndo) {
				if (_surface.UndoActionLanguageKey != LangKey.none) {
					undoAction = Language.GetString(_surface.UndoActionLanguageKey);
				}
			}
			string undoText = Language.GetFormattedString(LangKey.editor_undo, undoAction);
			btnUndo.Text = undoText;
			undoToolStripMenuItem.Text = undoText;

			bool canRedo = _surface.CanRedo;
			btnRedo.Enabled = canRedo;
			redoToolStripMenuItem.Enabled = canRedo;
			string redoAction = "";
			if (canRedo) {
                if (_surface.RedoActionLanguageKey != LangKey.none) {
                    redoAction = Language.GetString(_surface.RedoActionLanguageKey);
                }
			}
			string redoText = Language.GetFormattedString(LangKey.editor_redo, redoAction);
			btnRedo.Text = redoText;
			redoToolStripMenuItem.Text = redoText;
		}

		private void UpdateClipboardSurfaceDependencies() {
			if (_surface == null) {
				return;
			}
			// check dependencies for the Surface
			bool hasItems = _surface.HasSelectedElements;
			bool actionAllowedForSelection = hasItems && !_controlsDisabledDueToConfirmable;
			
			// buttons
			btnCut.Enabled = actionAllowedForSelection;
			btnCopy.Enabled = actionAllowedForSelection;
			btnDelete.Enabled = actionAllowedForSelection;

			// menus
			removeObjectToolStripMenuItem.Enabled = actionAllowedForSelection;
			copyToolStripMenuItem.Enabled = actionAllowedForSelection;
			cutToolStripMenuItem.Enabled = actionAllowedForSelection;
			duplicateToolStripMenuItem.Enabled = actionAllowedForSelection;

			// check dependencies for the Clipboard
			bool hasClipboard = ClipboardHelper.ContainsFormat(SupportedClipboardFormats) || ClipboardHelper.ContainsImage();
			btnPaste.Enabled = hasClipboard && !_controlsDisabledDueToConfirmable;
			pasteToolStripMenuItem.Enabled = hasClipboard && !_controlsDisabledDueToConfirmable;
		}

		#endregion
		
		#region status label handling
		private void UpdateStatusLabel(string text, ContextMenuStrip contextMenu) {
			statusLabel.Text = text;
			statusStrip1.ContextMenuStrip = contextMenu;
		}
		
		private void UpdateStatusLabel(string text) {
			UpdateStatusLabel(text, null);
		}
		private void ClearStatusLabel() {
			UpdateStatusLabel(null, null);
		}
		
		void StatusLabelClicked(object sender, MouseEventArgs e) {
			ToolStrip ss = ((ToolStripStatusLabel)sender).Owner;
			if(ss.ContextMenuStrip != null) {
				ss.ContextMenuStrip.Show(ss, e.X, e.Y);
			}
		}
		
		void CopyPathMenuItemClick(object sender, EventArgs e) {
			ClipboardHelper.SetClipboardData(_surface.LastSaveFullPath);
		}
		
		void OpenDirectoryMenuItemClick(object sender, EventArgs e) {
			if (_surface != null && _surface.LastSaveFullPath != null) {
				ProcessStartInfo psi = new ProcessStartInfo("explorer");
				psi.Arguments = Path.GetDirectoryName(_surface.LastSaveFullPath);
				psi.UseShellExecute = false;
				Process p = new Process();
				p.StartInfo = psi;
				p.Start();
			}
		}
		#endregion

		readonly List<BidirectionalBinding> _bindings = new List<BidirectionalBinding>();
		private void BindFieldControls() {
			_bindings.Add(new BidirectionalBinding(btnFillColor, "SelectedColor", _surface, FieldTypes.FILL_COLOR, NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(btnLineColor, "SelectedColor", _surface, FieldTypes.LINE_COLOR, NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(lineThicknessUpDown, "Value", _surface, FieldTypes.LINE_THICKNESS, DecimalIntConverter.GetInstance(), NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(blurRadiusUpDown, "Value", _surface, FieldTypes.BLUR_RADIUS, DecimalIntConverter.GetInstance(), NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(magnificationFactorUpDown, "Value", _surface, FieldTypes.MAGNIFICATION_FACTOR, DecimalIntConverter.GetInstance(), NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(pixelSizeUpDown, "Value", _surface, FieldTypes.PIXEL_SIZE, DecimalIntConverter.GetInstance(), NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(brightnessUpDown, "Value", _surface, FieldTypes.BRIGHTNESS, DecimalDoublePercentageConverter.GetInstance(), NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(fontFamilyComboBox, "Text", _surface, FieldTypes.FONT_FAMILY, NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(fontSizeUpDown, "Value", _surface, FieldTypes.FONT_SIZE, DecimalFloatConverter.GetInstance(), NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(fontBoldButton, "Checked", _surface, FieldTypes.FONT_BOLD, NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(fontItalicButton, "Checked", _surface, FieldTypes.FONT_ITALIC, NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(textHorizontalAlignmentButton, "SelectedTag", _surface, FieldTypes.TEXT_HORIZONTAL_ALIGNMENT, HorizontalAlignmentConverter.GetInstance(), NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(textVerticalAlignmentButton, "SelectedTag", _surface, FieldTypes.TEXT_VERTICAL_ALIGNMENT, VerticalAlignmentConverter.GetInstance(), NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(shadowButton, "Checked", _surface, FieldTypes.SHADOW, NotNullValidator.GetInstance()));
			_bindings.Add(new BidirectionalBinding(obfuscateModeButton, "SelectedTag", _surface, FieldTypes.PREPARED_FILTER_OBFUSCATE));
			_bindings.Add(new BidirectionalBinding(highlightModeButton, "SelectedTag", _surface, FieldTypes.PREPARED_FILTER_HIGHLIGHT));
		}
		
		/// <summary>
		/// shows/hides field controls (2nd toolbar on top) depending on fields of selected elements
		/// </summary>
		private void RefreshFieldControls() {
			propertiesToolStrip.SuspendLayout();
			if(_surface.HasSelectedElements || _surface.DrawingMode != DrawingModes.None) {
				// Update bindings
				foreach (BidirectionalBinding binding in _bindings) {
					binding.Refresh();
				}
				btnConfirm.Visible = btnCancel.Visible = _surface.IsElementWithFlagSelected(FieldFlag.CONFIRMABLE);
				// Update the labels separately (this might be made obsolete when adding the label to the BidirectionalBinding)
				lineThicknessLabel.Visible = lineThicknessUpDown.Visible;
				blurRadiusLabel.Visible = blurRadiusUpDown.Visible;
				magnificationFactorLabel.Visible = magnificationFactorUpDown.Visible;
				pixelSizeLabel.Visible = pixelSizeUpDown.Visible;
				brightnessLabel.Visible = brightnessUpDown.Visible;
				fontSizeLabel.Visible = fontSizeUpDown.Visible;

				// arrowHeadsDropDownButton doesn't have binding?
				arrowHeadsLabel.Visible = arrowHeadsDropDownButton.Visible = _surface.IsElementWithFieldTypeSelected(FieldTypes.ARROWHEADS);
			} else {
				HideToolstripItems();
			}
			propertiesToolStrip.ResumeLayout();
		}
		
		private void HideToolstripItems() {
			foreach(ToolStripItem toolStripItem in propertiesToolStrip.Items) {
				toolStripItem.Visible = false;
			}
		}
		
		/// <summary>
		/// refreshes all editor controls depending on selected elements and their fields
		/// </summary>
		private void RefreshEditorControls() {
			// if a confirmable element is selected, we must disable most of the controls
			// since we demand confirmation or cancel for confirmable element
			if (_surface.IsElementWithFieldTypeSelected(FieldTypes.FLAGS)) { // && ((FieldType.Flag)props.GetFieldValue(FieldType.FLAGS) & FieldType.Flag.CONFIRMABLE) == FieldType.Flag.CONFIRMABLE) {
				// disable most controls
				if(!_controlsDisabledDueToConfirmable) {
					ToolStripItemEndisabler.Disable(menuStrip1);
					ToolStripItemEndisabler.Disable(toolStrip1);
					ToolStripItemEndisabler.Disable(toolStrip2);
					ToolStripItemEndisabler.Enable(closeToolStripMenuItem);
					ToolStripItemEndisabler.Enable(helpToolStripMenuItem);
					ToolStripItemEndisabler.Enable(aboutToolStripMenuItem);
					ToolStripItemEndisabler.Enable(preferencesToolStripMenuItem);
					_controlsDisabledDueToConfirmable = true;
				}
			} else if(_controlsDisabledDueToConfirmable) {
				// re-enable disabled controls, confirmable element has either been confirmed or cancelled
				ToolStripItemEndisabler.Enable(menuStrip1);
				ToolStripItemEndisabler.Enable(toolStrip1);
				ToolStripItemEndisabler.Enable(toolStrip2);
				_controlsDisabledDueToConfirmable = false;
			}
			
			// en/disable controls depending on whether an element is selected at all
			UpdateClipboardSurfaceDependencies();
			UpdateUndoRedoSurfaceDependencies();
			
			// en/disablearrage controls depending on hierarchy of selected elements
			bool actionAllowedForSelection = _surface.HasSelectedElements && !_controlsDisabledDueToConfirmable;
			bool push = actionAllowedForSelection && _surface.CanPushSelectionDown();
			bool pull = actionAllowedForSelection && _surface.CanPullSelectionUp();
			arrangeToolStripMenuItem.Enabled = (push || pull);
			if (arrangeToolStripMenuItem.Enabled) {
				upToTopToolStripMenuItem.Enabled = pull;
				upOneLevelToolStripMenuItem.Enabled = pull;
				downToBottomToolStripMenuItem.Enabled = push;
				downOneLevelToolStripMenuItem.Enabled = push;
			}
			
			// finally show/hide field controls depending on the fields of selected elements
			RefreshFieldControls();
		}
	
		
		void ArrowHeadsToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.ChangeFields(FieldTypes.ARROWHEADS, (ArrowHeadCombination)((ToolStripMenuItem)sender).Tag);
		}
		
		void EditToolStripMenuItemClick(object sender, EventArgs e) {
			UpdateClipboardSurfaceDependencies();
			UpdateUndoRedoSurfaceDependencies();
		}

		void FontPropertyChanged(object sender, EventArgs e) {
			// in case we forced another FontStyle before, reset it first.
			if(fontBoldButton != null && _originalBoldCheckState != fontBoldButton.Checked) fontBoldButton.Checked = _originalBoldCheckState;
			if(fontItalicButton != null && _originalItalicCheckState != fontItalicButton.Checked) fontItalicButton.Checked = _originalItalicCheckState;
			
            FontFamily fam = fontFamilyComboBox.FontFamily;
           
            bool boldAvailable = fam.IsStyleAvailable(FontStyle.Bold);
			if (!boldAvailable && fontBoldButton != null) {
				_originalBoldCheckState = fontBoldButton.Checked;
				fontBoldButton.Checked = false;
			}
			if (fontBoldButton != null) {
				fontBoldButton.Enabled = boldAvailable;
           
				bool italicAvailable = fam.IsStyleAvailable(FontStyle.Italic);
				if (!italicAvailable && fontItalicButton != null) {
					fontItalicButton.Checked = false;
				}
				if (fontItalicButton != null) {
					fontItalicButton.Enabled = italicAvailable;
           
					bool regularAvailable = fam.IsStyleAvailable(FontStyle.Regular);
					if(!regularAvailable) {
						if(boldAvailable) {
							fontBoldButton.Checked = true;
						} else if(italicAvailable) {
							fontItalicButton.Checked = true;
						}
					}
				}
			}
		} 
		
	
		void FontBoldButtonClick(object sender, EventArgs e) {
			_originalBoldCheckState = fontBoldButton.Checked;
		}

		void FontItalicButtonClick(object sender, EventArgs e) {
			_originalItalicCheckState = fontItalicButton.Checked;
		}
		
		void ToolBarFocusableElementGotFocus(object sender, EventArgs e) {
			_surface.KeysLocked = true;
		}
		void ToolBarFocusableElementLostFocus(object sender, EventArgs e) {
			_surface.KeysLocked = false;
		}
		
		void SaveElementsToolStripMenuItemClick(object sender, EventArgs e) {
			SaveFileDialog saveFileDialog = new SaveFileDialog {
				Filter = "Greenshot templates (*.gst)|*.gst",
				FileName = FilenameHelper.GetFilenameWithoutExtensionFromPattern(CoreConfiguration.OutputFileFilenamePattern, _surface.CaptureDetails)
			};
			DialogResult dialogResult = saveFileDialog.ShowDialog();
			if(dialogResult.Equals(DialogResult.OK)) {
				using (Stream streamWrite = File.OpenWrite(saveFileDialog.FileName)) {
					_surface.SaveElementsToStream(streamWrite);
				}
			}
		}
		
		void LoadElementsToolStripMenuItemClick(object sender, EventArgs e) {
			OpenFileDialog openFileDialog = new OpenFileDialog {
				Filter = "Greenshot templates (*.gst)|*.gst"
			};
			if (openFileDialog.ShowDialog() == DialogResult.OK) {
				using (Stream streamRead = File.OpenRead(openFileDialog.FileName)) {
					_surface.LoadElementsFromStream(streamRead);
				}
				_surface.Refresh();
			}
		}

		void DestinationToolStripMenuItemClick(object sender, EventArgs e) {
			ILegacyDestination clickedDestination = null;
			if (sender is Control) {
				Control clickedControl = sender as Control;
				if (clickedControl.ContextMenuStrip != null) {
					clickedControl.ContextMenuStrip.Show(Cursor.Position);
					return;
				}
				clickedDestination = (ILegacyDestination)clickedControl.Tag;
			} else if (sender is ToolStripMenuItem) {
				ToolStripMenuItem clickedMenuItem = sender as ToolStripMenuItem;
				clickedDestination = (ILegacyDestination)clickedMenuItem.Tag;
			}
			if (clickedDestination != null) {
				ExportInformation exportInformation = clickedDestination.ExportCapture(true, _surface, _surface.CaptureDetails);
				if (exportInformation != null && exportInformation.ExportMade) {
					_surface.Modified = false;
				}
			}
		}
		
		protected void FilterPresetDropDownItemClicked(object sender, ToolStripItemClickedEventArgs e) {
			RefreshFieldControls();
			Invalidate(true);
		}
		
		void SelectAllToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.SelectAllElements();
		}

		
		void BtnConfirmClick(object sender, EventArgs e) {
			_surface.ConfirmSelectedConfirmableElements(true);
			RefreshFieldControls();
		}
		
		void BtnCancelClick(object sender, EventArgs e) {
			_surface.ConfirmSelectedConfirmableElements(false);
			RefreshFieldControls();
		}
		
		void Insert_window_toolstripmenuitemMouseEnter(object sender, EventArgs e) {
			ToolStripMenuItem captureWindowMenuItem = (ToolStripMenuItem)sender;
			PluginHelper.Instance.GreenshotMain.AddCaptureWindowMenuItems(captureWindowMenuItem, Contextmenu_window_Click);	
		}

		void Contextmenu_window_Click(object sender, EventArgs e) {
			ToolStripMenuItem clickedItem = (ToolStripMenuItem)sender;
			try {
				WindowDetails windowToCapture = (WindowDetails)clickedItem.Tag;
				ICapture capture = new Capture();
				using (Graphics graphics = Graphics.FromHwnd(Handle)) {
					capture.CaptureDetails.DpiX = graphics.DpiY;
					capture.CaptureDetails.DpiY = graphics.DpiY;
				}
				windowToCapture = WindowDetails.SelectCaptureWindow(windowToCapture);
				if (windowToCapture != null) {
					// TODO: Capture Helper is not in the scope of the editor project.
					//capture = CaptureHelper.CaptureWindow(windowToCapture, capture, coreConfiguration.WindowCaptureMode);
					Activate();
					WindowDetails.ToForeground(Handle);
					if (capture.Image != null) {
						_surface.AddImageContainer(capture.Image, 100, 100);
					}
				}

				capture.Dispose();
			} catch (Exception exception) {
				LOG.Error(exception);
			}
		}

		void AutoCropToolStripMenuItemClick(object sender, EventArgs e) {
			if (_surface.AutoCrop()) {
				RefreshFieldControls();
			}
		}

		void AddBorderToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.ApplyBitmapEffect(new BorderEffect());
			UpdateUndoRedoSurfaceDependencies();
		}

		/// <summary>
		/// This is used when the dropshadow button is used
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void AddDropshadowToolStripMenuItemClick(object sender, EventArgs e) {
			DropShadowEffect dropShadowEffect= new DropShadowEffect();
			// TODO: Use the dropshadow settings form to make it possible to change the default values
			//DialogResult result = new DropShadowSettingsForm(dropShadowEffect).ShowDialog(this);
			//if (result == DialogResult.OK) {
				_surface.ApplyBitmapEffect(dropShadowEffect);
				UpdateUndoRedoSurfaceDependencies();
			//}
		}

		/// <summary>
		/// Currently unused
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void ResizeToolStripMenuItemClick(object sender, EventArgs e) {
			ResizeEffect resizeEffect = new ResizeEffect(_surface.Image.Width, _surface.Image.Height, true);
			// TODO: Use the Resize SettingsForm to make it possible to change the default values
			// DialogResult result = new ResizeSettingsForm(resizeEffect).ShowDialog(this);
			// if (result == DialogResult.OK) {
				_surface.ApplyBitmapEffect(resizeEffect);
				UpdateUndoRedoSurfaceDependencies();
			//}
		}

		/// <summary>
		/// Call the torn edge effect
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void TornEdgesToolStripMenuItemClick(object sender, EventArgs e) {
			TornEdgeEffect tornEdgeEffect = new TornEdgeEffect();
			// TODO: Use the dropshadow settings form to make it possible to change the default values
			//DialogResult result = new TornEdgeSettingsForm(tornEdgeEffect).ShowDialog(this);
			//if (result == DialogResult.OK) {
				_surface.ApplyBitmapEffect(tornEdgeEffect);
				UpdateUndoRedoSurfaceDependencies();
			//}
		}

		void GrayscaleToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.ApplyBitmapEffect(new GrayscaleEffect());
			UpdateUndoRedoSurfaceDependencies();
		}

		void ClearToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.Clear(Color.Transparent);
			UpdateUndoRedoSurfaceDependencies();
		}

		void RotateCwToolstripButtonClick(object sender, EventArgs e) {
			_surface.ApplyBitmapEffect(new RotateEffect(90));
			UpdateUndoRedoSurfaceDependencies();
		}
		
		void RotateCcwToolstripButtonClick(object sender, EventArgs e) {
			_surface.ApplyBitmapEffect(new RotateEffect(270));
			UpdateUndoRedoSurfaceDependencies();
		}
		
		void InvertToolStripMenuItemClick(object sender, EventArgs e) {
			_surface.ApplyBitmapEffect(new InvertEffect());
			UpdateUndoRedoSurfaceDependencies();
		}

		private void ImageEditorFormResize(object sender, EventArgs e) {
			if (Surface == null || Surface.Image == null || panel1 == null) {
				return;
			}
			Size imageSize = Surface.Image.Size;
			Size currentClientSize = panel1.ClientSize;
			var canvas = Surface as Control;
			if (canvas == null) {
				return;
			}
			Panel panel = (Panel)canvas.Parent;
			if (panel == null) {
				return;
			}
			int offsetX = -panel.HorizontalScroll.Value;
			int offsetY = -panel.VerticalScroll.Value;
			if (currentClientSize.Width > imageSize.Width) {
				canvas.Left = offsetX + ((currentClientSize.Width - imageSize.Width) / 2);
			} else {
				canvas.Left = offsetX + 0;
			}
			if (currentClientSize.Height > imageSize.Height) {
				canvas.Top = offsetY + ((currentClientSize.Height - imageSize.Height) / 2);
			} else {
				canvas.Top = offsetY + 0;
			}
		}
	}
}
